/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.Delegation;

import socialproo.models.Ville;
import socialproo.services.interfaces.IdelegationSpecialite;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class DelegationService implements IdelegationSpecialite<Delegation,Integer,Ville> {
    private Connection connection;

    public DelegationService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Delegation t) {
            try {
            String req = "insert into delegation (id,idville,nom) values (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getId());  
            ps.setInt(2, t.getVille().getId());
            ps.setString(3,t.getNom());

            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }
	@Override
    public List<Delegation> getAllByVilleId(int id) {
        List<Delegation> dels = new ArrayList<>();
        try {
            String req = "select * from delegation where idville=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Delegation d = new Delegation(rs.getInt(1), new VilleService().findById(rs.getInt(2)), rs.getString(3));
                dels.add(d);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dels;
    }
    @Override
    public void delete(Integer id) {
            try {
            String req = "delete from delegation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }         
    }

    @Override
    public Delegation findById(Integer id) {
                Delegation delegation = null;
        try {
            String req = "select * from Delegation where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
     while(rs.next())
                delegation = new Delegation(rs.getInt(1),new VilleService().findById(rs.getInt(2)),rs.getString(3));
              
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return delegation;
    }
public Delegation findByNom(String nom) {
                Delegation delegation = null;
        try {
            String req = "select * from Delegation where nom=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
     while(rs.next())
                delegation = new Delegation(rs.getInt(1),new VilleService().findById(rs.getInt(2)),rs.getString(3));
              
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return delegation;
    }
    @Override
    public List<Delegation> findBynv(Ville nv) {
                List<Delegation> delegations = new ArrayList<>();
                   
        try {
            String req = "select * from Delegation where idville=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, nv.getId());
            ResultSet rs = ps.executeQuery();
                while(rs.next()){
                Delegation delegation = new Delegation(rs.getInt(1),new VilleService().findById(rs.getInt(2)),rs.getString(3));
                delegations.add(delegation);
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return delegations;
    }
    

    @Override
    public List<Delegation> getAll() {
       List<Delegation> delegations = new ArrayList<>();
                   
        try {
            String req = "select * from Delegation ";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ResultSet rs = ps.executeQuery();
                while(rs.next()){
                Delegation delegation = new Delegation(rs.getInt(1),new VilleService().findById(rs.getInt(2)),rs.getString(3));
                delegations.add(delegation);
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return delegations;   
    }
    public Delegation findByDelegationNom(String nom) {
        Delegation delegation = null;
        try {
            String req = "select * from delegation where nom =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                delegation = new Delegation(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return delegation;
    }
    
}
