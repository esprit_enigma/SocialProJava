/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.Ville;
import socialproo.services.interfaces.INatureVille;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class VilleService implements INatureVille<Ville,Integer>{
    private Connection connection;

    public VilleService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Ville t) {
       try {
            String req = "insert into Ville(nom) values (?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1, t.getNom());        
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
              try {
            String req = "delete from Ville where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public List<Ville> getAll() {
                List<Ville> villes = new ArrayList<>();
        try {
            String req = "select * from Ville";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Ville ville = new Ville(rs.getInt(1),rs.getString(2));
                villes.add(ville);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return villes;
    }

    @Override
    public Ville findById(Integer id) {
        Ville ville =null;
              try {
            String req = "select * from Ville where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
           while(rs.next())
            ville = new Ville(rs.getInt(1),rs.getString(2));

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
             return ville ;   
    }
        public Ville findByVillename(String villeName) {
        Ville ville = null;
        try {
            String req = "select * from ville where nom =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, villeName);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ville = new Ville(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ville;
    }
 @Override
    public Ville findByNom(String nom) {
      Ville v = null;
        try {
            String req = "select * from ville where nom =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                v = new Ville(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return v;  
    
    }

  
    
}
