/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Recomendation;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.services.interfaces.IRecomendationService;

/**
 *
 * @author Alaa
 */
public class RecomendationService implements IRecomendationService {

    private Connection connection;

    public RecomendationService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Recomendation r) {
        try {
            String req = "insert into recomendation(jobber_id,entreprise_id) values(?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, r.getJobber_id().getId());
            ps.setInt(2, r.getEntreprise_id().getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Recomendation r) {
        try {
            String req = "Update recomendation set (jobber_id,entreprise_id,date_ajout) values(?,?,?) where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, r.getJobber_id().getId());
            ps.setInt(2, r.getEntreprise_id().getId());
            ps.setDate(3, r.getDate_ajout());
            ps.setInt(4, r.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            String req = "delete from recomendation where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Recomendation> getAll() {
        List<Recomendation> lr = new ArrayList<>();
        try {
            String req = "select * from recomendation";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Recomendation r = new Recomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getDate(4));
                lr.add(r);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lr;
    }

    @Override
    public Recomendation findById(int id) {
        Recomendation r = null;
        try {
            String req = "Select * from recomendation where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = new Recomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getDate(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }
    
    public Recomendation findByTwoIds(int jobber_id,int entreprise_id) {
        Recomendation r = null;
        try {
            String req = "Select * from recomendation where jobber_id=? and entreprise_id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, jobber_id);
            ps.setInt(2, entreprise_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                r = new Recomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getDate(4));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return r;
    }
}
