/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.CodePostal;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CodePostalService implements IService<CodePostal, Integer> {

    private Connection connection;

    public CodePostalService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(CodePostal t) {
        try{
        String req = "insert into code_postal(cp, iddel) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1,t.getNom() );
            ps.setInt(2, t.getDelegation().getId());
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void update(CodePostal t) {
        try{
        String req = "update code_postal set( cp, iddel) VALUES (?,?)where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1,t.getNom() );
            ps.setInt(2, t.getDelegation().getId());
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void delete(Integer id) {
           try {
            String req = "delete from code_postal where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<CodePostal> getAll() {
        List<CodePostal> coedepostaux = new ArrayList<>();
        try {
            String req = "select (id,cp, iddel) from code_postal";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CodePostal codepostal = new CodePostal(rs.getInt(1), rs.getString(2), new DelegationService().findById(rs.getInt(3)));
                coedepostaux.add(codepostal);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return coedepostaux;
    }

    @Override
    public CodePostal findById(Integer id) {
        CodePostal codepostal = null;
        try {
            String req = "select * from code_postal where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codepostal = new CodePostal(rs.getInt(1), rs.getString(2),new DelegationService().findById(rs.getInt(3)));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return codepostal;
    }
    
  public CodePostal findByCodepostalId(int delegationId) {
        CodePostal codepostal = null;
        try {
            String req = "select * from code_postal where iddel =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, delegationId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codepostal = new CodePostal(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return codepostal;
    }
    public CodePostal findByCodepostalName(String nom) {
        CodePostal codepostal = null;
        try {
            String req = "select * from code_postal where cp =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codepostal = new CodePostal(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return codepostal;
    }
   
    
}
