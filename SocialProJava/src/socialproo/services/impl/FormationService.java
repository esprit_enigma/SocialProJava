/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.Experience;
import socialproo.models.Formation;
import socialproo.models.Jober;
import socialproo.services.interfaces.ICvJober;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class FormationService implements ICvJober<Formation,Integer,Jober>  {
    private Connection connection;

    public FormationService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Formation t) {
        try {
            String req = "insert into formation (idprofil,institut,diplome,description,dateDebut,dateFin,dateAjout) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getJober().getId());
            
            ps.setString(2, t.getInstitut());
            ps.setString(3,t.getDiplome());
            ps.setString(4,t.getDescription());
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDebut().getTime());
            ps.setDate(5,sqlDate1 );
            
            java.sql.Date sqlDate2 = new java.sql.Date(t.getDateFin().getTime());
            ps.setDate(6,sqlDate2 );
            
            java.sql.Date sqlDate3 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(7,sqlDate3 );
            
             
             
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
                   try {
            String req = "delete from formation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public List<Formation> findByJober(Jober jober) {
        List<Formation> formations = new ArrayList<>();
        try {
            String req = "select * from formation where idprofil=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, jober.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Formation formation = new Formation(rs.getInt(1),new JoberService().findById(rs.getInt(2)),rs.getString(3),rs.getString(4),rs.getString(5),rs.getDate(6),rs.getDate(7),rs.getDate(8));
                formations.add(formation);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return formations;
    }
   
    
    }
   
    

