/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Activite;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ActiviteService implements IService<Activite, Integer> {

    private Connection connection;

    public ActiviteService() {
         connection = DataSource.getInstance().getConnection();
    }
    

    @Override
    public void add(Activite t) {
        try{
            System.out.println("rah"+t.getId());
        String req = "insert into activite(categorie_id, nom) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
             System.out.println("rah"+t.getId());
            ps.setInt(1, t.getCategorie().getId());
            ps.setString(2,t.getNom() );
           
            
        ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void update(Activite t) {
        try{
        String req = "update activite set categorie_id=? , nom=?  where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, t.getCategorie().getId());
            ps.setString(2,t.getNom() );
            ps.setInt(3,t.getId());
            
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void delete(Integer id) {
       try {
            String req = "delete from activite where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Activite> getAll() {
                List<Activite> activites = new ArrayList<>();
        try {
            String req = "select * from activite";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                //Activite activite = new Activite( new CategorieService().findById(rs.getInt(1)),rs.getString(2));
                Activite activite = new Activite( rs.getInt(1),new CategorieService().findById(rs.getInt(2)),rs.getString(3));
                activites.add(activite);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return activites;
    }

    @Override
    public Activite findById(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      Activite activite = null;
        try {
            String req = "select * from activite where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
              
                activite = new Activite(rs.getInt(1),new CategorieService().findById(rs.getInt(2)),rs.getString(3));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return activite;
        
    }
        public Activite findByNom(String n) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      Activite activite = null;
        try {
            String req = "select * from activite where nom =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, n);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                activite = new Activite(rs.getInt(1),new CategorieService().findById(rs.getInt(2)),rs.getString(3));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return activite;
        
    }


    
}
