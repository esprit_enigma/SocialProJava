/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import socialproo.models.Nature;
import socialproo.models.Ville;
import socialproo.services.interfaces.INatureVilles;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class NatureService implements INatureVilles<Nature,Integer,String>{
    private Connection connection;

    public NatureService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Nature t) {
          try {
            String req = "insert into Nature(nom,dateAjout) values (?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1, t.getNom());  
            
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(2,sqlDate1 );
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
                 try {
            String req = "delete from Nature where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public List<Nature> getAll() {
                  List<Nature> natures = new ArrayList<>();
        try {
            String req = "select * from Nature";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                  Date date1 = new Date(rs.getDate(3).getTime());
                Nature nature = new Nature(rs.getInt(1),rs.getString(2),date1);
                natures.add(nature);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return natures;
    }

    @Override
    public Nature findById(Integer id) {
          Nature nature =null;
              try {
            String req = "select * from Nature where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
             ps.setInt(1, id);
             
            ResultSet rs = ps.executeQuery();
            while (rs.next()){ 
            Date date1 = new Date(rs.getDate(3).getTime());
                 nature = new Nature(rs.getInt(1),rs.getString(2),date1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
             return nature ;  
    }

    @Override
    public Nature findByNom(String nom) {
      
        Nature n = null;
        try {
            String req = "select * from nature where nom=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                n = new Nature(rs.getInt(1), rs.getString(2), rs.getDate(3));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
      
    
    
    }
    
}
