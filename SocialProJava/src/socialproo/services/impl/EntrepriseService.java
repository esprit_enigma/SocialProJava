/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Entreprise;
import socialproo.models.User;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import socialproo.models.ListEntreprises;
import socialproo.models.ListProjet;

/**
 *
 * @author ASUS
 */
//public class EntrepriseService implements IService<Entreprise, Integer> {
public class EntrepriseService implements IService<Entreprise, Integer> {

    private Connection connection;

    public EntrepriseService() {
        connection = DataSource.getInstance().getConnection();
    }
    

    @Override
    public void add(Entreprise t) {
        try{
        String req = "insert into entreprise(user_id,nom, adresse, Nationalite, siteWeb, numTel, description, valide, emailentrp,dateBloque,dateAjout,upload,facebook,twitter,googlePlus,linkedin) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            //ps.setInt(1, t.getCodePostal().getId());
            System.out.println("rah"+t.getUserId());
           
            ps.setInt(1, t.getUserId().getId());
           
            
            ps.setString(2,t.getNom() );
            ps.setString(3,t.getAdresse() );
            ps.setString(4,t.getNationalite() );
            ps.setString(5,t.getEmailentrp());
            ps.setInt(6,t.getNumTel() );
            ps.setString(7,t.getDescription() );
            
            ps.setBoolean(8, t.isValide());
            ps.setString(9, t.getEmail());
            //java.sql.Date sqlDate1 = new java.sql.Date(t.getDateAjout().getTime());
             ps.setString(10,LocalDate.now().plus(Period.ofDays(3)).toString());
             //java.sql.Date sqlDate2 = new java.sql.Date(t.getDateBloque().getTime());
             ps.setString(11,LocalDate.now().toString() );


            ps.setBoolean(12, t.isUpload());
             ps.setString(13, t.getFacebook());
              ps.setString(14, t.getTwitter());
               ps.setString(15, t.getGooglePlus());
                ps.setString(16, t.getLinkedin());
                 
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 

    }

    @Override
    public void update(Entreprise t) {
       try{
           //"update user set (name, email) values (?,?) where id = ?"
        String req = "update entreprise set nom=?, adresse=?, Nationalite=?, siteWeb=?, numTel=?, description=?, facebook=?, twitter=?, googlePlus=?, linkedin=?, valide=?, emailentrp=? where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1,t.getNom() );
            ps.setString(2,t.getAdresse() );
            ps.setString(3,t.getNationalite() );
            ps.setString(4,t.getSiteWeb() );
            ps.setInt(5,t.getNumTel() );
            ps.setString(6,t.getDescription() );
            ps.setString(7,t.getFacebook());
            ps.setString(8,t.getTwitter() );
            ps.setString(9,t.getGooglePlus() );
            ps.setString(10,t.getLinkedin() );
            ps.setBoolean(11, t.isValide());
            ps.setString(12, t.getEmail());
            
            ps.setInt(13, t.getId());
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }
    public void updateCodePostal(Entreprise t) {
       try{
           //"update user set (name, email) values (?,?) where id = ?"
        String req = "update entreprise set idcp=?  where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, t.getCodePostal().getId());
            ps.setInt(2, t.getId());
           
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }
        public void updateUpload(int id) {
       try{
           //"update user set (name, email) values (?,?) where id = ?"
        String req = "update entreprise set upload=? where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setBoolean(1, true);
            ps.setInt(2, id);
           System.out.println("logina");
           System.out.println("idddd"+id);
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }
               public void updateValide(int id) {
       try{
           //"update user set (name, email) values (?,?) where id = ?"
        String req = "update entreprise set valide=?  where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setBoolean(1, true);
            ps.setInt(2, id);
           
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }
        public void updateReseaux(Entreprise t) {
       try{
           //"update user set (name, email) values (?,?) where id = ?"
        String req = "update entreprise set facebook=?,twitter=?,linkedin=?,googlePlus=?  where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, t.getFacebook());
            ps.setString(2, t.getTwitter());
            ps.setString(3, t.getLinkedin());
            ps.setString(4, t.getGooglePlus());
            ps.setInt(5, t.getId());
           
            ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }
        


    @Override
    public void delete(Integer id) {
         try {
            String req = "delete from entreprise where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Entreprise> getAll() {
        System.out.println("ok");
        List<Entreprise> entreprises = new ArrayList<>();
        try {
            String req = "select * from entreprise";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Date date1= new Date(rs.getDate(16).getTime());
                Date date2= new Date(rs.getDate(17).getTime());
                //User user=null;
                Entreprise entreprise = new Entreprise(rs.getInt(1),new CodePostalService().findById(rs.getInt(2)),new UserService().findById(rs.getInt(3)), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),rs.getInt(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13),rs.getBoolean(14), rs.getString(15),date1,date2,rs.getBoolean(18));
                entreprises.add(entreprise);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return entreprises;
    }
         public ObservableList<ListEntreprises> getList() {
                
                 ObservableList<ListEntreprises> listentreprises = FXCollections.observableArrayList();
        try {
            String req = "select * from entreprise ";
            
            PreparedStatement ps = connection.prepareStatement(req);
             
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //(int id, String username, String email, String nom, int telephone)
                ListEntreprises entreprise = new ListEntreprises(rs.getInt(1),new UserService().findById(rs.getInt(3)).getUsername(),rs.getString(15),rs.getString(4),rs.getInt(8) );
                listentreprises.add(entreprise);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return listentreprises;
    }
          public ObservableList<ListEntreprises> getListtovalide() {
                
                 ObservableList<ListEntreprises> listentreprises = FXCollections.observableArrayList();
        try {
            String req = "select * from entreprise  ";
            
            PreparedStatement ps = connection.prepareStatement(req);
             
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("res"+rs.getInt(1));
                //,String datebloque 16,String dateajout17,boolean upload18,boolean valide14
                ListEntreprises entreprise = new ListEntreprises(rs.getInt(3),rs.getInt(1),new UserService().findById(rs.getInt(3)).getUsername(),rs.getString(15),rs.getString(4),rs.getInt(8),rs.getString(16),rs.getString(17),rs.getBoolean(18),rs.getBoolean(14),new UserService().findById(rs.getInt(3)).getRoles());
                   System.out.println("res"+entreprise.toString());
                listentreprises.add(entreprise);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return listentreprises;
    }


    @Override
    public Entreprise findById(Integer id) {
        Entreprise entreprise = null;
        try {
            String req = "select * from entreprise where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                entreprise = new Entreprise(rs.getInt(1), rs.getString(2));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return entreprise;
    }
    
    
     public Entreprise findByIdUser(Integer id) {
        Entreprise entreprise = null;
        try {
            String req = "select * from entreprise where user_id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                 Date date1= new Date(rs.getDate(16).getTime());
                Date date2= new Date(rs.getDate(17).getTime());
                entreprise = new Entreprise(rs.getInt(1),new CodePostalService().findById(rs.getInt(2)), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7),rs.getInt(8), rs.getString(9),rs.getString(15), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return entreprise;
    }


   
    
    
}
