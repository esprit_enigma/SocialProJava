package socialproo.services.impl;

import javafx.scene.control.TextField;

public class Functions {


    public Functions() {
    }


    public boolean isDouble(TextField tf) {
        boolean b = false;
        if (!(tf.getText() == null || tf.getText().length() == 0)) {
            try {
                // Do all the validation you need here such as
                double value = Double.parseDouble(tf.getText());
                if (value > 0.5) {
                    b = true;
                }
            } catch (NumberFormatException e) {
            }
        }
        return b;
    }

    public boolean isValidInteger(TextField tf, int i) {
        boolean b = false;
        if (!(tf.getText() == null || tf.getText().length() == 0)) {
            try {
                int value = Integer.parseInt(tf.getText());
                if ((tf.getText().length() == i)) {
                    b = true;
                }
            } catch (NumberFormatException e) {
            }
        }
        return b;
    }

    public boolean isMobileField(TextField tf) {
        boolean b = false;
        if (tf.getText().isEmpty()) {
            b = true;
        }
        if (!(tf.getText() == null || tf.getText().length() == 0)) {
            try {
                int value = Integer.parseInt(tf.getText());
                if ((tf.getText().length() <= 8)) {
                    b = true;
                }
            } catch (NumberFormatException e) {
            }
        }
        return b;
    }

}
