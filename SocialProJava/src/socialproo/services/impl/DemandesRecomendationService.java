/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.DemandesRecomendation;
import socialproo.services.interfaces.IDemandesRecomendation;
import socialproo.technique.DataSource;

/**
 *
 * @author Alaa
 */
public class DemandesRecomendationService implements IDemandesRecomendation {

    private Connection connection;

    public DemandesRecomendationService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(DemandesRecomendation dm) {
        try {
            String req = "insert into demandes_recomendation(expediteur_id, recepteur_id,etat) values (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, dm.getExpediteur_id().getId());
            ps.setInt(2, dm.getRecepteur_id().getId());
            ps.setString(3, dm.getEtat());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(DemandesRecomendation dm) {
        try {
            String req = "update demandes_recomendation set (expediteur_id, recepteur_id,etat) values (?,?,?) where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, dm.getExpediteur_id().getId());
            ps.setInt(2, dm.getRecepteur_id().getId());
            ps.setString(3, dm.getEtat());
            ps.setInt(4, dm.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            String req = "delete from demandes_recomendation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<DemandesRecomendation> getAll() {
        List<DemandesRecomendation> dms = new ArrayList<>();
        try {
            String req = "select * from demandes_recomendation";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DemandesRecomendation dm = new DemandesRecomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getString(4));
                dms.add(dm);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dms;
    }
    
     public List<DemandesRecomendation> getAllByExp(int exp) {
        List<DemandesRecomendation> dms = new ArrayList<>();
        try {
            String req = "select * from demandes_recomendation where recepteur_id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, exp);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                DemandesRecomendation dm = new DemandesRecomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getString(4));
                dms.add(dm);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dms;
    }
    
    
    @Override
    public DemandesRecomendation findById(int id) {
        DemandesRecomendation dm = null;
        try {
            String req = "select * from demandes_recomendation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dm = new DemandesRecomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getString(4));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dm;
    }

    public DemandesRecomendation findByTwoIds(int expediteur_id, int recepteur_id) {
        DemandesRecomendation dm = null;
        try {
            String req = "select * from demandes_recomendation WHERE expediteur_id=? AND recepteur_id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, expediteur_id);
            ps.setInt(2, recepteur_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                dm = new DemandesRecomendation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)), rs.getString(4));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dm;
    }    
}
