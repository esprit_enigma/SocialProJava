/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import socialproo.models.Competance;
import socialproo.models.Nature;
import socialproo.services.interfaces.INatureVilles;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class CompetanceService implements INatureVilles<Competance,Integer,String>{
    private Connection connection;

    public CompetanceService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Competance t) {
         try {
            String req = "insert into Competance(nom,valide,dateAjout) values (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1, t.getNom());  
            ps.setBoolean(2, t.getValide());
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(3,sqlDate1 );
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
          try {
            String req = "delete from Competance where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }     
    }

    @Override
    public List<Competance> getAll() {
          List<Competance> competances = new ArrayList<>();
        try {
            String req = "select * from Competance";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                
                Date date1 = new Date(rs.getDate(4).getTime());
                Competance competance = new Competance(rs.getInt(1),rs.getString(2),rs.getBoolean(3),date1);
                competances.add(competance);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return competances;
    }

    @Override
    public Competance findById(Integer id) {
           Competance competance =null;
              try {
            String req = "select * from Competance where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
             ps.setInt(1, id);
             
            ResultSet rs = ps.executeQuery();
            while (rs.next()){ 
            Date date1 = new Date(rs.getDate(4).getTime());
                 competance = new Competance(rs.getInt(1),rs.getString(2),rs.getBoolean(3),date1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
             return competance ;  
    }

    @Override
    public Competance findByNom(String nom) {
     Competance competance =null;
              try {
            String req = "select * from Competance where nom=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
             ps.setString(1, nom);
             
            ResultSet rs = ps.executeQuery();
            while (rs.next()){ 
            Date date1 = new Date(rs.getDate(4).getTime());
                 competance = new Competance(rs.getInt(1),rs.getString(2),rs.getBoolean(3),date1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
             return competance ;  
    }
    
}
