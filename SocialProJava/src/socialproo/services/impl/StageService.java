/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.Experience;
import socialproo.models.Jober;
import socialproo.models.Stage;
import socialproo.services.interfaces.ICvJober;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class StageService implements ICvJober<Stage,Integer,Jober>{
    
    private Connection connection;

    public StageService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Stage t) {
            try {
            String req = "insert into stage (idprofil,organisation,dateDebut,dateFin,dateAjout,description) values (?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getJober().getId());
            
            ps.setString(2, t.getOrganisation());
         
            
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDebut().getTime());
            ps.setDate(3,sqlDate1 );
            
            java.sql.Date sqlDate2 = new java.sql.Date(t.getDateFin().getTime());
            ps.setDate(4,sqlDate2 );
            
            java.sql.Date sqlDate3 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(5,sqlDate3 );
            
             ps.setString(6,t.getDescription());
             
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
      try {
            String req = "delete from stage where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }      
    }

    @Override
    public List<Stage> findByJober(Jober jober) {
               List<Stage> stages = new ArrayList<>();
        try {
            String req = "select * from Stage where idprofil=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, jober.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Stage stage = new Stage(rs.getInt(1),new JoberService().findById(rs.getInt(2)),rs.getString(3),rs.getDate(4),rs.getDate(5),rs.getDate(6),rs.getString(7));
                stages.add(stage);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return stages;   
    }
    
}
