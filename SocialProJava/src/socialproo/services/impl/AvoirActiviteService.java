/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.AvoirActivite;
import socialproo.models.CodePostal;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import socialproo.models.ActiviteComplexe;

/**
 *
 * @author ASUS
 */
public class AvoirActiviteService implements IService<AvoirActivite, Integer> {

    private Connection connection;

    public AvoirActiviteService() {
         connection = DataSource.getInstance().getConnection();
    }
    

    @Override
    public void add(AvoirActivite t) {
        try{
        String req = "insert into avoir_activite(id_activite, id_entreprise,description) VALUES (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,t.getActivite().getId() );
            ps.setInt(2, t.getEntreprise().getId());
            ps.setString(3, t.getDescription());
            
          
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void update(AvoirActivite t) {
            try{
            String req = "update avoir_activite set id_activite=?,  id_entreprise=?, description=? where id=? ";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,t.getActivite().getId() );
            ps.setInt(2, t.getEntreprise().getId());
            ps.setString(3, t.getDescription());
          
            ps.setInt(4,t.getId() );
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void delete(Integer id) {
          try {
            String req = "delete from avoir_activite where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ObservableList<AvoirActivite> getAll() {
                //List<AvoirActivite> avoiractivites = new ArrayList<>();
                 ObservableList<AvoirActivite> avoiractivites = FXCollections.observableArrayList();
        try {
            String req = "select * from avoir_activite";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AvoirActivite avoiractivite = new AvoirActivite(rs.getInt(1), rs.getString(4),rs.getString(5), new ActiviteService().findById(rs.getInt(2)),new EntrepriseService().findById(rs.getInt(3)));
                avoiractivites.add(avoiractivite);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return avoiractivites;
    }
     public ObservableList<ActiviteComplexe> getList(int id) {
                //List<AvoirActivite> avoiractivites = new ArrayList<>();
                 ObservableList<ActiviteComplexe> activitescomplexes = FXCollections.observableArrayList();
        try {
            String req = "select * from avoir_activite where id_entreprise = ?";
            
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //new EntrepriseService().findById(rs.getInt(3))
                ActiviteComplexe avoiractivite = new ActiviteComplexe((new ActiviteService().findById(rs.getInt(2))).getId(), rs.getString(4), (new ActiviteService().findById(rs.getInt(2))).getNom(),(new ActiviteService().findById(rs.getInt(2))).getCategorie().getNom());
                activitescomplexes.add(avoiractivite);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return activitescomplexes;
    }

    @Override
    public AvoirActivite findById(Integer id) {
       AvoirActivite avoiractivite = null;
        try {
            String req = "select * from avoir_activite where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                avoiractivite = new AvoirActivite(rs.getInt(1), rs.getString(4),rs.getString(5), new ActiviteService().findById(rs.getInt(2)),new EntrepriseService().findById(rs.getInt(3)));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return avoiractivite;
    }
    public AvoirActivite findByIdActivite(Integer id) {
       AvoirActivite avoiractivite = null;
        try {
            String req = "select * from avoir_activite where id_activite =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                avoiractivite = new AvoirActivite(rs.getInt(1), rs.getString(4),rs.getString(5), new ActiviteService().findById(rs.getInt(2)),new EntrepriseService().findById(rs.getInt(3)));
                
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return avoiractivite;
    }
public AvoirActivite findByIdEntreprise(Integer id) {
       AvoirActivite avoiractivite = null;
        try {
            String req = "select * from avoir_activite where id_entreprise =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                avoiractivite = new AvoirActivite(rs.getInt(1), rs.getString(4),rs.getString(5), new ActiviteService().findById(rs.getInt(2)),new EntrepriseService().findById(rs.getInt(3)));
                System.out.println("s5attttttttttt"+avoiractivite);
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return avoiractivite;
    }


    
}
