/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Delegation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import socialpro.controllers.LoginController;

import socialproo.models.Jober;
import socialproo.models.Specialite;
import socialproo.models.User;
import socialproo.services.interfaces.IJober;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class JoberService implements IJober {
    private Connection connection;
    LoginController lc = new LoginController();
    User user = lc.getCurrentUser();
    public JoberService() {
        connection = DataSource.getInstance().getConnection();
    }
    
    @Override
    public void add(Jober t) {
              try {
            String req = "insert into profil (iddel,idspec,nom,prenom,dateNaissance,adresse,sexe,description,numTel,dateCreation,idUser) values (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getDelegation().getId());    
            
            ps.setInt(2, t.getSpecialite().getId());
            
            ps.setString(3,t.getNom());
            
            ps.setString(4,t.getPrenom());
            
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDeNaissance().getTime());
            ps.setDate(5,sqlDate1 );
            
            ps.setString(6,t.getAdresse());
            
            ps.setString(7,t.getSexe());
            
            ps.setString(8,t.getDescription()); 
            
            ps.setInt(9,t.getNumTel());
            
            java.sql.Date sqlDate3 = new java.sql.Date(t.getDateCreation().getTime());
            ps.setDate(10,sqlDate3 );
            
            ps.setInt(11,t.getUser().getId());
             
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }  
    }

    @Override
    public void update(Jober t) {
            try {
            String req = "update profil set iddel=?,idspec=?,nom=?,prenom=?,dateNaissance=?,adresse=?,sexe=?,description=?,numTel=? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
        
            ps.setInt(1, t.getDelegation().getId());    
            ps.setInt(2, t.getSpecialite().getId());
            ps.setString(3,t.getNom());
            ps.setString(4,t.getPrenom());
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDeNaissance().getTime());
            ps.setDate(5,sqlDate1 );
            ps.setString(6,t.getAdresse());
            ps.setString(7,t.getSexe());
            ps.setString(8,t.getDescription()); 
            ps.setInt(9,t.getNumTel());

            
            ps.setInt(10, t.getId());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }  
    }

    @Override
    public void delete(int id) {
                try {
            String req = "delete from Jober where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }      
    }

    @Override
    public List<Jober> getAll() {
                  List<Jober> jobers = new ArrayList<>();
        try {
            String req = "select * from Profil";
            PreparedStatement ps = connection.prepareStatement(req);
       
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Date date1= new Date(rs.getDate(6).getTime());
                          Date date2= new Date(rs.getDate(11).getTime());
               
              Jober jober =new Jober(rs.getInt(1),new DelegationService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),date1,rs.getString(7),rs.getString(9),rs.getInt(10),date2,user,rs.getString(8));
  jobers.add(jober);
          }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return jobers;
    }

    @Override
    public Jober findById(int id) {
               Jober jober = null;
        try {
            String req = "select * from Profil where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Date date1= new Date(rs.getDate(6).getTime());
                       Date date2= new Date(rs.getDate(11).getTime());
               
               jober =new Jober(rs.getInt(1),new DelegationService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),date1,rs.getString(7),rs.getString(9),rs.getInt(10),date2,user,rs.getString(8));  
            }
        
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return jober;
    }

    @Override
    public List<Jober> findBySpecialite(Specialite spec) {
                  List<Jober> jobers = new ArrayList<>();
        try {
            String req = "select * from Profil where idspec=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, spec.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Date date1= new Date(rs.getDate(6).getTime());
                Date date2= new Date(rs.getDate(11).getTime());
               
             Jober  jober =new Jober(rs.getInt(1),new DelegationService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),date1,rs.getString(7),rs.getString(9),rs.getInt(10),date2,user,rs.getString(8));
            jobers.add(jober);
            }
            
  
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return jobers;
    }

    @Override
    public List<Jober> findByDelegation(Delegation del) {
                     List<Jober> jobers = new ArrayList<>();
        try {
            String req = "select * from Profil where iddel=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, del.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Date date1= new Date(rs.getDate(6).getTime());
                Date date2= new Date(rs.getDate(11).getTime());
               
          Jober   jober =new Jober(rs.getInt(1),new DelegationService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),date1,rs.getString(7),rs.getString(9),rs.getInt(10),date2,user,rs.getString(8));
          jobers.add(jober);
            }
            
  
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return jobers;  
    }
        public Jober findByUser(User user) {
               Jober jober = null;
        try {
            String req = "select * from Profil where idUser=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Date date1= new Date(rs.getDate(6).getTime());
                Date date2= new Date(rs.getDate(11).getTime());
               
               jober =new Jober(rs.getInt(1),new DelegationService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),date1,rs.getString(7),rs.getString(9),rs.getInt(10),date2,user,rs.getString(8));
            }
  
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return jober;
    }

}
