/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Notification;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import socialproo.services.interfaces.INotificationService;

/**
 *
 * @author Alaa
 */
public class NotificationService implements INotificationService {

    private Connection connection;

    public NotificationService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Notification n) {
        try {
            String req = "insert into notification(user_id,text,state) values (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, n.getUser_id().getId());
            ps.setString(2, n.getText());
            ps.setString(3, n.getState());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Notification n) {
        try {
            String req = "Update notification set (user_id,text, time,state,time_opened) values (?,?,?,?,?) where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(2, n.getText());
            ps.setDate(2, (Date) n.getTime());
            ps.setInt(1, n.getUser_id().getId());
            ps.setString(4, n.getState());
            ps.setDate(5, (Date) n.getTime_opened());
            ps.setInt(6, n.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            String req = "delete from notification where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Notification> getAll() {
        List<Notification> ns = new ArrayList<>();
        try {
            String req = "select * from notification";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Notification n = new Notification(rs.getInt(1), new UserService().findById(rs.getInt(2)), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getDate(6));
                ns.add(n);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ns;
    }

    @Override
    public Notification findById(int id) {
        Notification n = null;
        try {
            String req = "select * from notification where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                n = new Notification(rs.getInt(1), new UserService().findById(rs.getInt(2)), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getDate(6));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public List<Notification> getAllByUser(int id) {
        List<Notification> ns = new ArrayList<>();
        try {
            String req = "select * from notification where user_id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Notification n = new Notification(rs.getInt(1), new UserService().findById(rs.getInt(2)), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getDate(6));
                ns.add(n);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ns;
    }
}
