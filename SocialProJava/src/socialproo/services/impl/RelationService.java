/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import socialproo.models.Relation;
import socialproo.services.interfaces.IRelationService;
import socialproo.technique.DataSource;

/**
 *
 * @author Alaa
 */
public class RelationService implements IRelationService {

    private Connection connection;

    public RelationService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Relation r) {
        try {
            String req = "insert into relation(idfollower, idfollowed) values (?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, r.getIdfollower().getId());
            ps.setInt(2, r.getIdfollowed().getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Relation r) {
        try {
            String req = "update relation set (idfollower, idfollowed) values (?,?) where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, r.getIdfollower().getId());
            ps.setInt(2, r.getIdfollowed().getId());
            ps.setInt(3, r.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            String req = "delete from relation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Relation> getAll() {
        List<Relation> relations = new ArrayList<>();
        try {
            String req = "select * from relation";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Relation relation = new Relation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)));
                relations.add(relation);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return relations;
    }

    @Override
    public Relation findById(int id) {
        Relation relation = null;
        try {
            String req = "select * from relation where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                relation = new Relation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return relation;
    }
    
    public Relation findByIdFollowerIdFollowed(int idfollower,int idfollowed ) {
        Relation relation = null;
        try {
            String req = "select * from relation where 	idfollower=? and idfollowed=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idfollower);
            ps.setInt(2, idfollowed);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                relation = new Relation(rs.getInt(1), new UserService().findById(rs.getInt(2)), new UserService().findById(rs.getInt(3)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return relation;
    }
}
