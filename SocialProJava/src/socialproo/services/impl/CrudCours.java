/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Commentaires;
import socialproo.models.Cours;
import socialproo.models.Entreprise;
import socialproo.services.interfaces.IServiceCrud;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 *
 * @author m_s info
 */
public class CrudCours implements IServiceCrud<Cours>{
     private Connection connexion ;
    
    public CrudCours ()
    {
        connexion = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Cours t) {
    String req = "insert into cours (iduser,idspec,description,note,tuto,courpdf,date_ajout,date_modification,etat,titre,image) values (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
            ps.setInt(1, t.getEntreprise().getId());
  ps.setInt(2, t.getSpecialite().getId());
            ps.setString(3, t.getDescription());
            ps.setInt(4, 0);
            ps.setString(5, t.getTuto());
            ps.setString(6, t.getCourpdf());
            ps.setDate(7, new java.sql.Date(t.getDateAjout().getTime()));
            ps.setDate(8,null);
            ps.setBoolean(9, false);
            ps.setString(10,t.getTitre());
            ps.setString(11,t.getImage());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            TrayNotification tr = new TrayNotification("Erreur de communication avec la base", "Erreur de communication avec la base", NotificationType.ERROR);
    tr.showAndWait();
        }    
    }

    @Override
    public void update(Cours t) {
        String req = "update cours set (iduser,idspec,description,note,tuto,courpdf,date_ajout,date_modification,etat,titre,image) values (?,?,?,?,?,?,?,?,?,?,?) where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
            ps.setInt(1, t.getEntreprise().getId());
            ps.setInt(2, t.getSpecialite().getId());
            ps.setString(3, t.getDescription());
            ps.setInt(4, 0);
            ps.setString(5, t.getTuto());
            ps.setString(6, t.getCourpdf());
            ps.setDate(7, new java.sql.Date(t.getDateAjout().getTime()));
            ps.setDate(8,null);
            ps.setBoolean(9, false);
            ps.setString(10,t.getTitre());
            ps.setString(11,t.getImage());
            
            ps.setInt(12, t.getId());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
             TrayNotification tr = new TrayNotification("Erreur de communication avec la base", "Erreur de communication avec la base", NotificationType.ERROR);
    tr.showAndWait();
        }    
    
    }

    @Override
    public void delete(int id) {
    String req = "delete from cours where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setInt(1, id);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
             TrayNotification tr = new TrayNotification("Erreur de communication avec la base", "Erreur de communication avec la base", NotificationType.ERROR);
    tr.showAndWait();
        }      
    }

    @Override
    public List<Cours> getAll() {
      List<Cours> cours = new ArrayList<Cours>();
        String req = "select * from cours";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //    public Cours(int id, Entreprise entreprise, Specialite specialite                                                   ,String description,String image, int note,  String tuto, String courpdf, Date dateAjout, Date dateModification, boolean etat, String titre) {

                Cours c = new Cours(rs.getInt(1), (Entreprise) new EntrepriseService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getString(7),rs.getString(8),rs.getDate(9),rs.getDate(10),rs.getBoolean(11),rs.getString(12));
                cours.add(c);
                System.out.println("nnnnn"+cours);
            }
            
        } catch (SQLException ex) {
//             TrayNotification tr = new TrayNotification("Erreur de communication avec la base", "Erreur de communication avec la base", NotificationType.ERROR);
//    tr.showAndWait();
            System.out.println("nnnnn"+cours);
             ex.printStackTrace();
            
        } 
        
        return cours;    
    }

    @Override
    public Cours findById(int id) {
    Cours cours = null;
        try {
            String req = "select * from cours where id =?";
            PreparedStatement ps = connexion.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cours c = new Cours(rs.getInt(1), (Entreprise) new EntrepriseService().findById(rs.getInt(2)),new SpecialiteService().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getString(7),rs.getString(8),rs.getDate(9),rs.getDate(10),rs.getBoolean(11),rs.getString(12));
            }
        } catch (SQLException ex) {
            TrayNotification tr = new TrayNotification("Erreur de communication avec la base", "Erreur de communication avec la base", NotificationType.ERROR);
    tr.showAndWait();
        }
        return cours;    
    }
    
}
