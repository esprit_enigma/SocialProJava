/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Jober;
import socialproo.models.Projet;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import socialproo.services.interfaces.ICvJober;

/**
 *
 * @author oudayblouza
 */
public class ProjetService implements ICvJober<Projet,Integer,Jober> {
   private Connection connection;

    public ProjetService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Projet t) {
      try {
            String req = "insert into projet (idprofil,nom,organisation,description,dateDebutProjet,dateFinProjet,dateAjout) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getJober().getId());
            
            ps.setString(2, t.getNom());
            ps.setString(3,t.getOrganisation());
            ps.setString(4,t.getDescription());
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDebut().getTime());
            ps.setDate(5,sqlDate1 );
            
            java.sql.Date sqlDate2 = new java.sql.Date(t.getDateFin().getTime());
            ps.setDate(6,sqlDate2 );
            
            java.sql.Date sqlDate3 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(7,sqlDate3 );
            
             
             
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }


    @Override
    public void delete(Integer id) {

                   try {
            String req = "delete from projet where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public List<Projet> findByJober(Jober jober) {
           List<Projet> projets = new ArrayList<>();
        try {
            String req = "select * from projet where idprofil=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, jober.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Projet projet = new Projet(rs.getInt(1),rs.getString(3),rs.getString(4),rs.getString(5),rs.getDate(6),rs.getDate(7),rs.getDate(8),new JoberService().findById(rs.getInt(2)));
                projets.add(projet);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return projets;
    }
    
}
