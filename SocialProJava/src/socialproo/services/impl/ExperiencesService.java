/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import socialproo.models.Experience;
import socialproo.models.Jober;
import socialproo.services.interfaces.ICvJober;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class ExperiencesService implements ICvJober<Experience,Integer,Jober> {
   
    private Connection connection;

    public ExperiencesService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Experience t) {
            try {
            String req = "insert into experience (idprofil,organisation,poste,dateDebut,dateFin,dateAjout,description) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getJober().getId());
            
            ps.setString(2, t.getOrganisation());
            ps.setString(3,t.getPoste());
            
            java.sql.Date sqlDate1 = new java.sql.Date(t.getDateDebut().getTime());
            ps.setDate(4,sqlDate1 );
            
            java.sql.Date sqlDate2 = new java.sql.Date(t.getDateFin().getTime());
            ps.setDate(5,sqlDate2 );
            
            java.sql.Date sqlDate3 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(6,sqlDate3 );
            
             ps.setString(7,t.getDescription());
             
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }

    @Override
    public void delete(Integer id) {
            try {
            String req = "delete from Experience where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }   
    }


    @Override
    public List<Experience> findByJober(Jober jober) {
           List<Experience> experiences = new ArrayList<>();
        try {
            String req = "select * from Experience where idprofil=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, jober.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Experience experience = new Experience(rs.getInt(1),new JoberService().findById(rs.getInt(2)),rs.getString(3),rs.getString(4),rs.getDate(5),rs.getDate(6),rs.getDate(7),rs.getString(8));
                experiences.add(experience);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return experiences;
    }

    
    
}
