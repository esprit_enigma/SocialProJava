/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import socialproo.models.ProjetEntreprise;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import socialproo.models.ListProjet;

/**
 *
 * @author ASUS
 */
public class ProjetEntrepriseService implements IService<ProjetEntreprise, Integer> {

    private Connection connection;

    public ProjetEntrepriseService() {
        connection = DataSource.getInstance().getConnection();
    }
    

    @Override
    public void add(ProjetEntreprise t) {
                try{
        String req = "insert into projet_entreprise(id_entreprise, nom,destinaation,description,dateAjoutProjet) VALUES (?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, t.getEntreprise().getId());
            ps.setString(2,t.getNom() );
            ps.setString(3,t.getDestination() );
            ps.setString(4,t.getDescription());
             ps.setString(5,t.getDateAjout() );
           
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void update(ProjetEntreprise t) {
        try{
        String req = "update projet_entreprise set id_entreprise =?, nom=?,destinaation=?,description=?,dateAjoutProjet=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, t.getEntreprise().getId());
            ps.setString(2,t.getNom() );
            ps.setString(3,t.getDestination() );
            ps.setString(4,t.getDescription());
            ps.setString(5,t.getDateAjout() );
            ps.setInt(6,t.getId() );
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
    }
    }

    @Override
    public void delete(Integer id) {
            try {
            String req = "delete from projet_entreprise where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ObservableList<ProjetEntreprise> getAll() {
        ObservableList<ProjetEntreprise> projetentreprises = FXCollections.observableArrayList();
        try {
            String req = "select * from projet_entreprise";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProjetEntreprise projetentreprise =  new ProjetEntreprise(rs.getInt(1),new EntrepriseService().findById(rs.getInt(2)), rs.getString(3),rs.getString(4),rs.getString(5));
                projetentreprises.add(projetentreprise);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return projetentreprises;
    }
     public ObservableList<ListProjet> getList(int id) {
                
                 ObservableList<ListProjet> listeprojets = FXCollections.observableArrayList();
        try {
            String req = "select * from projet_entreprise where id_entreprise = ?";
            
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //new EntrepriseService().findById(rs.getInt(3))
                ListProjet projet = new ListProjet(rs.getInt(1), rs.getString(3),rs.getString(4),rs.getString(5) );
                listeprojets.add(projet);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return listeprojets;
    }


    @Override
    public ProjetEntreprise findById(Integer id) {
               ProjetEntreprise projetentreprise = null;
        try {
            String req = "select * from projet_entreprise where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                projetentreprise = new ProjetEntreprise(rs.getInt(1),new EntrepriseService().findById(rs.getInt(2)), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return projetentreprise;
    }
    public ProjetEntreprise findByIdEntreprise(Integer id) {
       ProjetEntreprise projet = null;
        try {
            String req = "select * from projet_entreprise where id_entreprise =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                projet = new ProjetEntreprise(rs.getInt(1),new EntrepriseService().findById(rs.getInt(2)), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
                
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return projet;
    }
         public XYChart.Series getPie(int id) { 
             XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName("Nombre De Projets");

      
        
             String year = null;
             Calendar cal;
                  java.util.Date d;
             SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd") ;
             //ObservableList<PieChart.Data> data = FXCollections.observableArrayList();
        try {
            String req = "select extract(year from dateAjoutProjet),count(id) as t from projet_entreprise where id_entreprise = ? group by extract(year from dateAjoutProjet)";
            
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("date "+rs.getString(1)+"id "+rs.getInt(2));
                //new EntrepriseService().findById(rs.getInt(3))
                //ListProjet projet = new ListProjet(rs.getInt(1), rs.getString(3),rs.getString(4),rs.getString(5) );
        
               
                System.out.println("date"+year);
                  dataSeries1.getData().add(new XYChart.Data(rs.getString(1),rs.getInt(2)));
              //  data.add(new PieChart.Data( ));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dataSeries1;
    }

    public void saveXLSFile(File file,int id) {
        try {
            //System.out.println("Clicked,waiting to export....");
            
            FileOutputStream fileOut;
            fileOut = new FileOutputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet workSheet = workbook.createSheet("sheet 0");
            Row row1 = workSheet.createRow((short) 0);
            row1.createCell(0).setCellValue("Années");
            row1.createCell(1).setCellValue("Nombre De Projets");
            Row row2;

           try {
            String req = "select extract(year from dateAjoutProjet),count(id) as t from projet_entreprise where id_entreprise = ? group by extract(year from dateAjoutProjet)";
            
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int a = rs.getRow();
                row2 = workSheet.createRow((short) a);
                row2.createCell(0).setCellValue(rs.getString(1));
                row2.createCell(1).setCellValue(rs.getString(2));
            }} catch (SQLException ex) {
            ex.printStackTrace();
        }
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
            
        } catch (IOException e) {
            
            System.err.println(e);

        }
    }
}
