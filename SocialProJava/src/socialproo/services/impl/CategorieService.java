/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Categorie;
import socialproo.services.interfaces.IService;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class CategorieService implements IService<Categorie, Integer> {
    private Connection connection;

    public CategorieService() {
         connection = DataSource.getInstance().getConnection();
    }
    

    @Override
    public void add(Categorie t) {
                try{
        String req = "insert into categorie(nom, dateAjout) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1,t.getNom() );
             ps.setString(2, LocalDate.now().toString());
          
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Categorie t) {
        System.out.println("toukou "+t.getNom());
        try{
        String req = "update categorie set nom=?, dateAjout=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1,t.getNom() );
            //java.sql.Date sqlDate1 = new java.sql.Date(t.getDateAjout().getTime());
             ps.setString(2, LocalDate.now().toString());
            
            ps.setInt(3,t.getId() );
                ps.executeUpdate();
        }catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
            try {
            String req = "delete from categorie where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        }   catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Categorie> getAll() {
        System.out.println("ok");
                List<Categorie> categories = new ArrayList<>();
                
        try {
             
            String req = "select * from categorie";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
           
            while (rs.next()) {
  
                Categorie categorie = new Categorie(rs.getInt(1),rs.getString(2),rs.getString(3));
                categories.add(categorie);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return categories;
    }

    @Override
    public Categorie findById(Integer id) {
                Categorie categorie = null;
        try {
            String req = "select * from categorie where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               
                categorie = new Categorie(rs.getInt(1),rs.getString(2),rs.getString(3));
               
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return categorie;
    }
       public Categorie findByCategorieName(String nom) {
        Categorie categorie = null;
        try {
            String req = "select * from categorie where nom =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
               
                categorie = new Categorie(rs.getInt(1),rs.getString(2),rs.getString(3));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return categorie;
    }
       
    
}
