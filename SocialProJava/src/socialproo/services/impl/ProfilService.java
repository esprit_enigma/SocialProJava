/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Profil;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import socialproo.services.interfaces.IProfilService;

/**
 *
 * @author Alaa
 */
public class ProfilService implements IProfilService {

    private Connection connection;

    public ProfilService() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public void add(Profil p) {

    }

    @Override
    public void update(Profil p) {
    }

    @Override
    public void delete(int id) {
         try {
            String req = "delete from profil where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public List<Profil> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Profil findById(int id) {
        Profil profil = null;
        try {
            String req = "select * from profil where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                profil = new Profil(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getDate(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10), rs.getDate(11), new UserService().findById(rs.getInt(12)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return profil;
    }

    public Profil findByUserId(int id) {
        Profil profil = null;
        try {
            String req = "select * from profil where idUser =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                profil = new Profil(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getDate(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getInt(10), rs.getDate(11), new UserService().findById(rs.getInt(12)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return profil;
    }

}
