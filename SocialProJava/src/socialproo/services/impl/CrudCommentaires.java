/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Commentaires;
import socialproo.services.interfaces.IServiceCrud;
import socialproo.technique.DataSource;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author m_s info
 */
public class CrudCommentaires implements IServiceCrud<Commentaires>{

    private Connection connexion ;
    
    public CrudCommentaires ()
    {
        connexion = DataSource.getInstance().getConnection();
    }
    
    @Override
    public void add(Commentaires t) {
        String req = "insert into commentaires (id_cour,id_user,contenu,date_ajout,date_modification,note) values (?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setInt(1, t.getCours().getId());
            ps.setInt(2, t.getJober().getId());
            ps.setString(3, t.getContenu());
            java.sql.Date sqlDate = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(4, sqlDate);
            ps.setDate(5, null);
            ps.setInt(6, 0);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Commentaires t) {
    String req = "update commentaires set id_cour=?,id_user=?,contenu=?,date_ajout=?,date_modification=?,note=?  where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setInt(1, t.getCours().getId());
            ps.setInt(2, 3);
            ps.setString(3, t.getContenu());
            java.sql.Date sqlDate = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(4, sqlDate);
            ps.setDate(5, null);
            ps.setInt(6, 0);
            ps.setInt(7, t.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    
    }

    @Override
    public void delete(int id) {
          String req = "delete from commentaires where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setInt(1, id);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }  
    }

    @Override
    public List<Commentaires> getAll() {
        List<Commentaires> commentaires = new ArrayList<Commentaires>();
        String req = "select * from commentaires";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Commentaires commentaire = new Commentaires(rs.getInt(1),new CrudCours().findById(rs.getInt(2)),new UserService().findById(rs.getInt(3)) ,rs.getString(4),rs.getDate(5),rs.getDate(6),rs.getInt(7) );
                commentaires.add(commentaire);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
        
        return commentaires;
    }

    @Override
    public Commentaires findById(int id) {
        Commentaires commentaire = null;
        try {
            String req = "select * from commentaires where id =?";
            PreparedStatement ps = connexion.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                commentaire = new Commentaires(rs.getInt(1),new CrudCours().findById(rs.getInt(2)),new UserService().findById(rs.getInt(3)) ,rs.getString(4),rs.getDate(5),rs.getDate(6),rs.getInt(7) );
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return commentaire;
    }
    
    
}
