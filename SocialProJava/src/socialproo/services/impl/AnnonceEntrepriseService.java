/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.AnnonceEntreprise;

import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author IMEN
 */
public class AnnonceEntrepriseService  {
     private Connection connection;

    public AnnonceEntrepriseService() {
        connection = DataSource.getInstance().getConnection();
    }
     

    public void add(AnnonceEntreprise annonceEntreprise) {
        
         try {
            String req = "insert into annonceentreprise(idact,iduser,iddelegation,type_annonce,titre,details,budget,valide) values (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,annonceEntreprise.getActivite().getId());
            ps.setInt(2,annonceEntreprise.getUser());
            ps.setInt(3,annonceEntreprise.getDelegation().getId());
            ps.setString(4,annonceEntreprise.getTypeAnnonce());
            ps.setString(5,annonceEntreprise.getTitre());
            ps.setString(6,annonceEntreprise.getDetails());
            ps.setString(7,annonceEntreprise.getBudget());
            ps.setInt(8,annonceEntreprise.getValide());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

   
    public void update(AnnonceEntreprise annonceEntreprise) {
    try {
      String req = "update annonceentreprise set idact=?,iduser=?,iddelegation=?,type_annonce=?,titre=?,details=?,budget=?,valide=? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,annonceEntreprise.getActivite().getId());
            ps.setInt(2,annonceEntreprise.getUser());
            ps.setInt(3,annonceEntreprise.getDelegation().getId());
            ps.setString(4,annonceEntreprise.getTypeAnnonce());
            ps.setString(5,annonceEntreprise.getTitre());
            ps.setString(6,annonceEntreprise.getDetails());
            ps.setString(7,annonceEntreprise.getBudget());
            ps.setInt(8,annonceEntreprise.getValide());
            ps.setInt(9,annonceEntreprise.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    }

   
   public void delete(Integer id) {
        try {
            String req = "delete from annonceentreprise where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

   
     public List<AnnonceEntreprise> getAll() {
        List<AnnonceEntreprise> entr = new ArrayList<>();
        try {
            String req = "select * from annonceentreprise";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AnnonceEntreprise an;
                an = new AnnonceEntreprise(rs.getInt(1),new ActiviteService().findById(rs.getInt(2)),rs.getInt(3),new DelegationService().findById(rs.getInt(4)),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getInt(9));
                entr.add(an);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return entr;
    }
   
    public AnnonceEntreprise findById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
