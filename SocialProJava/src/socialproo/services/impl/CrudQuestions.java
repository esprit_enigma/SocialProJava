/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.Commentaires;
import socialproo.models.Question;
import socialproo.services.interfaces.IServiceCrud;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author m_s info
 */
public class CrudQuestions implements IServiceCrud<Question>{

    private Connection connexion ;
    
    public CrudQuestions ()
    {
        connexion = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Question t) {
        String req = "insert into question (contenu,id_cour,reponse1,reponse2,reponse_v) values (?,?,?,?,?)";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setString(1, t.getContenu());
            ps.setInt(2, t.getCours().getId());
            ps.setString(3, t.getReponse1());
            ps.setString(4, t.getReponse2());
            ps.setString(5, t.getReponseV());
            
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Question t) {
     String req = "update question set (contenu,id_cour,reponse1,reponse2,reponse_v) values (?,?,?,?,?) where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setString(1, t.getContenu());
            ps.setInt(2, t.getCours().getId());
            ps.setString(3, t.getReponse1());
            ps.setString(4, t.getReponse2());
            ps.setString(5, t.getReponseV());
            
            ps.setInt(6, t.getId());
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }        
    }

    @Override
    public void delete(int id) {
        String req = "delete from question where id = ?";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ps.setInt(1, id);
            ps.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }  
    }

    @Override
    public List<Question> getAll() {
        List<Question> question = new ArrayList<Question>();
        String req = "select * from question";
        try {
            PreparedStatement ps = connexion.prepareStatement(req);
           
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question q = new Question(rs.getInt(1),rs.getString(2),new CrudCours().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),rs.getString(6));
                question.add(q);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
        
        return question;}

    @Override
    public Question findById(int id) {
        Question question = null;
        try {
            String req = "select * from question where id =?";
            PreparedStatement ps = connexion.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            
                question = new Question(rs.getInt(1),rs.getString(2),new CrudCours().findById(rs.getInt(3)),rs.getString(4),rs.getString(5),rs.getString(6));
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return question;
    
    }
    
}
