/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import socialproo.models.Delegation;

import socialproo.models.Nature;
import socialproo.models.Specialite;
import socialproo.services.interfaces.IdelegationSpecialite;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class SpecialiteService implements IdelegationSpecialite<Specialite,Integer,Nature>{
        private Connection connection;

    public SpecialiteService() {
        connection = DataSource.getInstance().getConnection();
    }
       public List<Specialite> getAllByNatureId(int id) {
        List<Specialite> specs = new ArrayList<>();
        try {
            String req = "select * from specialite where idnature=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Specialite s = new Specialite(rs.getInt(1), new NatureService().findById(rs.getInt(2)), rs.getString(3), rs.getBoolean(4), rs.getDate(5));
                specs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return specs;
    }
    @Override
    public void add(Specialite t) {
             try {
            String req = "insert into Specialite (id,idnature,nom,valide,dateAjout) values (?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, t.getId());  
            ps.setInt(2, t.getNature().getId());
            ps.setString(3,t.getNom());
            ps.setBoolean(4, t.getValide());
            java.sql.Date date1 = new java.sql.Date(t.getDateAjout().getTime());
            ps.setDate(5, date1);
                 

            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }  
    }

    @Override
    public void delete(Integer id) {
               try {
            String req = "delete from Specialite where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public Specialite findById(Integer id) {
                  Specialite specialite = null;
        try {
            String req = "select * from Specialite where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
             while(rs.next()){
                Date date1 = new Date(rs.getDate(5).getTime());
                specialite = new Specialite(rs.getInt(1),new NatureService().findById(rs.getInt(2)),rs.getString(3),rs.getBoolean(4),date1);
             }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return specialite; }

    @Override
    public List<Specialite> findBynv(Nature nv) {
                   List<Specialite> specialites = new ArrayList<>();
                   
        try {
            String req = "select * from Specialite where idnature=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setInt(1, nv.getId());
            ResultSet rs = ps.executeQuery();
                while(rs.next()){
                    Date date1= new Date(rs.getDate(5).getTime());
                Specialite specialite = new Specialite(rs.getInt(1),new NatureService().findById(rs.getInt(2)),rs.getString(3),rs.getBoolean(4),date1);
                specialites.add(specialite);
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return specialites;
    }

    @Override
    public List<Specialite> getAll() {
             List<Specialite> specialites = new ArrayList<>();
                   
        try {
            String req = "select * from Specialite ";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ResultSet rs = ps.executeQuery();
                while(rs.next()){
                 Date date1= new Date(rs.getDate(5).getTime());
                Specialite specialite = new Specialite(rs.getInt(1),new NatureService().findById(rs.getInt(2)),rs.getString(3),rs.getBoolean(4),date1);
                specialites.add(specialite);
                }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return specialites;   
    }
        public Specialite findByNom(String nom) {
                  Specialite specialite = null;
        try {
            String req = "select * from Specialite where nom=?";
            PreparedStatement ps = connection.prepareStatement(req);
             ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
             while(rs.next()){
                Date date1 = new Date(rs.getDate(5).getTime());
                specialite = new Specialite(rs.getInt(1),new NatureService().findById(rs.getInt(2)),rs.getString(3),rs.getBoolean(4),date1);
             }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return specialite; 
        }

    @Override
    public List<Delegation> getAllByVilleId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
}
