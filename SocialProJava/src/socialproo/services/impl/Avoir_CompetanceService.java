/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import socialproo.models.Avoir_Competance;
import socialproo.models.Competance;
import socialproo.models.Jober;
import socialproo.models.User;
import socialproo.services.interfaces.ICompetance;
import socialproo.technique.DataSource;

/**
 *
 * @author Le Parrain
 */
public class Avoir_CompetanceService implements ICompetance {
    private Connection connection;

    public Avoir_CompetanceService() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public void add(Avoir_Competance comp) {
            try {
            String req = "insert into avoir_competance(idcompetance,idprofil,niveau,dateAjout) values (?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, comp.getCompetance().getId());
            ps.setInt(2,comp.getJober().getId());
            ps.setInt(3,comp.getNiveau());
            java.sql.Date sqlDate1 = new java.sql.Date(comp.getDateAjout().getTime());
            ps.setDate(4,sqlDate1 );
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void delete(int id) {
              try {
            String req = "delete from avoir_competance where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public void update(Avoir_Competance comp) {
                try {
            String req = "update avoir_competance set niveau=? ,dateAjout=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
       
            ps.setInt(1,comp.getNiveau());
            java.sql.Date sqlDate1 = new java.sql.Date(comp.getDateAjout().getTime());
            ps.setDate(2,sqlDate1 );
            ps.setInt(3,comp.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } 
    }

    @Override
    public List<Avoir_Competance> findbyJober(Jober j) {
             List<Avoir_Competance> competances = new ArrayList<>();
        try {
            String req = "select * from avoir_competance where idprofil=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, j.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
          
                Date date1= new Date(rs.getDate(5).getTime());
           
             Avoir_Competance competance =new Avoir_Competance(rs.getInt(1),new CompetanceService().findById(rs.getInt(2)),new JoberService().findById(rs.getInt(3)),rs.getInt(4),date1);
            competances.add(competance);
            }
            
  
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return competances;   
    }

    @Override
    public Avoir_Competance findbyJoberComp(Jober j, Competance c) {
   Avoir_Competance competance = new Avoir_Competance();
        try {
            String req = "select * from avoir_competance where idprofil=? and idcompetance=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, j.getId());
            ps.setInt(2, c.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
          
                Date date1= new Date(rs.getDate(5).getTime());
           
             competance =new Avoir_Competance(rs.getInt(1),new CompetanceService().findById(rs.getInt(2)),new JoberService().findById(rs.getInt(3)),rs.getInt(4),date1);
            
            }
            
  
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return competance;   
    }
    
}
