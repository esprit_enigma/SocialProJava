/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.impl;

import socialproo.models.AnnonceJober;
import socialproo.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author IMEN
 */
public class AnnonceJoberService  {
 private Connection connection;

    public AnnonceJoberService() {
        connection = DataSource.getInstance().getConnection();
    }
 

  
    public void add(AnnonceJober annonceJober) {
        try {
            String req = "insert into annoncejober(idspecialite,iddelegation,idprofil,type_annonce,titre,type_emploi,description,salaire,niveau,experience,valide) values(?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,annonceJober.getSpecialite().getId());
            ps.setInt(2,annonceJober.getDelegation().getId());
            ps.setInt(3,annonceJober.getProfil());
            ps.setString(4,annonceJober.getTypeAnnonce());
            ps.setString(5,annonceJober.getTitre());
            ps.setString(6,annonceJober.getTypeEmploi());
            ps.setString(7,annonceJober.getDescription());
            ps.setInt(8,annonceJober.getSalaire());
            ps.setString(9,annonceJober.getNiveau());
            ps.setString(10,annonceJober.getExperience());
            ps.setInt(11,annonceJober.getValide());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

 
    public void update(AnnonceJober annonceJober) {
    try {
            String req = "update annoncejober set idspecialite=?,iddelegation=?,idprofil=?,type_annonce=?,titre=?,type_emploi=?,description=?,salaire=?,niveau=?,experience=?,valide=? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1,annonceJober.getSpecialite().getId());
            ps.setInt(2,annonceJober.getDelegation().getId());
            ps.setInt(3,annonceJober.getProfil());
            ps.setString(4,annonceJober.getTypeAnnonce());
            ps.setString(5,annonceJober.getTitre());
            ps.setString(6,annonceJober.getTypeEmploi());
            ps.setString(7,annonceJober.getDescription());
            ps.setInt(8,annonceJober.getSalaire());
            ps.setString(9,annonceJober.getNiveau());
            ps.setString(10,annonceJober.getExperience());
            ps.setInt(11,annonceJober.getValide());
            ps.setInt(12,annonceJober.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    
    }
    

    public void delete(Integer id) {
 try {
            String req = "delete from annoncejober where id =?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }    }

    public List getAll() {
         List<AnnonceJober> entr = new ArrayList<>();
        try {
            String req = "select * from annoncejober";
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AnnonceJober an;
                an = new AnnonceJober(rs.getInt(1),new SpecialiteService().findById(rs.getInt(2)),new DelegationService().findById(rs.getInt(3)),rs.getInt(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getInt(9),rs.getString(10),rs.getString(11));
                entr.add(an);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return entr;    }

    public AnnonceJober findById(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
    
}
