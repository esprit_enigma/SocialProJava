/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Entreprise;
import socialproo.models.Ville;

/**
 *
 * @author Le Parrain
 */
public interface INatureVille<O,I> {
    Ville findByNom(String nom)  ;
    void add(O t);

    void delete(I id);

    List<O> getAll();

    O findById(I id);
}
