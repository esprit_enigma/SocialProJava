/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.Categorie;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface ICategorieService {
    void add(Categorie categorie);

    void update(Categorie categorie);

    void delete(int id);

    List<Categorie> getAll();
    
}
