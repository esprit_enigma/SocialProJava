/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.CodePostal;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface ICodePostalService {
    void add(CodePostal codepostal);

    void update(CodePostal codepostal);

    void delete(int id);

    List<CodePostal> getAll();
    
}
