/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Avoir_Competance;
import socialproo.models.Competance;
import socialproo.models.Jober;

/**
 *
 * @author Le Parrain
 */
public interface ICompetance {
    void add(Avoir_Competance comp);
    void delete(int id);
    void update(Avoir_Competance comp);
    List<Avoir_Competance> findbyJober(Jober j);
    Avoir_Competance findbyJoberComp(Jober j,Competance c);
    
}
