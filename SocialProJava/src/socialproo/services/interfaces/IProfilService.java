/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Profil;

/**
 *
 * @author Alaa
 */
public interface IProfilService {

    void add(Profil p);

    void update(Profil p);

    void delete(int id);

    List<Profil> getAll();

    Profil findById(int id);
}
