/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Relation;

/**
 *
 * @author Alaa
 */
public interface IRelationService {

    void add(Relation r);

    void update(Relation r);

    void delete(int id);

    List<Relation> getAll();

    Relation findById(int id);
}
