/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.DemandesRecomendation;

/**
 *
 * @author Alaa
 */
public interface IDemandesRecomendation {

    void add(DemandesRecomendation dm);

    void update(DemandesRecomendation dm);

    void delete(int id);

    List<DemandesRecomendation> getAll();

    DemandesRecomendation findById(int id);
}
