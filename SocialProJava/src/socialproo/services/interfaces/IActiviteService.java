/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.Activite;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface IActiviteService {
    void add(Activite activite);

    void update(Activite activite);

    void delete(int id);

    List<Activite> getAll();
    
}
