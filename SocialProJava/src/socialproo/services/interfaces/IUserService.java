/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.User;

/**
 *
 * @author Alaa
 */
public interface IUserService {
    void add(User r);

    void update(User r);
    void updateRole(User r);
    void delete(int id);

    List<User> getAll();
    
    User findById(int id);
}
