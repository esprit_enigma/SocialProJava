/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.ProjetEntreprise;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface IProjetEntrepriseService {
    void add(ProjetEntreprise projetentreprise);

    void update(ProjetEntreprise projetentreprise);

    void delete(int id);

    List<ProjetEntreprise> getAll();
    
}
