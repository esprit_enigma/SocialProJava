/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Notification;

/**
 *
 * @author Alaa
 */
public interface INotificationService {

    void add(Notification n);

    void update(Notification n);

    void delete(int id);

    List<Notification> getAll();

    Notification findById(int id);
}
