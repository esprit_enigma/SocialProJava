/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Delegation;


/**
 *
 * @author Le Parrain
 */
public interface IdelegationSpecialite<O,I,NV> {
      List<Delegation> getAllByVilleId(int id) ;
    void add(O t);
    void delete(I id);
    O findById(I id);
    List<O> findBynv(NV nv);
    List<O> getAll();
  
}
