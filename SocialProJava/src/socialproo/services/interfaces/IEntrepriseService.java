/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.Entreprise;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface IEntrepriseService {
    void add(Entreprise entreprise);

    void update(Entreprise entreprise);

    void delete(int id);

    List<Entreprise> getAll();
    
    
}
