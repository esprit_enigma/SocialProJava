/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Jober;

/**
 *
 * @author Le Parrain
 */
public interface INatureVilles<O,I,S> {
     
    void add(O t);

    void delete(I id);

    List<O> getAll();

    O findById(I id);
    O findByNom(S nom);
}
