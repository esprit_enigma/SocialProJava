/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Recomendation;

/**
 *
 * @author Alaa
 */
public interface IRecomendationService {

    void add(Recomendation r);

    void update(Recomendation r);

    void delete(int id);

    List<Recomendation> getAll();

    Recomendation findById(int id);
}
