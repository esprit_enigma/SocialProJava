/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import java.util.List;
import socialproo.models.Delegation;
import socialproo.models.Jober;
import socialproo.models.Specialite;

/**
 *
 * @author Le Parrain
 */
public interface IJober {
        void add(Jober t);

    void update(Jober t);

    void delete(int id);

    List<Jober> getAll();
    List<Jober> findBySpecialite(Specialite spec);
    List<Jober> findByDelegation(Delegation del);
    Jober findById(int id);
}
