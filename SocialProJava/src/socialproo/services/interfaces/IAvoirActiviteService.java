/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.services.interfaces;

import socialproo.models.AvoirActivite;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface IAvoirActiviteService {
    void add(AvoirActivite avoiractivite);

    void update(AvoirActivite avoiractivite);

    void delete(int id);

    List<AvoirActivite> getAll();
    
}
