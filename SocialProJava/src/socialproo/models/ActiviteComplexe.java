/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

/**
 *
 * @author ASUS
 */
public class ActiviteComplexe {
     private String nom;
      private String nomcategorie;
     private String description;
     int id;

    public ActiviteComplexe() {
    }

    public ActiviteComplexe(int id,String description,String nom, String nomcategorie) {
        this.nom = nom;
        this.id=id;
        this.nomcategorie = nomcategorie;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNomcategorie() {
        return nomcategorie;
    }

    public void setNomcategorie(String nomcategorie) {
        this.nomcategorie = nomcategorie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
     
     
    
}
