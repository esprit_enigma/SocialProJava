/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author m_s info
 */
public class Commentaires {
    
    private int id ;
    
    private String contenu ; 
    
    private Date dateAjout ;
    
    private Date datemodification ;
    
    private User jober ;
    
    private Cours cours ;

    public Commentaires(int id, Cours cours, User jober, String contenu, Date dateAjout, Date datemodification, int note) {
 this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
        this.cours = cours;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public Commentaires(int id, Cours cours, User jober, String contenu, Date dateAjout, Date datemodification) {
        this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
        this.cours = cours;
    }

    public Commentaires()
    {
        
    }
    public Commentaires(int id, String contenu, Date dateAjout, Date datemodification, User jober) {
        this.id = id;
        this.contenu = contenu;
        this.dateAjout = dateAjout;
        this.datemodification = datemodification;
        this.jober = jober;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDatemodification() {
        return datemodification;
    }

    public void setDatemodification(Date datemodification) {
        this.datemodification = datemodification;
    }

    public User getJober() {
        return jober;
    }

    public void setJober(User jober) {
        this.jober = jober;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.id;
        hash = 67 * hash + Objects.hashCode(this.contenu);
        hash = 67 * hash + Objects.hashCode(this.dateAjout);
        hash = 67 * hash + Objects.hashCode(this.datemodification);
        hash = 67 * hash + Objects.hashCode(this.jober);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Commentaires other = (Commentaires) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.contenu, other.contenu)) {
            return false;
        }
        if (!Objects.equals(this.dateAjout, other.dateAjout)) {
            return false;
        }
        if (!Objects.equals(this.datemodification, other.datemodification)) {
            return false;
        }
        if (!Objects.equals(this.jober, other.jober)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Commentaires{" + "id=" + id + ", contenu=" + contenu + ", dateAjout=" + dateAjout + ", datemodification=" + datemodification + ", jober=" + jober + ", cours=" + cours + '}';
    }

   
    
    
    
    
}
