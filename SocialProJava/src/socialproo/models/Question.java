/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

import java.util.Objects;

/**
 *
 * @author m_s info
 */
public class Question {
    
    private int id ;
    
    private String contenu ;
    
    private String reponse1;
    
    private String reponse2;
    
    private String reponseV;
    
    private Cours cours;

    public Question(int id, String contenu, Cours cours, String reponse1, String reponse2, String reponseV) {
        this.id = id;
        this.contenu = contenu;
        this.reponse1 = reponse1;
        this.reponse2 = reponse2;
        this.reponseV = reponseV;
        this.cours = cours;
    }
    
    

    public Question(String contenu, String reponse1, String reponse2, String reponseV, Cours cours) {
        this.contenu = contenu;
        this.reponse1 = reponse1;
        this.reponse2 = reponse2;
        this.reponseV = reponseV;
        this.cours = cours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getReponse1() {
        return reponse1;
    }

    public void setReponse1(String reponse1) {
        this.reponse1 = reponse1;
    }

    public String getReponse2() {
        return reponse2;
    }

    public void setReponse2(String reponse2) {
        this.reponse2 = reponse2;
    }

    public String getReponseV() {
        return reponseV;
    }

    public void setReponseV(String reponseV) {
        this.reponseV = reponseV;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + this.id;
        hash = 43 * hash + Objects.hashCode(this.contenu);
        hash = 43 * hash + Objects.hashCode(this.reponse1);
        hash = 43 * hash + Objects.hashCode(this.reponse2);
        hash = 43 * hash + Objects.hashCode(this.reponseV);
        hash = 43 * hash + Objects.hashCode(this.cours);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Question other = (Question) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.contenu, other.contenu)) {
            return false;
        }
        if (!Objects.equals(this.reponse1, other.reponse1)) {
            return false;
        }
        if (!Objects.equals(this.reponse2, other.reponse2)) {
            return false;
        }
        if (!Objects.equals(this.reponseV, other.reponseV)) {
            return false;
        }
        if (!Objects.equals(this.cours, other.cours)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Question{" + "id=" + id + ", contenu=" + contenu + ", reponse1=" + reponse1 + ", reponse2=" + reponse2 + ", reponseV=" + reponseV + ", cours=" + cours + '}';
    }
    
    
    
}
