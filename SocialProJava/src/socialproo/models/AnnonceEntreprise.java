/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

import java.util.Objects;

/**
 *
 * @author IMEN
 */
public class AnnonceEntreprise {
    
    private int id;
    private Activite activite;
    private int user;
    private Delegation delegation;
    private String typeAnnonce;
    private String titre;
    private String details;
    private String budget;
    private int valide =0;

    public AnnonceEntreprise(int id,Activite activite, int user, Delegation delegation, String typeAnnonce, String titre, String details, String budget,int valide) {
       this.id=id;
        this.activite = activite;
        this.user = user;
        this.delegation = delegation;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.details = details;
        this.budget = budget;
        this.valide=valide;
       
    }
    public AnnonceEntreprise(Activite activite, int user, Delegation delegation, String typeAnnonce, String titre, String details, String budget,int valide) {
      
        this.activite = activite;
        this.user = user;
        this.delegation = delegation;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.details = details;
        this.budget = budget;
this.valide=valide;
       
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getUser() {
        return user;
    }



 

    public void setValide(int valide) {
        this.valide = valide;
    }

    public int getValide() {
        return valide;
    }

    public AnnonceEntreprise() {
    }

    public int getId() {
        return id;
    }

    public Activite getActivite() {
        return activite;
    }

  

    public Delegation getDelegation() {
        return delegation;
    }

    public String getTypeAnnonce() {
        return typeAnnonce;
    }

    public String getTitre() {
        return titre;
    }

    public String getDetails() {
        return details;
    }

    public String getBudget() {
        return budget;
    }

  
    public void setId(int id) {
        this.id = id;
    }

    public void setActivite(Activite activite) {
        this.activite = activite;
    }

   

    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;
    }

    public void setTypeAnnonce(String typeAnnonce) {
        this.typeAnnonce = typeAnnonce;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }



    @Override
    public String toString() {
        return "AnnonceEntreprise{" + "id=" + id + ", activite=" + activite + ", user=" + user + ", delegation=" + delegation + ", typeAnnonce=" + typeAnnonce + ", titre=" + titre + ", details=" + details + ", budget=" + budget + ", valide=" + valide + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.id;
      
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnnonceEntreprise other = (AnnonceEntreprise) obj;
      
          if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
}
