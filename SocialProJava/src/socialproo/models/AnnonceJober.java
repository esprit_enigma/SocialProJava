/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

/**
 *
 * @author IMEN
 */
public class AnnonceJober {

    private int id;
    private Delegation delegation;
    private Specialite specialite;
    private int profil;
    private String typeAnnonce;
    private String titre;
    private String typeEmploi;
    private String description;
    private int salaire;
    private String niveau;
    private String experience;
    private int valide;
       public AnnonceJober(String delegation, Specialite specialite, int i, String type, String titre, String typeEmploi, String description, int s, String niveau, String experience, int i0) {
    }

    public AnnonceJober(Delegation delegation, Specialite specialite, int profil, String typeAnnonce, String titre, String typeEmploi, String description, int salaire, String niveau, String experience,int valide) {

        this.delegation = delegation;
        this.specialite = specialite;
        this.profil = profil;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.typeEmploi = typeEmploi;
        this.description = description;
        this.salaire = salaire;
        this.niveau = niveau;
        this.experience = experience;
this.valide=0;
    }
  
public AnnonceJober(int id, Specialite specialite, Delegation delegation, int profil, String typeAnnonce, String titre, String typeEmploi, String description, int salaire, String niveau, String experience) {
        this.id = id;
        this.delegation = delegation;
        this.specialite = specialite;
        this.profil = profil;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.typeEmploi = typeEmploi;
        this.description = description;
        this.salaire = salaire;
        this.niveau = niveau;
        this.experience = experience;
                
    }

    public AnnonceJober(int id, Specialite specialite, Delegation delegation, int profil, String typeAnnonce, String titre, String typeEmploi, String description, int salaire, String niveau, String experience,int valide) {
        this.id = id;
        this.delegation = delegation;
        this.specialite = specialite;
        this.profil = profil;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.typeEmploi = typeEmploi;
        this.description = description;
        this.salaire = salaire;
        this.niveau = niveau;
        this.experience = experience;
                this.valide=0;
    }
    public AnnonceJober( Specialite specialite, Delegation delegation, int profil, String typeAnnonce, String titre, String typeEmploi, String description, int salaire, String niveau, String experience,int valide) {
       
        this.delegation = delegation;
        this.specialite = specialite;
        this.profil = profil;
        this.typeAnnonce = typeAnnonce;
        this.titre = titre;
        this.typeEmploi = typeEmploi;
        this.description = description;
        this.salaire = salaire;
        this.niveau = niveau;
        this.experience = experience;
                this.valide=0;
    }
   

   

    public int getProfil() {
        return profil;
    }

    public void setProfil(int profil) {
        this.profil = profil;
    }

    @Override
    public String toString() {
        return "AnnonceJober{" + "id=" + id + ", delegation=" + delegation + ", specialite=" + specialite + ", profil=" + profil + ", typeAnnonce=" + typeAnnonce + ", titre=" + titre + ", typeEmploi=" + typeEmploi + ", description=" + description + ", salaire=" + salaire + ", niveau=" + niveau + ", experience=" + experience + ", valide=" + valide + '}';
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AnnonceJober other = (AnnonceJober) obj;
        if (this.id != other.id) {
            return false;
        }

        return true;
    }

    

    public void setId(int id) {
        this.id = id;
    }

    public void setDelegation(Delegation delegation) {
        this.delegation = delegation;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }

 

    public void setTypeAnnonce(String typeAnnonce) {
        this.typeAnnonce = typeAnnonce;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setTypeEmploi(String typeEmploi) {
        this.typeEmploi = typeEmploi;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public void setValide(int valide) {
        this.valide = valide;
    }

    public AnnonceJober() {
    }

    public int getId() {
        return id;
    }

    public Delegation getDelegation() {
        return delegation;
    }

    public Specialite getSpecialite() {
        return specialite;
    }

   

    public String getTypeAnnonce() {
        return typeAnnonce;
    }

    public String getTitre() {
        return titre;
    }

    public String getTypeEmploi() {
        return typeEmploi;
    }

    public String getDescription() {
        return description;
    }

    public int getSalaire() {
        return salaire;
    }

    public String getNiveau() {
        return niveau;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public int getValide() {
        return valide;
    }

}
