/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

/**
 *
 * @author ASUS
 */
public class ListProjet {
     private String nom;
      private String client;
     private String description;
     int id;

    public ListProjet() {
    }

    public ListProjet(int id,String nom, String client, String description) {
        this.id=id;
        this.nom = nom;
        this.client = client;
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
     

    
}
