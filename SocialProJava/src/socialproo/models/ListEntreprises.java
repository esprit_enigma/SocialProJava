/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

import java.util.Date;

/**
 *
 * @author ASUS
 */
public class ListEntreprises {
    int id;
    String username;
    String email;
    String nom;
    int telephone;
    String fichiervalidation;
    String datebloque;
    String dateajout;
    boolean upload;
    boolean valide;
    String role;
    int iduser;

    public ListEntreprises(int id, String username, String email, String nom, int telephone) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.nom = nom;
        this.telephone = telephone;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public ListEntreprises(int iduser,int id, String username, String email, String nom, int telephone,String datebloque,String dateajout,boolean upload,boolean valide,String role) {
        this.id = id;
        this.iduser=iduser;
        this.username = username;
        this.email = email;
        this.nom = nom;
        this.telephone = telephone;
        this.role=role;
        this.dateajout=dateajout;
        this.datebloque=datebloque;
        this.upload=upload;
        this.valide=valide;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getFichiervalidation() {
        return fichiervalidation;
    }

    public void setFichiervalidation(String fichiervalidation) {
        this.fichiervalidation = fichiervalidation;
    }

    public String getDatebloque() {
        return datebloque;
    }

    public void setDatebloque(String datebloque) {
        this.datebloque = datebloque;
    }

    public String getDateajout() {
        return dateajout;
    }

    public void setDateajout(String dateajout) {
        this.dateajout = dateajout;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    

    @Override
    public String toString() {
        return "ListEntreprises{" + "id=" + id + ", username=" + username + ", email=" + email + ", nom=" + nom + ", telephone=" + telephone + ", fichiervalidation=" + fichiervalidation + ", datebloque=" + datebloque + ", dateajout=" + dateajout + ", upload=" + upload + ", valide=" + valide + ", role=" + role + '}';
    }

    
    

    
    
}
