/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialproo.models;

import java.util.Date;

/**
 *
 * @author Alaa
 */
public class Profil {

    private int id;
    private int idDelegation;
    private int idSpecialite;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String address;
    private String sexe;
    private String Description;
    private int numTel;
    private Date dateCreation;
    private User idUser;

    public Profil() {
    }

    public Profil(int id) {
        this.id = id;
    }

    public Profil(int idDelegation, int idSpecialite, String nom, String prenom, Date dateNaissance, String address, String sexe, String Description, int numTel, Date dateCreation, User idUser) {
        this.idDelegation = idDelegation;
        this.idSpecialite = idSpecialite;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.address = address;
        this.sexe = sexe;
        this.Description = Description;
        this.numTel = numTel;
        this.dateCreation = dateCreation;
        this.idUser = idUser;
    }

    public Profil(int id, int idDelegation, int idSpecialite, String nom, String prenom, Date dateNaissance, String address, String sexe, String Description, int numTel, Date dateCreation, User idUser) {
        this.id = id;
        this.idDelegation = idDelegation;
        this.idSpecialite = idSpecialite;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.address = address;
        this.sexe = sexe;
        this.Description = Description;
        this.numTel = numTel;
        this.dateCreation = dateCreation;
        this.idUser = idUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdDelegation() {
        return idDelegation;
    }

    public void setIdDelegation(int idDelegation) {
        this.idDelegation = idDelegation;
    }

    public int getIdSpecialite() {
        return idSpecialite;
    }

    public void setIdSpecialite(int idSpecialite) {
        this.idSpecialite = idSpecialite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return "Profil{" + "id=" + id + ", idDelegation=" + idDelegation + ", idSpecialite=" + idSpecialite + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", address=" + address + ", sexe=" + sexe + ", Description=" + Description + ", numTel=" + numTel + ", dateCreation=" + dateCreation + ", idUser=" + idUser + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Profil other = (Profil) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

}
