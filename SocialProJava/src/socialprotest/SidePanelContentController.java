/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialprotest;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class SidePanelContentController implements Initializable {
    @FXML
    private VBox box;
    @FXML
    private JFXButton btnProfile;
    @FXML
    private JFXButton btnlocalisation;
    @FXML
    private JFXButton btnfonctionnalite;
    
    
    @FXML
    private JFXButton btnreseaux;
    @FXML
    private JFXButton btnannonce;
    @FXML
    private JFXButton btnrecom;
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
   

    @FXML
    private void onClickProfile(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource("/socialpro/GUI/Home.fxml").openStream());
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);
} 
        @FXML
    void onClicklocalisation(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource("/socialpro/GUI/AjouterLocalisation.fxml").openStream());
          
        } catch (IOException e) {
            
        }
            BorderPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);
    }
        @FXML
    void onClickFonctionnalite(ActionEvent event) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource("/socialpro/GUI/Fonctionnalite.fxml").openStream());
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);
    }
    //AjouterReseauxSociaux
       @FXML
    void onClickReseaux(ActionEvent event) {
               FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            fxmlLoader.load(getClass().getResource("/socialpro/GUI/AjouterReseauxSociaux.fxml").openStream());
        } catch (IOException e) {
            
        }
           BorderPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);

    }
     
    
    
}
