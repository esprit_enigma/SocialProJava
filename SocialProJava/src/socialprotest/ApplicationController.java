/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 */
package socialprotest;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import media.UserIdMedia;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ApplicationController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private JFXHamburger hamburger;
    @FXML
    private JFXButton btnLogOut;
    
    @FXML
    private StackPane acContent;
    
    @FXML
    private AnchorPane acDashBord;
    @FXML
    private Label lblUsrRole;
    @FXML
    private Label lblUserId;
    @FXML
    private AnchorPane acHead;
    
    public static StackPane rootP;
    String role;
    int id;
    
    private UserIdMedia usrNameMedia;

    public UserIdMedia getUsrNameMedia() {
        return usrNameMedia;
    }

    public void setUserIdMedia(UserIdMedia usrNameMedia) {
       //lblUserId.setText(String.valueOf(usrNameMedia.getId()));
       //lblUsrRole.setText(usrNameMedia.getRole());
        id = usrNameMedia.getId();
        role = usrNameMedia.getRole();

        this.usrNameMedia = usrNameMedia;
    }

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rootP = acContent;
        
        try {
            VBox box = FXMLLoader.load(getClass().getResource("SidePanelContent.fxml"));
            drawer.setSidePane(box);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        
        
        
        HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition(hamburger);
        transition.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            transition.setRate(transition.getRate()*-1);
            
            transition.play();
            
            
            if(drawer.isShown())
            {
                drawer.close();
            }else
                drawer.open();
        });
    }
        @FXML
    private void btnLogOut(ActionEvent event) throws IOException {
        acContent.getChildren().clear();
        acContent.getChildren().add(FXMLLoader.load(getClass().getResource("/socialpro/GUI/Login.fxml")));
        
        drawer.getChildren().clear();
       acHead.getChildren().clear();
       // acHead.setVisible(false);
       acHead.setMaxHeight(0.0);
    }

}
