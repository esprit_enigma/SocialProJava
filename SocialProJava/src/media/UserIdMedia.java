/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package media;

/**
 *
 * @author ASUS
 */
public class UserIdMedia {
   int Id;
   String role;

    public UserIdMedia() {
    }

    public UserIdMedia(int Id, String role) {
        this.Id = Id;
        this.role = role;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
   
    
}
