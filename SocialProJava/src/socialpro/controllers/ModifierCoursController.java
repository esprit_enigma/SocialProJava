/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Cours;
import socialproo.models.Entreprise;
import socialproo.models.Specialite;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import socialproo.services.impl.CrudCours;
import socialproo.services.impl.SpecialiteService;

/**
 * FXML Controller class
 *
 * @author Seddik
 */
public class ModifierCoursController implements Initializable {
@FXML
    private ImageView imagev;

    @FXML
    private JFXTextField image;

    @FXML
    private JFXTextField pdf;

    @FXML
    private JFXComboBox<?> specialite;

    @FXML
    private JFXComboBox<?> id;
    
    @FXML
    private JFXTextField titre;

    @FXML
    private JFXTextField video;
    
    @FXML
    private JFXButton ajout;
        
    @FXML
    private JFXTextArea description;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
            id.setPromptText("choisissez l'id du cours");
           
        Image img = new Image("Ressources/Icon_Livre.png");
        imagev.setImage(img);
        
        ImageView ajoutimg = new ImageView("Ressources/53327.png");
                
                ajoutimg.setFitWidth(80);
                ajoutimg.setFitHeight(80);
                ajout.setGraphic(ajoutimg);
                
                
    SpecialiteService ss = new SpecialiteService();
        List<Specialite> lst = new ArrayList();
        lst = ss.getAll();
        
        List <Cours> lstCours = new CrudCours().getAll();
        ObservableList listId = FXCollections.observableArrayList();
       lstCours.forEach(e -> listId.add(e.getId()) );
        
        id.setItems(listId);
        ObservableList listespec = FXCollections.observableArrayList();
       lst.forEach(e -> listespec.add(e.getNom()) );
        
        specialite.setItems(listespec);            
        
       
        ajout.setOnAction(e->
                {
                    modifiercours();
                });
    }    
    
    @FXML
    void modifiercours() {
        String titre1 = titre.getText();
        
       String video1 = video.getText();
       
       String image1 = image.getText();
       
       String spec1 = (String) specialite.getValue();
       
       String pdf1 = pdf.getText();
       
       String description1 = description.getText();
      
       if ((!titre1.equals(""))&&(!video1.equals(""))&&(!image1.equals(""))&&(!spec1.equals(""))&&(!description1.equals(""))&&(!pdf1.equals("")))
       {
           Entreprise e = new Entreprise();
        e.setId(1);
        Date d = new Date();
        java.sql.Date sqlDate = new java.sql.Date(d.getTime());

        
        Cours c = new Cours();
           System.out.println(id.getValue().toString());
       new CrudCours().update(c);
           Alert alert = new Alert(Alert.AlertType.INFORMATION,"Cours modifié avec succé",ButtonType.CLOSE);
           alert.show();
       }
       else 
       {
           Alert alert = new Alert(Alert.AlertType.ERROR,"veuillez remplir tous les champs",ButtonType.CLOSE);
           alert.show();
       }
    }
    
    
}
