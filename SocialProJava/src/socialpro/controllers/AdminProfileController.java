/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Delegation;
import socialproo.models.DemandesRecomendation;
import socialproo.models.Nature;
import socialproo.models.Notification;
import socialproo.models.Profil;
import socialproo.models.Recomendation;
import socialproo.models.Relation;
import socialproo.models.Specialite;
import socialproo.models.User;
import socialproo.models.Ville;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.textfield.TextFields;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.DemandesRecomendationService;
import socialproo.services.impl.NatureService;
import socialproo.services.impl.NotificationService;
import socialproo.services.impl.ProfilService;
import socialproo.services.impl.RecomendationService;
import socialproo.services.impl.RelationService;
import socialproo.services.impl.SpecialiteService;
import socialproo.services.impl.UserService;
import socialproo.services.impl.VilleService;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class AdminProfileController implements Initializable {

    @FXML
    JFXListView MenuListView;
    @FXML
    Label signoutlbl, users, notif, nature, ville, delegation, spec, delegajouterlabel, villeajouterlabel, entreprise,natureajouterlabel, specajouterlabel;
    @FXML
    JFXTextField nomville, nomdelegtf, nomnature, nomspectf;
    @FXML
    JFXButton ajoutvillebtn;
    @FXML
    VBox vboxnotif, vboxnature, vboxdeleg, vboxville, vboxspec, vboxusers, vboxentreprise;
    @FXML
    ComboBox villecombobox, naturecombobox;
    @FXML
    TableView naturetable, villetable, delegtable, spectable, userstable;

    @FXML
    ImageView logo;
    @FXML
    TextField usertextfield;

    String Clicked = "";

    Stage stage;
    Scene scene;

    @FXML
    private StackPane transparentN;
    Parent root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        List<User> lu = new UserService().getAll();
        List<String> ls = new ArrayList<>();
        for (User u : lu) {
            if (u.getUsername() != null) {
                ls.add(u.getUsername());
            }

        }
        TextFields.bindAutoCompletion(usertextfield, ls);

        vboxnotif.setAlignment(Pos.TOP_CENTER);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(false);
        vboxspec.setVisible(false);
        vboxusers.setVisible(false);
        vboxentreprise.setVisible(false);
        //Ville comboBox
        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();
        ObservableList<String> villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i.getNom()));
        villecombobox.setItems(villechoices);

        //Nature comboBox
        NatureService ns = new NatureService();
        List<Nature> ln = ns.getAll();
        ObservableList<String> naturechoices = FXCollections.observableArrayList();
        ln.forEach(i -> naturechoices.add(i.getNom()));
        naturecombobox.setItems(naturechoices);

        UserstableUpdate();
        VilletableUpdate();
        DelegtableUpdate();
        NaturetableUpdate();
        SpectableUpdate();

        notif = new Label("Notifications");
        nature = new Label("Natures");
        ville = new Label("Villes");
        delegation = new Label("Delegations");
        spec = new Label("Specialites");
        users = new Label("Users");
 entreprise = new Label("Entreprises");
        MenuListView.getItems().add(notif);
        MenuListView.getItems().add(users);
        MenuListView.getItems().add(ville);
        MenuListView.getItems().add(delegation);
        MenuListView.getItems().add(nature);
        MenuListView.getItems().add(spec);
        MenuListView.getItems().add(entreprise);

    }

    public void handleMouseClick() {
        if (MenuListView.getSelectionModel().getSelectedItem() == notif) {
            showNotif();
            Clicked = "notif";
        } else if (MenuListView.getSelectionModel().getSelectedItem() == nature) {
            showNature();
        } else if (MenuListView.getSelectionModel().getSelectedItem() == entreprise) {
            try {
                showEntreprises();
            } catch (IOException ex) {
                Logger.getLogger(AdminProfileController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        else if (MenuListView.getSelectionModel().getSelectedItem() == ville) {
            showVille();
        } else if (MenuListView.getSelectionModel().getSelectedItem() == delegation) {
            showDeleg();
        } else if (MenuListView.getSelectionModel().getSelectedItem() == spec) {
            showSpec();
        } else if (MenuListView.getSelectionModel().getSelectedItem() == users) {
            showUsers();
        }
    }

    public void showNotif() {
 transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxnotif);
        vboxnotif.setVisible(true);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(false);
        vboxspec.setVisible(false);
        vboxusers.setVisible(false);

        if (!"notif".equals(Clicked)) {
            NotificationService ns = new NotificationService();
            List<Notification> listnotif = ns.getAllByUser(LoginController.getCurrentUser().getId());
            for (Notification n : listnotif) {
                Label l = new Label(n.getText());
                l.setPadding(new Insets(8, 0, 8, 0));
                l.setStyle("-fx-effect: dropshadow(three-pass-box,  #132736, 10, 0, 0, 0);"
                        + "-fx-font-family: Cambria,'Helvetica Neue',Arial,sans-serif;"
                        + "-fx-text-fill: #1304db;"
                        + "-fx-font-size: 17px;");
                vboxnotif.getChildren().add(l);
            }
        }

    }

    public void showUsers() {
        transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxusers);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(false);
        vboxspec.setVisible(false);
        vboxusers.setVisible(true);
    }

    public void DeleteUser() {
        if (userstable.getSelectionModel().getSelectedItem() != null) {
            User u = (User) userstable.getSelectionModel().getSelectedItem();

            //delete demande recommandation
            DemandesRecomendationService drs = new DemandesRecomendationService();
            List<DemandesRecomendation> ldr = drs.getAll();
            for (DemandesRecomendation d : ldr) {
                if (d.getExpediteur_id() == u || d.getRecepteur_id() == u) {
                    drs.delete(d.getId());
                }
            }

            //delete recommandation
            RecomendationService rs = new RecomendationService();
            List<Recomendation> lr = rs.getAll();
            for (Recomendation r : lr) {
                if (r.getJobber_id() == u || r.getEntreprise_id() == u) {
                    rs.delete(r.getId());
                }
            }

            //delete relation
            RelationService rss = new RelationService();
            List<Relation> lrr = rss.getAll();
            for (Relation rr : lrr) {
                if (rr.getIdfollowed() == u) {
                    rss.delete(rr.getId());
                }

                if (rr.getIdfollower() == u) {
                    rss.delete(rr.getId());
                }
            }

            //delete profile
            ProfilService ps = new ProfilService();
            Profil p = ps.findByUserId(u.getId());
            if (p != null) {
                ps.delete(p.getId());
            }

            UserService us = new UserService();
            us.delete(u.getId());
        }
        UserstableUpdate();
    }

    public void Usersearch() {

        if (usertextfield.getText().length() > 0) {
            userstable.getColumns().clear();
            UserService us = new UserService();
            List<User> lu = new ArrayList<>();
            lu.add(us.findByUsername(usertextfield.getText()));

            if (lu.isEmpty() == false) {
                TableColumn<User, Integer> uidcolumn = new TableColumn<>("Id");
                uidcolumn.setMinWidth(50);
                uidcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

                TableColumn<User, String> uusernamecolumn = new TableColumn<>("Username");
                uusernamecolumn.setMinWidth(50);
                uusernamecolumn.setCellValueFactory(new PropertyValueFactory<>("username"));

                TableColumn<User, String> uemailcolumn = new TableColumn<>("Email");
                uemailcolumn.setMinWidth(50);
                uemailcolumn.setCellValueFactory(new PropertyValueFactory<>("email"));

                TableColumn<User, String> urolecolumn = new TableColumn<>("Role");
                urolecolumn.setMinWidth(50);
                urolecolumn.setCellValueFactory(new PropertyValueFactory<>("roles"));

                ObservableList<User> userschoices = FXCollections.observableArrayList();
                lu.forEach(i -> userschoices.add(i));

                userstable.setItems(userschoices);
                userstable.getColumns().addAll(uidcolumn, uusernamecolumn, uemailcolumn, urolecolumn);
            }

        } else if (usertextfield.getText().length() == 0) {
            UserstableUpdate();
        }

    }

    public void UserstableUpdate() {
        userstable.getColumns().clear();

        UserService us = new UserService();
        List<User> lu = us.getAll();

        TableColumn<User, Integer> uidcolumn = new TableColumn<>("Id");
        uidcolumn.setMinWidth(70);
        uidcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<User, String> uusernamecolumn = new TableColumn<>("Username");
        uusernamecolumn.setMinWidth(150);
        uusernamecolumn.setCellValueFactory(new PropertyValueFactory<>("username"));

        TableColumn<User, String> uemailcolumn = new TableColumn<>("Email");
        uemailcolumn.setMinWidth(200);
        uemailcolumn.setCellValueFactory(new PropertyValueFactory<>("email"));

        TableColumn<User, String> urolecolumn = new TableColumn<>("Role");
        urolecolumn.setMinWidth(140);
        urolecolumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getClearRoles()));

//urolecolumn.setCellValueFactory(new PropertyValueFactory<>("roles"));
        ObservableList<User> userschoices = FXCollections.observableArrayList();
        lu.forEach(i -> userschoices.add(i));

        userstable.setItems(userschoices);
        userstable.getColumns().addAll(uidcolumn, uusernamecolumn, uemailcolumn, urolecolumn);
    }

    public void showVille() {
    transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxville);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(true);
        vboxspec.setVisible(false);
        vboxusers.setVisible(false);
    }

    public void AjoutVille() {
        String sss = nomville.getText();
        System.out.println(sss);
        if (sss.length() > 0) {
            VilleService vs = new VilleService();
            Ville v = new Ville(sss);
            vs.add(v);
            villeajouterlabel.setText("Ville " + sss + " ajouter");
        }
        VilletableUpdate();
    }

    public void DeleteVille() {
        if (villetable.getSelectionModel().getSelectedItem() != null) {
            Ville v = (Ville) villetable.getSelectionModel().getSelectedItem();

            DelegationService ds = new DelegationService();
            List<Delegation> ld = ds.getAllByVilleId(v.getId());
            for (Delegation d : ld) {
                ds.delete(d.getId());
            }
            VilleService vs = new VilleService();
            vs.delete(v.getId());
        }

        VilletableUpdate();

    }

    public void VilletableUpdate() {
        villetable.getColumns().clear();

        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();

        TableColumn<Ville, Integer> vidcolumn = new TableColumn<>("Id");
        vidcolumn.setMinWidth(150);
        vidcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Ville, String> vnomcolumn = new TableColumn<>("Nom");
        vnomcolumn.setMinWidth(150);
        vnomcolumn.setCellValueFactory(new PropertyValueFactory<>("nom"));

        ObservableList<Ville> villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i));

        villetable.setItems(villechoices);
        villetable.getColumns().addAll(vidcolumn, vnomcolumn);
    }

    public void showDeleg() {
        transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxdeleg);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(true);
        vboxville.setVisible(false);
        vboxspec.setVisible(false);
        vboxusers.setVisible(false);
    }

    public void AjoutDeleg() {
        String sss = nomdelegtf.getText();
        String ss = (String) villecombobox.getValue();
        if (sss.length() > 0 && ss.length() > 0) {
            VilleService vs = new VilleService();
            DelegationService ds = new DelegationService();
            Ville v = vs.findByNom(ss);
            Delegation d = new Delegation(v, sss);
            ds.add(d);
            delegajouterlabel.setText("Delegation " + sss + " ajouter");
        }

        DelegtableUpdate();
    }

    public void DeleteDeleg() {
        if (delegtable.getSelectionModel().getSelectedItem() != null) {
            Delegation d = (Delegation) delegtable.getSelectionModel().getSelectedItem();
            DelegationService ds = new DelegationService();
            ds.delete(d.getId());
        }

        DelegtableUpdate();
    }

    public void DelegtableUpdate() {

        delegtable.getColumns().clear();

        DelegationService ds = new DelegationService();
        List<Delegation> ld = ds.getAll();

        TableColumn<Delegation, Integer> didcolumn = new TableColumn<>("Id");
        didcolumn.setMinWidth(100);
        didcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Delegation, String> dnomcolumn = new TableColumn<>("Nom");
        dnomcolumn.setMinWidth(150);
        dnomcolumn.setCellValueFactory(new PropertyValueFactory<>("nom"));

        TableColumn<Delegation, String> dvillecolumn = new TableColumn<>("Ville");
        dvillecolumn.setMinWidth(150);
        dvillecolumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getVille().getNom()));

        ObservableList<Delegation> delegationschoices = FXCollections.observableArrayList();
        ld.forEach(i -> delegationschoices.add(i));

        delegtable.setItems(delegationschoices);
        delegtable.getColumns().addAll(didcolumn, dnomcolumn, dvillecolumn);
    }

    public void showNature() {
        transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxnature);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(true);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(false);
        vboxspec.setVisible(false);
        vboxusers.setVisible(false);
    }
    public void showEntreprises() throws IOException{
    
        System.out.println("ENTREPRISESSS");
//        FXMLLoader fxmlLoader = new FXMLLoader();
//                   fxmlLoader.setLocation(getClass().getResource("/GUI/ValiderEntreprises.fxml"));
//
//            fxmlLoader.load();
//        
//            
//            Parent parent = fxmlLoader.getRoot();
//            Scene scene = new Scene(parent);
//            scene.setFill(new Color(0, 0, 0, 0));
//          
//         
//            Stage nStage = new Stage();
//            nStage.setScene(scene);
//            nStage.initModality(Modality.APPLICATION_MODAL);
//            nStage.initStyle(StageStyle.TRANSPARENT);
//            nStage.show();
FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/ValiderEntreprises.fxml"));

            fxmlLoader.load();
        
       
      AnchorPane acPane = fxmlLoader.getRoot();

        transparentN.getChildren().clear();

        transparentN.getChildren().add(acPane);
    
    
    }

    public void AjoutNature() {
        String sss = nomnature.getText();
        if (sss.length() > 0) {
            NatureService ns = new NatureService();
            Date date = new Date();
            java.sql.Date d = new java.sql.Date(date.getTime());
            Nature n = new Nature(sss, d);
            ns.add(n);
            natureajouterlabel.setText("Nature " + sss + " ajouter");
        }
        NaturetableUpdate();
    }

    public void DeleteNature() {
        if (naturetable.getSelectionModel().getSelectedItem() != null) {
            Nature n = (Nature) naturetable.getSelectionModel().getSelectedItem();

            SpecialiteService ss = new SpecialiteService();
            List<Specialite> ls = ss.getAllByNatureId(n.getId());
            for (Specialite s : ls) {
                ss.delete(s.getId());
            }
            NatureService ns = new NatureService();
            ns.delete(n.getId());
        }

        NaturetableUpdate();
    }

    public void NaturetableUpdate() {
        naturetable.getColumns().clear();

        NatureService ns = new NatureService();
        List<Nature> ln = ns.getAll();
        TableColumn<Nature, Integer> idcolumn = new TableColumn<>("Id");
        idcolumn.setMinWidth(130);
        idcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Nature, String> nomcolumn = new TableColumn<>("Nom");
        nomcolumn.setMinWidth(130);
        nomcolumn.setCellValueFactory(new PropertyValueFactory<>("nom"));

        TableColumn<Nature, Date> datecolumn = new TableColumn<>("Date");
        datecolumn.setMinWidth(130);
        datecolumn.setCellValueFactory(new PropertyValueFactory<>("dateAjout"));

        ObservableList<Nature> naturechoices2 = FXCollections.observableArrayList();
        ln.forEach(i -> naturechoices2.add(i));

        naturetable.setItems(naturechoices2);
        naturetable.getColumns().addAll(idcolumn, nomcolumn, datecolumn);
    }

    public void showSpec() {
             transparentN.getChildren().clear();
 transparentN.getChildren().add(vboxspec);
        vboxnotif.setVisible(false);
        vboxnature.setVisible(false);
        vboxdeleg.setVisible(false);
        vboxville.setVisible(false);
        vboxspec.setVisible(true);
        vboxusers.setVisible(false);
    }

    public void AjoutSpec() {
        String sss = nomspectf.getText();
        String ssss = (String) naturecombobox.getValue();
        if (sss.length() > 0 && ssss.length() > 0) {
            NatureService ns = new NatureService();
            SpecialiteService ss = new SpecialiteService();
            Nature n = ns.findByNom(ssss);
            Date date = new Date();
            java.sql.Date d = new java.sql.Date(date.getTime());
            Specialite s = new Specialite(n, sss, true, d);
            ss.add(s);
            specajouterlabel.setText("Specialite " + sss + " ajouter");
        }
        SpectableUpdate();
    }

    public void DeleteSpec() {
        if (spectable.getSelectionModel().getSelectedItem() != null) {
            Specialite s = (Specialite) spectable.getSelectionModel().getSelectedItem();
            SpecialiteService ss = new SpecialiteService();
            ss.delete(s.getId());
        }

        SpectableUpdate();
    }

    public void SpectableUpdate() {
        spectable.getColumns().clear();

        SpecialiteService ss = new SpecialiteService();
        List<Specialite> ls = ss.getAll();

        TableColumn<Specialite, Integer> sidcolumn = new TableColumn<>("Id");
        sidcolumn.setMinWidth(100);
        sidcolumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Specialite, String> snomcolumn = new TableColumn<>("Nom");
        snomcolumn.setMinWidth(150);
        snomcolumn.setCellValueFactory(new PropertyValueFactory<>("nom"));

        TableColumn<Specialite, String> snaturecolumn = new TableColumn<>("Nature");
        snaturecolumn.setMinWidth(150);
        snaturecolumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getNature().getNom()));

        ObservableList<Specialite> Specialitechoice = FXCollections.observableArrayList();
        ls.forEach(i -> Specialitechoice.add(i));

        spectable.setItems(Specialitechoice);
        spectable.getColumns().addAll(sidcolumn, snomcolumn, snaturecolumn);
    }

    public void SignOut() throws IOException {
        stage = (Stage) notif.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }
}
