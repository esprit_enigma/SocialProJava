/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import socialproo.models.Activite;
import socialproo.models.AvoirActivite;
import socialproo.models.Categorie;
import socialproo.models.Entreprise;
import socialproo.services.impl.ActiviteService;
import socialproo.services.impl.AvoirActiviteService;
import socialproo.services.impl.CategorieService;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.interfaces.ICategorieService;
import socialproo.services.interfaces.IService;
import socialprotest.ApplicationController;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterActiviteController implements Initializable {

    @FXML
    private JFXComboBox<String> cbCategorie;
    @FXML
    
    private JFXTextField tfNom;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    public JFXButton btnModifier;
    @FXML
    public JFXButton btnAjouter;
    @FXML
    public Button btnClose;
  
    String categorieName;
    ApplicationController apCotroller = new ApplicationController();

    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    AvoirActivite avoiract = new AvoirActivite();
    Activite act = new Activite();
    AvoirActiviteService avoiractiviteservice = new AvoirActiviteService();
    int actviteId;
    AvoirActivite av = new AvoirActivite();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void cbCategorieOnAction(MouseEvent event) {
        List<Categorie> categories = new ArrayList<>();
        cbCategorie.getItems().clear();
        cbCategorie.setPromptText(null);
        Categorie categori = new Categorie();
        IService categorieservice = new CategorieService();
        categories = categorieservice.getAll();
        System.out.println(categories);
        for (int i = 0; i < categories.size(); i++) {
            categorieName = categories.get(i).getNom();
            cbCategorie.getItems().remove(categorieName);
            cbCategorie.getItems().add(categorieName);

        }
    }

    @FXML
    private void btnmodificationAction(ActionEvent event) {
        System.out.println("Clicked");
        String t = null;

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        System.out.println("entrep activite "+entreprise);

        //actviteId = avoiractiviteservice.findByIdEntreprise(entreprise.getId()).getActivite().getId();
        System.out.println("woooooooooooooooooooooooooooooooh"+actviteId);
        av = avoiractiviteservice.findByIdActivite(actviteId);
        System.out.println("avselected"+av);
        Activite activite = new Activite();
        ActiviteService as = new ActiviteService();
        activite = as.findById(actviteId);
        activite.setNom(tfNom.getText().trim());
        System.out.println("tacou " + activite.getNom());
        as.update(activite);
        int catId;
        catId = activite.getCategorie().getId();
        System.out.println("taktak " + catId);
        Categorie cat = new Categorie();
        CategorieService cats = new CategorieService();
        cat = cats.findById(catId);
        System.out.println("cat"+cat);
        if (!cbCategorie.getSelectionModel().isEmpty()) {
System.out.println("catsel"+cbCategorie.getSelectionModel());
            cat.setNom(cbCategorie.getSelectionModel().getSelectedItem());
            System.out.println("catmodnom"+cat);
            cats.update(cat);

        } else if (!cbCategorie.getPromptText().isEmpty()) {

            cat.setNom(cbCategorie.getPromptText());
                        System.out.println("tpassel " + cat.getNom());
            cats.update(cat);
        }

        av.setDescription(taDescription.getText().trim());

        

        System.out.println("avvvvv" + av);
        avoiractiviteservice.update(av);
        TrayNotification tn = new TrayNotification("MODIFICATION ACTIVITE", "Modification Effectuée Avec Succées", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.POPUP);
                tn.showAndDismiss(Duration.seconds(2));

    }

    @FXML
    private void btnAjouterAction(ActionEvent event) {
        Categorie categorie = new Categorie();
        CategorieService categorieservice = new CategorieService();
        Activite activite = new Activite();
        ActiviteService activiteService = new ActiviteService();
        activite.setNom(tfNom.getText());
        categorie = categorieservice.findByCategorieName(cbCategorie.getSelectionModel().getSelectedItem());
        activite.setCategorie(categorie);
        System.out.println("activite" + activite.getId());
        activiteService.add(activite);

        act = activiteService.findByNom(activite.getNom());
        actviteId = act.getId();
        AvoirActivite avoiractivite = new AvoirActivite();
        AvoirActiviteService avoiractiviteservice = new AvoirActiviteService();
        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        avoiractivite.setDescription(taDescription.getText());
        avoiractivite.setActivite(act);
        avoiractivite.setEntreprise(entreprise);
        
        avoiractiviteservice.add(avoiractivite);
       
        TrayNotification tn = new TrayNotification("AJOUT ACTIVITE", "Ajout Effectué Avec Succés", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.POPUP);
                tn.showAndDismiss(Duration.seconds(2));
        

    }

    public void btnCloseOnAction(ActionEvent actionEvent) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    public void showDetails(int id) {

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        //actviteId = avoiractiviteservice.findByIdEntreprise(entreprise.getId()).getActivite().getId();
        //act.id = actviteId;
        AvoirActivite av = new AvoirActivite();
        av = avoiractiviteservice.findByIdActivite(id);
        tfNom.setText(av.getActivite().getNom());
        cbCategorie.setPromptText(av.getActivite().getCategorie().getNom());
        taDescription.setText(av.getDescription());
      

    }

    public boolean isNotNull() {
        boolean isNotNull;
        if (cbCategorie.getSelectionModel().isEmpty()) {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur : Champs Vide ");
            alert.setContentText("Veuillez remplir tous les champs");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            isNotNull = false;
        } else {

            isNotNull = true;
        }
        return isNotNull;
    }

}
