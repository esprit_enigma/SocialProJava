/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.User;
import socialproo.technique.BCrypt;
import socialproo.technique.Mailer;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialproo.services.impl.UserService;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class ResetPasswordController implements Initializable {

    @FXML
    Label errorlb;

    @FXML
    TextField emailtf, codetf;

    @FXML
    PasswordField newpasswordtf;
    @FXML
    VBox HBMain, EmailVBox;

    User user;

    Stage stage;
    Scene scene;
    Parent root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        EmailVBox.setVisible(false);
        HBMain.setVisible(true);

        // TODO
    }

    public void sendCode() {
        errorlb.setText("");
        if (emailtf.getText().length() == 0) {
            errorlb.setText("Email field is empty");
        } else {
            UserService us = new UserService();
            User u = us.findByEmail(emailtf.getText());
            if (u == null) {
                errorlb.setText("Email is not valid");
            } else {
                user = u;
                Random random = new Random();
                int r = random.nextInt(9000) + 1000;
                Mailer.setCode(r);
                System.out.println(r);
                Mailer.send("SocialPro0@gmail.com", "enigmaEsprit", emailtf.getText(), "Rénitialisation de mot de passe", "Code de vérification :" + Mailer.getCode());
                System.out.println(Mailer.getCode());
                errorlb.setText("Reset code sent to email");
            }
        }

    }

    public void ChangePassword() {
        errorlb.setText("");
        if (codetf.getText().length() < 1) {
            errorlb.setText("Code field is empty");
        } else {
            if (codetf.getText().equals(String.valueOf(Mailer.getCode()))) {
                errorlb.setText("");
                EmailVBox.setVisible(true);
                HBMain.setVisible(false);
            } else {
                errorlb.setText("The reset code is wrong");
            }
        }
    }

    public void NewPassword() throws IOException {
        if (newpasswordtf.getText().length() == 0) {
            errorlb.setText("Password field is empty");
        } else if (newpasswordtf.getText().length() < 8) {
            errorlb.setText("Password is short");
        } else {
            String hashedPassword = BCrypt.hashpw(newpasswordtf.getText(), BCrypt.gensalt(13));
            User newu = user;
            newu.setPassword(hashedPassword);
            UserService us = new UserService();
            us.update(newu);

            stage = (Stage) newpasswordtf.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }
    }

}
