/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;
import socialproo.models.Entreprise;
import socialproo.services.impl.EntrepriseService;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ApplicationController implements Initializable {

    @FXML
    private Circle circleImgUsr;

    @FXML
    private Button btnLogOut;
    @FXML
    private ImageView imgabout;
    @FXML
    private ImageView imgMenuBtn;

    @FXML
    private StackPane acContent;

    @FXML
    public Button btnProfile;

    @FXML
    private ToggleButton sideMenuToogleBtn;

    @FXML
    private Label lblUsrNamePopOver;

    @FXML
    private Button btnfonctionnalite;

    @FXML
    private ImageView imgHomeBtn;

    @FXML
    private ImageView imgStoreBtn;

    @FXML
    private Button btnRecommendateion;

    @FXML
    private ImageView imgEmployeBtn;

    @FXML
    private Button btnannonce;

    @FXML
    private MenuButton mbtnUsrInfoBox;

    @FXML
    private ImageView imgSellBtn;

   

    @FXML
    private AnchorPane acDashBord;

    @FXML
    private Label lblUserId;

    @FXML
    private MenuItem miPopOver;

    @FXML
    private AnchorPane miPop;

    @FXML
    private AnchorPane acMain;

    @FXML
    private Circle imgUsrTop;

    @FXML
    private Label lblUsrName;

    @FXML
    private BorderPane appContent;

    @FXML
    private Label lblRoleAs;

    @FXML
    private Label lblFullName;

    @FXML
    private AnchorPane acHead;
    

    @FXML
    private Button btnCours;

    @FXML
    private ScrollPane leftSideBarScroolPan;
    public static StackPane rootP;
       Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();

    Image menuImage = new Image("/icon/menu.png");
    Image menuImageRed = new Image("/icon/menuRed.png");
    Image image;

    String defultStyle = "-fx-border-width: 0px 0px 0px 5px;"
            + "-fx-border-color:none";

    String activeStyle = "-fx-border-width: 0px 0px 0px 5px;"
            + "-fx-border-color:#FF4E3C";

    Image home = new Image("/icon/home.png");
    Image homeRed = new Image("/icon/homeRed.png");
    Image stock = new Image("/icon/stock.png");
    Image stockRed = new Image("/icon/stockRed.png");
    Image sell = new Image("/icon/sell2.png");
    Image sellRed = new Image("/icon/sell2Red.png");
    Image employee = new Image("/icon/employe.png");
    Image employeeRed = new Image("/icon/employeRed.png");
    Image cours = new Image("/icon/about.png");
    Image coursRed = new Image("/icon/aboutRed.png");
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //  viewDetails();
         rootP = acContent;
          imgMenuBtn.setImage(menuImage);
//        Image usrImg = new Image("/socialpro/images/profile.png");
//
//        imgUsrTop.setFill(new ImagePattern(usrImg));
//        circleImgUsr.setFill(new ImagePattern(usrImg));
    }    

    @FXML
public  void onClickProfile(ActionEvent event)throws IOException {
        System.out.println("nader");
        homeActive();
   
        FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/MenuProfile.fxml"));

            fxmlLoader.load();
        
        
       MenuProfileController menuController = fxmlLoader.getController();
        menuController.bpProfile.getStylesheets().add("/GUI/Menu.css");
        
        menuController.btnProfileOnAction(event);
      AnchorPane acPane = fxmlLoader.getRoot();

        acContent.getChildren().clear();

        acContent.getChildren().add(acPane);
    }

    @FXML
    void onClickFonctionnalite(ActionEvent event) throws IOException {
        sotreActive();
        
           FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/Fonctionnalite.fxml"));

            fxmlLoader.load();
        
        
      AnchorPane acPane = fxmlLoader.getRoot();

        acContent.getChildren().clear();

        acContent.getChildren().add(acPane);

    }
    // <ChoiceBox fx:id="typeprofile" prefHeight="25.0" prefWidth="223.0" GridPane.columnIndex="4" GridPane.columnSpan="2" GridPane.rowIndex="2" />
    @FXML
    void onClickbtnRecommendateion(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/listDemande.fxml"));

            fxmlLoader.load();
        
        
      AnchorPane acPane = fxmlLoader.getRoot();

        acContent.getChildren().clear();

        acContent.getChildren().add(acPane);
 
    }

    @FXML
    void onClickbtnannonce(ActionEvent event) throws IOException {
        annonceActive();
//        FXMLLoader fxmlLoader = new FXMLLoader();
//                   fxmlLoader.setLocation(getClass().getResource("/GUI/Annonce.fxml"));
//
//        try {
//            fxmlLoader.load();
//        } catch (IOException ex) {
//            Logger.getLogger(ApplicationController.class.getName()).log(Level.SEVERE, null, ex);
//        }
FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/Annonce.fxml"));

            fxmlLoader.load();
        
        
      AnchorPane acPane = fxmlLoader.getRoot();

        acContent.getChildren().clear();

        acContent.getChildren().add(acPane);
    }

    @FXML
    void onClickbtnCours(ActionEvent event) throws IOException {
coursActive();
       FXMLLoader fxmlLoader = new FXMLLoader();
                   fxmlLoader.setLocation(getClass().getResource("/GUI/FormationEntrep.fxml"));

            fxmlLoader.load();
        
        
      AnchorPane acPane = fxmlLoader.getRoot();

        acContent.getChildren().clear();

        acContent.getChildren().add(acPane);
    }


    @FXML
    private void btnLogOut(ActionEvent event) throws IOException {
        acContent.getChildren().clear();
        acContent.getChildren().add(FXMLLoader.load(getClass().getResource("/GUI/Login.fxml")));
        acDashBord.getChildren().clear();
        acHead.getChildren().clear();
        acHead.setMaxHeight(0.0);
    }

    @FXML
    private void mbtnOnClick(ActionEvent event) {
    }

    @FXML
    private void sideMenuToogleBtnOnCLick(ActionEvent event) {
        if (sideMenuToogleBtn.isSelected()) {
            imgMenuBtn.setImage(menuImageRed);
            TranslateTransition sideMenu = new TranslateTransition(Duration.millis(200.0), acDashBord);
            sideMenu.setByX(-130);
            sideMenu.play();
            acDashBord.getChildren().clear();
        } else {
            imgMenuBtn.setImage(menuImage);
            TranslateTransition sideMenu = new TranslateTransition(Duration.millis(200.0), acDashBord);
            sideMenu.setByX(130);
            sideMenu.play();
            acDashBord.getChildren().add(leftSideBarScroolPan);
        }
    }

    @FXML
    private void acMain(KeyEvent event) {
                if (event.getCode() == KeyCode.F11) {
            Stage stage = (Stage) acMain.getScene().getWindow();
            stage.setFullScreen(true);
        }
    }

    @FXML
    private void acMainOnMouseMove(MouseEvent event) {
    }
    private void homeActive() {
         System.out.println("d5al");
        imgHomeBtn.setImage(homeRed);
        imgStoreBtn.setImage(stock);
        imgSellBtn.setImage(sell);
        imgEmployeBtn.setImage(employee);
        imgabout.setImage(cours);
        btnProfile.setStyle(activeStyle);
        btnfonctionnalite.setStyle(defultStyle);
        btnannonce.setStyle(defultStyle);
        btnRecommendateion.setStyle(defultStyle);
        btnCours.setStyle(defultStyle);
        
    }

    private void sotreActive() {
        imgHomeBtn.setImage(home);
        imgStoreBtn.setImage(stockRed);
        imgSellBtn.setImage(sell);
        imgEmployeBtn.setImage(employee);
        imgabout.setImage(cours);
        
        btnCours.setStyle(defultStyle);
        btnProfile.setStyle(defultStyle);
        btnfonctionnalite.setStyle(activeStyle);
        btnannonce.setStyle(defultStyle);
        btnRecommendateion.setStyle(defultStyle);
        
    }

    private void sellActive() {
        imgHomeBtn.setImage(home);
        imgStoreBtn.setImage(stock);
        imgSellBtn.setImage(sellRed);
        imgEmployeBtn.setImage(employee);
        imgabout.setImage(cours);
        btnProfile.setStyle(defultStyle);
        btnfonctionnalite.setStyle(defultStyle);
        btnannonce.setStyle(activeStyle);
         btnCours.setStyle(defultStyle);
        btnRecommendateion.setStyle(defultStyle);
        
    }

    private void employeeActive() {
        imgHomeBtn.setImage(home);
        imgStoreBtn.setImage(stock);
        imgSellBtn.setImage(sell);
        imgEmployeBtn.setImage(employeeRed);
        imgabout.setImage(cours);
        btnProfile.setStyle(defultStyle);
        btnfonctionnalite.setStyle(defultStyle);
        btnannonce.setStyle(defultStyle);
        btnRecommendateion.setStyle(activeStyle);
         btnCours.setStyle(defultStyle);
        
    }
    //sellRed
        private void coursActive() {
        imgHomeBtn.setImage(home);
        imgStoreBtn.setImage(stock);
        imgSellBtn.setImage(sell);
        imgEmployeBtn.setImage(employee);
        imgabout.setImage(coursRed);
        btnProfile.setStyle(defultStyle);
        btnfonctionnalite.setStyle(defultStyle);
        btnannonce.setStyle(defultStyle);
        btnRecommendateion.setStyle(defultStyle);
        btnCours.setStyle(activeStyle);
        
    }
        
        private void annonceActive() {
        imgHomeBtn.setImage(home);
        imgStoreBtn.setImage(stock);
        imgSellBtn.setImage(sellRed);
        imgEmployeBtn.setImage(employee);
        imgabout.setImage(cours);
        btnProfile.setStyle(defultStyle);
        btnfonctionnalite.setStyle(defultStyle);
        btnannonce.setStyle(activeStyle);
        btnRecommendateion.setStyle(defultStyle);
        btnCours.setStyle(defultStyle);
        
    }
 
 
      public void viewDetails() {
          System.out.println("marwamaj");
             entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        System.out.println("entrep activite "+LoginController.getInstance().getUser());
         Image usrImg = new Image("http://localhost/SocialPro/web/LogoEntreprises/"+LoginController.getInstance().getUser().getUsername()+"/"+LoginController.getInstance().getUser().getUsername()+".jpg");
        

       
        circleImgUsr.setFill(new ImagePattern(usrImg));
        imgUsrTop.setFill(new ImagePattern(usrImg));
        lblFullName.setText(entreprise.getNom());
        lblUsrNamePopOver.setText(entreprise.email);
    }

    
}
