/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.User;
import socialproo.technique.Mailer;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import socialproo.services.impl.UserService;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class EmailValidationController implements Initializable {

    @FXML
    Label sendnewcode, errorlabel;
    @FXML
    TextField codetf;
    int r;
    static User testuser;

    public static User getTestuser() {
        return testuser;
    }

    public static void setTestuser(User testuser) {
        EmailValidationController.testuser = testuser;
    }
    Stage stage;
    Scene scene;
    Parent root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Random random = new Random();
        r = random.nextInt(9000) + 1000;
        Mailer.setCode(r);
        Mailer.send("SocialPro0@gmail.com", "enigmaEsprit", testuser.getEmail(), "Email Validation", "Validation code :" + r);
        errorlabel.setText("Validation code sent to email");
    }

    public void SendNewCode() {
        Random random = new Random();
        Mailer.setCode(random.nextInt(9000) + 1000);
        Mailer.send("SocialPro0@gmail.com", "enigmaEsprit", testuser.getEmail(), "Email Validation", "Validation code :" + r);
        errorlabel.setText("Validation code sent to email");
    }

    public void validate() throws IOException {
        if (codetf.getText().length() < 1) {
            errorlabel.setText("Code field is empty");
        } else if (String.valueOf(r).equals(codetf.getText())) {
            
            testuser.setEnabled(true);
            UserService us = new UserService();
            us.updateEnabled(us.findByEmail(testuser.getEmail()));

            stage = (Stage) errorlabel.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        } else {
            errorlabel.setText("Code is wrong");
        }
    }

}
