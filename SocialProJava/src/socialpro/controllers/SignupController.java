/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.User;
import socialproo.technique.BCrypt;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialproo.services.impl.UserService;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class SignupController implements Initializable {

   

    Stage stage;
    Parent root;
    
    
     @FXML
    private AnchorPane apMother;

    @FXML
    private Button registerbtn;

    @FXML
    private TextField usernametf;

    @FXML
    private ChoiceBox<String> typeprofile;

    @FXML
    private TextField emailtf;

    @FXML
    private AnchorPane apDesignPane;

    @FXML
    private PasswordField pwtf;
    @FXML
    private Hyperlink hlback;


    @FXML
    private PasswordField pwctf;

    
    
  
 
   
    
    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    @FXML
    void Register(ActionEvent event) throws IOException {
        UserService us = new UserService();
        String typeprof = typeprofile.getValue();
        String username = usernametf.getText();
        String email = emailtf.getText();
        String pw = pwtf.getText();
        String pwc = pwctf.getText();

        if (username.length() > 0 && email.length() > 0 && pw.length() > 0 && pwc.length() > 0) {
            if (us.findByUsername(username) == null) {
                if (ValidateEmail(email)) {
                    if (us.findByEmail(email) == null) {
                        if (pw.equals(pwc)) {
                            String hashedPassword = BCrypt.hashpw(pw, BCrypt.gensalt(13));
                             StringBuilder finalpassword = new StringBuilder(hashedPassword);
                            finalpassword.setCharAt(2, 'y');
                            String pass = finalpassword.toString();
                            String role = "";
                            if (typeprof.equals("Jobber")) {
                                role = "a:1:{i:0;s:13:\"ROLE_PREJOBER\";}";
                            } else if (typeprof.equals("Entreprise")) {
                                role = "a:1:{i:0;s:18:\"ROLE_PREENTREPRISE\";}";
                            }
                            User u = new User(username, username, email, email, false, null, pass, null, null, null, role);
                            us.add(u);
                            EmailValidationController.setTestuser(u);
                            
                            stage = (Stage) typeprofile.getScene().getWindow();
                            root = FXMLLoader.load(getClass().getResource("/GUI/EmailValidation.fxml"));
                            Scene scene = new Scene(root);
                            stage.setScene(scene);
                            stage.centerOnScreen();
                            stage.show();


                          
                            
                        } else {
                             TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Mot De Passe De Confirmation Est Invalide ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
                          
                        }
                    } else {
                         TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Email Est Deja Utilisé ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
                          
                        
                    }
                } else {
                     TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Email Est Invalide ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
                          
                    
                }
            } else {
                 TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Nom De L Utilisateur Est Déja Utlisé ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
                          
              
            }
        } else {
             TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Verifier Tous Les Champs");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
                          
        }
    }

    public Boolean ValidateEmail(String email) {
        Pattern p = Pattern.compile("[a-zA-Z0-9][a-zA-Z0-9._]*@[a-zA-Z0-9]+([.][a-zA-Z]+)+");
        Matcher m = p.matcher(email);

        if (m.find() && m.group().equals(email)) {
            return true;
        } else {
            return false;
        }
    }
    public void loadUpdate() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("Login -SocialPro");
            nStage.show();
            Stage stage = (Stage) hlback.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

     

    }
    @FXML
    void hlbackonaction(ActionEvent event) throws IOException {
    loadUpdate();
    }


}
