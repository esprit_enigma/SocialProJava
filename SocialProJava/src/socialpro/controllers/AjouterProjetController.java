/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import socialproo.models.Entreprise;
import socialproo.models.ProjetEntreprise;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.ProjetEntrepriseService;
import socialprotest.ApplicationController;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterProjetController implements Initializable {
    @FXML
    private JFXTextField tfNom;
    @FXML
    private JFXTextField tfClient;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    public JFXButton btnModifier;
    @FXML
    public JFXButton btnAjouter;
    @FXML
    private Button btnClose;
     @FXML
    private DatePicker itemdate;

    ApplicationController apCotroller = new ApplicationController();
    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    ProjetEntreprise projet = new ProjetEntreprise();
    ProjetEntrepriseService projetservice = new ProjetEntrepriseService();
    int projetId;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnmodificationAction(ActionEvent event) {
         entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
         
          projet= projetservice.findById(projetId);
          System.out.println("ppppppppppppppppppp"+projetId);
        //  projetId=projet.getId();
          projet.setNom(tfNom.getText().trim());
          projet.setDestination(tfClient.getText().trim());
          projet.setDescription(taDescription.getText().trim());
          projet.setDateAjout(itemdate.getValue().toString());

        
        System.out.println("projet" + projet.getDateAjout()+projet.getNom());
        projetservice.update(projet);
        TrayNotification tn = new TrayNotification("MODIFICATION PROJET", "Modification Effectuée Avec Succées", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.POPUP);
                tn.showAndDismiss(Duration.seconds(2));
       

    }

    @FXML
    private void btnAjouterAction(ActionEvent event) {
          
        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        projet.setNom(tfNom.getText());
        projet.setDescription(taDescription.getText());
        projet.setDestination(tfClient.getText());
        projet.setEntreprise(entreprise);
        projet.setDateAjout(itemdate.getValue().toString());
        projetservice.add(projet);
        
        TrayNotification tn = new TrayNotification("AJOUT PROJET", "Ajout Effectué Avec Succés", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.POPUP);
                tn.showAndDismiss(Duration.seconds(2));
       
    }

    @FXML
    private void btnCloseOnAction(ActionEvent event) {
           Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }
        public void showDetails(int id) {

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        //projetId = projetservice.findByIdEntreprise(entreprise.getId()).getId();
        projet= projetservice.findById(id);
       
       System.out.println("date "+projet.toString());
        tfNom.setText(projet.getNom());
        tfClient.setText(projet.getDestination());
        taDescription.setText(projet.getDescription());
            System.out.println("date "+projet.getDateAjout());
            System.out.println("date "+projet.dateAjout);
        itemdate.setValue(LocalDate.parse(projet.getDateAjout()));

    }
    
}
