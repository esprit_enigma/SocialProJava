/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class FonctionnaliteMenuController implements Initializable {

    
    @FXML
    private JFXButton btnProjets;

    @FXML
    private JFXButton btnActivite;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
     @FXML
    void OnClickbtnActivite(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
          
                   fxmlLoader.setLocation(getClass().getResource("/GUI/listFonctionnalite.fxml"));

            fxmlLoader.load();
       
              
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);
    }

    
     @FXML
    void OnClickBtnProjets(ActionEvent event) {
         FXMLLoader fxmlLoader = new FXMLLoader();
        try {
                fxmlLoader.setLocation(getClass().getResource("/GUI/AfficherProjet.fxml"));

            fxmlLoader.load();
          
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);

    }
    
}
