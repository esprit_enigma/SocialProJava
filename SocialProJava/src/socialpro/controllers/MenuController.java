/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Jober;
import socialproo.models.User;
import com.jfoenix.controls.JFXButton;
import javafx.scene.control.Label;


import com.jfoenix.controls.JFXRippler;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventType;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import socialproo.services.impl.JoberService;

/**
 *
 * @author oudayblouza
 */
public class MenuController implements Initializable {

    @FXML
    private ImageView image;

    @FXML
    private VBox box;
    
    @FXML
    private StackPane stacky;
    LoginController lc = new LoginController();
    User user = lc.currentUser;
    JoberService js = new JoberService();
    Jober j = js.findByUser(user);
    
       Stage stage;
    Scene scene;
    Parent root;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

                Image imageOk = new Image("/Ressources/Lock.png");
        ImageView imgvl = new ImageView(imageOk);
        imgvl.setFitHeight(30);
        imgvl.setFitWidth(30);
        Button logout = new Button("Logout", imgvl);
 logout.addEventHandler(MouseEvent.MOUSE_CLICKED, (e)->{
         LoginController.setCurrentUser(null);
        stage = (Stage) logout.getScene().getWindow();
                    try {
                        root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
                    } catch (IOException ex) {
                        Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
                    }
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
 });
        stacky.getChildren().add(logout);
        
        
        Image img = new Image("Ressources/person-flat.png");
try{
        InputStream input = null;
        try {
            input = new URL("http://localhost/images/ProfilePic/" + user.getUsername() + "/" + user.getUsername() + ".jpg").openStream();
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedImage image1 = null;
        try {
            image1 = ImageIO.read(input);
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Image image0 = SwingFXUtils.toFXImage(image1, null);

        image.setImage(image0);}
catch(Exception ex1) {
         image.setImage(img);
        }
//        lv.setExpanded(true);
//        lv.depthProperty().set(1);
        Label username = new Label(user.getUsername());
        box.getChildren().add(username);
        Separator sp0 = new Separator();
        box.getChildren().add(sp0);
        JFXButton profilBtn = new JFXButton("Mon Profil");
        profilBtn.setAccessibleText("Mon Profil");
        profilBtn.setId("profilBtn");
        profilBtn.setPrefHeight(45);
        profilBtn.setPrefWidth(150);
        profilBtn.setStyle("-fx-background-color: #698a76  ");

        profilBtn.setRipplerFill(Paint.valueOf("#698a76"));
        JFXRippler rippler = new JFXRippler(profilBtn);
        rippler.setAccessibleText("profilBtn");

        JFXButton editprofilBtn = new JFXButton("Mon CV");
        editprofilBtn.setId("editprofilBtn");
        editprofilBtn.setPrefHeight(45);
        editprofilBtn.setPrefWidth(150);
        editprofilBtn.setStyle("-fx-background-color: #698a76  ;");
        editprofilBtn.setRipplerFill(Paint.valueOf("#698a76"));
        JFXRippler rippler1 = new JFXRippler(editprofilBtn);
        rippler1.setAccessibleText("editprofilBtn");

        JFXButton allproBtn = new JFXButton("Tous les Profils");
        allproBtn.setId("allproBtn");
        allproBtn.setPrefHeight(45);
        allproBtn.setPrefWidth(150);
        allproBtn.setStyle("-fx-background-color: #698a76  ;");
        allproBtn.setRipplerFill(Paint.valueOf("#698a76"));
        JFXRippler rippler0 = new JFXRippler(allproBtn);
        rippler0.setAccessibleText("allproBtns");
        
        


        JFXButton notificationprofilBtn = new JFXButton("Mes Notifications");
        notificationprofilBtn.setId("notificationprofilBtn");
        notificationprofilBtn.setPrefHeight(45);
        notificationprofilBtn.setPrefWidth(150);
        notificationprofilBtn.setStyle("-fx-background-color: #698a76  ;");
        notificationprofilBtn.setRipplerFill(Paint.valueOf("#698a76"));
        JFXRippler rippler3 = new JFXRippler(notificationprofilBtn);

        JFXButton FormationBtn = new JFXButton("Mes Formations");
        FormationBtn.setId("FormationBtn");
        FormationBtn.setPrefHeight(45);
        FormationBtn.setPrefWidth(150);
        FormationBtn.setStyle("-fx-background-color: #698a76  ;");
        FormationBtn.setRipplerFill(Paint.valueOf("#698a76"));
        JFXRippler rippler4 = new JFXRippler(FormationBtn);
 rippler4.setAccessibleText("FormationBtn");
 
   
//            profilBtn.setGraphic(new ImageView(new Image(new FileInputStream("/Ressources/person-flat.png"))));
        box.getChildren().add(rippler);
        Separator sp = new Separator();
        box.getChildren().add(sp);
        box.getChildren().add(rippler1);
        Separator sp1 = new Separator();
        box.getChildren().add(sp1);
        box.getChildren().add(rippler0);
        Separator sp2b = new Separator();
        box.getChildren().add(sp2b);
        
        
        
    
        box.getChildren().add(rippler3);
        Separator sp3 = new Separator();
        box.getChildren().add(sp3);
        box.getChildren().add(rippler4);
        Separator sp4 = new Separator();
        box.getChildren().add(sp4);


    }

    public void SignOut() throws IOException {
        LoginController.setCurrentUser(null);
//        stage = (Stage) .getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
