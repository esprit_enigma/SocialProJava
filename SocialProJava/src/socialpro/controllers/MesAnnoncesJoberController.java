/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialproo.models.AnnonceEntreprise;
import socialproo.models.AnnonceJober;
import socialproo.services.impl.AnnonceJoberService;

/**
 * FXML Controller class
 *
 * @author IMEN
 */
public class MesAnnoncesJoberController implements Initializable {
    @FXML
    private JFXTextField tfSearch;
    @FXML
    private Button btnRefresh;
    @FXML
    private JFXButton btnDelete;
    
     @FXML
    private JFXButton btnAddNew;

    @FXML
    private JFXButton btnUpdate;
    @FXML
    private ListView<AnnonceJober> listannonce;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listannonce();
    }

    List<AnnonceJober> ls = new ArrayList<>();
    AnnonceJoberService a = new AnnonceJoberService();
    AnnonceJober ann = new AnnonceJober();

    public void listannonce() {
        ls = a.getAll();
        ObservableList<AnnonceJober> ob = FXCollections.observableArrayList(ls);
        listannonce.setItems(ob);

    }
      @FXML
    void btnAddItemOnAction(ActionEvent event) {
       AjoutAnnonceJoberController ajact = new AjoutAnnonceJoberController();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/AjoutAnnonceJober.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
          AjoutAnnonceJoberController ajac  = fxmlLoader.getController();
        // ajac.btnModifier.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        

    }

    @FXML
    void btnDeleteOnAction(ActionEvent event) {
     AnnonceJober selectedCatagory = listannonce.getSelectionModel().getSelectedItem();  
     Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alerte");
        alert.setHeaderText("Confirm");
        alert.setContentText("Vous voulez supprimer cette annonce ? \n pour Confirmer cliquez ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
             a.delete(selectedCatagory.getId());
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
//  listannonce.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<AnnonceJober>() {
//            @Override
//            public void changed(ObservableValue<? extends AnnonceJober> observable, AnnonceJober oldValue, AnnonceJober newValue) {
//                   ButtonType oui = new ButtonType("Oui", ButtonBar.ButtonData.YES);
//                ButtonType non = new ButtonType("Non", ButtonBar.ButtonData.NO);
//                Alert alert = new Alert(Alert.AlertType.WARNING,
//                        "Voulez vous vraiment supprimer cette annonce ?", oui, non);
//                alert.setTitle("Attention");
//                alert.setHeaderText(null);
//                Optional<ButtonType> result = alert.showAndWait();
//                if (result.isPresent() && result.get() == oui) {
//                    a.delete(newValue.getId());
//
//                    Alert info = new Alert(Alert.AlertType.INFORMATION);
//                    info.setTitle("Information");
//                    info.setHeaderText(null);
//                    info.setContentText("Cette annonce  est supprimée avec success");
//
//                    info.showAndWait();
//
//                }
//              
//           
//
//            }
//        }
//        );
    }

    @FXML
    void btnRefreshOnAction(ActionEvent event) {

    }

    @FXML
    void btnUpdateOnAction(ActionEvent event) {
          MapTrajetController ajact = new MapTrajetController();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/MapTrajet.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
          MapTrajetController ajac  = fxmlLoader.getController();
        // ajac.btnModifier.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
