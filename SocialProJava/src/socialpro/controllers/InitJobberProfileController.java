/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Delegation;
import socialproo.models.Jober;
import socialproo.models.Nature;
import socialproo.models.Specialite;
import socialproo.models.User;
import socialproo.models.Ville;

import java.awt.image.BufferedImage;
import java.io.File;

import java.io.IOException;
import java.net.URL;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

import socialproo.services.impl.DelegationService;
import socialproo.services.impl.JoberService;
import socialproo.services.impl.NatureService;
import socialproo.services.impl.SpecialiteService;
import socialproo.services.impl.UserService;
import socialproo.services.impl.VilleService;
import ui.DateUtils;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class InitJobberProfileController implements Initializable {

    @FXML
    TextField nom, prenom, numTel, adress;

    @FXML
    ComboBox ville, delegation, nature, specialite, sexe;

    @FXML
    DatePicker DateDeNai;

    @FXML
    TextArea description;

    @FXML
    Button submitbtn, logout;
    @FXML
    StackPane sp;
    @FXML
    ImageView image1, image2;
    @FXML
    AnchorPane ap1, ap0, ap2;
    @FXML
    Group grp;

    Stage stage;
    Parent root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        numTel.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    numTel.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }

        });
        ap0.setMinHeight(600);
        ap0.setMinWidth(800);

        Image img2 = new Image("Ressources/background2.jpg");
        Image img = new Image("Ressources/socialpro.png");
        Image img1 = new Image("Ressources/person-flat.png");
        Image img3 = new Image("Ressources/logout.png");
        image1.setImage(img);
        image2.setImage(img1);

        BackgroundImage myBI = new BackgroundImage(img2, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
        ap0.setBackground(new Background(myBI));

        ville.setPromptText("Ville");
        nature.setPromptText("Nature");
        specialite.setPromptText("Specialité");
        delegation.setPromptText("Delegation");
        sexe.setPromptText("Sexe");
       
        //Sexe ComboBox
        ObservableList<String> sexechoices = FXCollections.observableArrayList("Homme", "Femme");
        sexe.setItems(sexechoices);

        //ville ComboBox
        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();
        ObservableList<String> villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i.getNom()));
        ville.setItems(villechoices);

        //delegation ComboBox
        ville.setOnAction(e -> {
            Ville v = vs.findByNom((String) ville.getValue());
            DelegationService ds = new DelegationService();
            List<Delegation> ld = ds.findBynv(v);
            if (ld != null) {
                ObservableList<String> delegationchoices = FXCollections.observableArrayList();
                ld.forEach(i -> delegationchoices.add(i.getNom()));
                delegation.setItems(delegationchoices);
            }
        });

        //Nature ComboBox
        NatureService ns = new NatureService();
        List<Nature> ln = ns.getAll();
        ObservableList<String> Naturechoices = FXCollections.observableArrayList();
        ln.forEach(n -> Naturechoices.add(n.getNom()));
        nature.setItems(Naturechoices);

        //Specialite ComboBox
        nature.setOnAction(e -> {
            Nature n = ns.findByNom((String) nature.getValue());
            SpecialiteService ss = new SpecialiteService();
            List<Specialite> ls = ss.findBynv(n);
            if (ls != null) {
                ObservableList<String> specialitechoices = FXCollections.observableArrayList();
                ls.forEach(i -> specialitechoices.add(i.getNom()));
                specialite.setItems(specialitechoices);
            }
        });
    }

    public void upload() throws IOException {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("upload image");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Images", "*.bmp", "*.gif", "*.jpg", "*.jpeg", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.showOpenDialog(stage);
        if (fileChooser.showOpenDialog(stage) != null) {
            File file = fileChooser.showOpenDialog(stage);
            BufferedImage image = ImageIO.read(file);
            Image image0 = SwingFXUtils.toFXImage(image, null);
            image2.setImage(image0);
        }
    }

    public void dateverif() {

        LocalDate date1 = LocalDate.now();

        LocalDate date = DateDeNai.getValue();
        int year1 = date1.getYear();
        int year = date.getYear();

        if (year >= year1 - 15) {

            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Date Invalide");
            alert.setContentText("Votre age est inférieur ou égale à 15 ans vous n'etes pas autorise à continuer");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            DateDeNai.setValue(null);
        }

    }

    public void enregistrer() throws IOException {
        LoginController lc = new LoginController();
        User us = lc.currentUser;
        UserService userv = new UserService();
        User user = userv.findByUsername(us.getUsername());
        lc.currentUser = user;
        if ((delegation.getValue()!=null)&&(sexe.getValue()!=null)&&(specialite.getValue()!=null)&&(DateDeNai.getValue() != null) && (!prenom.getText().equals("")) && (!nom.getText().equals("")) && (!description.getText().equals("")) && (!numTel.getText().equals(""))) {
            if (user != null) {
                DelegationService ds = new DelegationService();

                Delegation d = ds.findByNom(delegation.getValue().toString());

                SpecialiteService ss = new SpecialiteService();
                Specialite s = ss.findByNom(specialite.getValue().toString());
                Date d1 = new Date();

                Date df = DateUtils.asDate(DateDeNai.getValue());

                System.out.println(d + " " + s + "" + nom.getText() + "" + d1 + "" + adress.getText() + "" + sexe.getValue().toString() + "" + description.getText() + "" + 52897990 + "" + d1 + "" + user);
                Jober j = new Jober(d, s, nom.getText(), prenom.getText(), df, adress.getText(), sexe.getValue().toString(), description.getText(), Integer.parseInt(numTel.getText()), d1, user);

                JoberService js = new JoberService();

                js.add(j);

                user.setRoles("a:1:{i:0;s:10:\"ROLE_JOBER\";}");

                userv.update(user);

                stage = (Stage) submitbtn.getScene().getWindow();

                root = FXMLLoader.load(getClass().getResource("/GUI/Jober.fxml"));

                Scene scene = new Scene(root);

                stage.setScene(scene);
                stage.show();
            } else {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Erreur");
                alert.setHeaderText("Erreur de Connexion");
                alert.setContentText("Vous devez vous connectez");
                alert.showAndWait().ifPresent(rs -> {
                    if (rs == ButtonType.OK) {
                        System.out.println("Pressed OK.");
                    }
                });
                stage = (Stage) submitbtn.getScene().getWindow();
                try {
                    root = FXMLLoader.load(getClass().getResource("/GUI/login.fxml"));
                } catch (IOException ex) {
                    Logger.getLogger(InitJobberProfileController.class.getName()).log(Level.SEVERE, null, ex);
                }
                Scene scene = new Scene(root);

                stage.setScene(scene);

                stage.show();
            }

        } else {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Formulaire Incomplet");
            alert.setContentText("Vous devez Remplir tous les champs");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
        }
        
        
        
        
        
    }
}
