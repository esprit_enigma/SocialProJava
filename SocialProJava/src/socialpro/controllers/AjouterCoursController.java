/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Cours;
import socialproo.models.Entreprise;
import socialproo.models.Specialite;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import socialproo.services.impl.CrudCours;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.SpecialiteService;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author m_s info
 */
public class AjouterCoursController implements Initializable {

    @FXML
    private ImageView imagev;

    @FXML
    private JFXTextField image;
     @FXML
    private JFXButton ajouti;
    
    @FXML
    private JFXButton ajoutp;
    @FXML
    private JFXTextField pdf;

    @FXML
    private JFXComboBox<?> specialite;

    @FXML
    private JFXTextField titre;

    @FXML
    private JFXTextField video;
    
    @FXML
    private JFXButton ajout;
        
    @FXML
    private JFXTextArea description;


    /**
     * Initializes the controller class.
     */
    socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();

    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        image.setPromptText("Image");
        pdf.setPromptText("Lien pdf du cours");
        video.setPromptText("Lien Du Video");
        specialite.setPromptText("Specialite");
         description.setPromptText("Description du cours");
        titre.setPromptText("Titre du cours");
        Image img = new Image("Ressources/Icon_Livre.png");
        imagev.setImage(img);
        
        ImageView ajoutimg = new ImageView("Ressources/53327.png");
                
                ajoutimg.setFitWidth(80);
                ajoutimg.setFitHeight(80);
                ajout.setGraphic(ajoutimg);
                
                
    SpecialiteService ss = new SpecialiteService();
        List<Specialite> lst = new ArrayList();
        lst = ss.getAll();
        ObservableList listespec = FXCollections.observableArrayList();
       lst.forEach(e -> listespec.add(e.getNom()) );
        
        specialite.setItems(listespec);            
        
        ajout.setOnAction(e->
                {
                    ajoutcours();
                });
    }
    
    Cours c= new Cours();

       @FXML
    void ajoutimage(ActionEvent event)throws IOException {
        socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\images\\cours\\");
//        if (!file.exists()) {
//            if (file.mkdir()) {
//                System.out.println("Directory is created!");
//            } else {
//                System.out.println("Failed to create directory!");
//            }
//        }
//         else {
//            try {
//
//                delete(file);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//                System.exit(0);
//            }
//
//            file.mkdir();
//
//        }
        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdf\\"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        fileChooser.setTitle("Choisissez un fichier pdf");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\images\\cours\\" + c.getId() + ".png");
                // File h =new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdfEntreprises\\"+LoginController.getInstance().getUser().getUsername()+"\\"+file.getName());

                saveFileImg(file);
                System.out.println(file);
                // EntrepriseService entrepriseService = new EntrepriseService();
                // entrepriseService.updateUpload(LoginController.getInstance().getUser().getId());
                if (file.renameTo(t)) {
                    file.delete();
                } else {
                    System.out.println("non");
                }

            } catch (Exception e) {
            }

            image.setText(file.getName());
        }

    }
    private void saveFileImg(File f) throws IOException {

        FileWriter writer = new FileWriter("C:\\wamp\\www\\SocialPro\\web\\images\\cours\\" + f.getName(), true);

        writer.close();
    }
      @FXML
      private void ajoutpdf(ActionEvent event)throws IOException{
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\images\\pdf\\");

        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdf\\"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF", "*.pdf")
        );

        fileChooser.setTitle("Choisissez un fichier pdf");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\images\\pdf\\" + c.getId() + ".pdf");
                // File h =new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdfEntreprises\\"+LoginController.getInstance().getUser().getUsername()+"\\"+file.getName());

                saveFile(file);
                System.out.println(file);
                // EntrepriseService entrepriseService = new EntrepriseService();
                // entrepriseService.updateUpload(LoginController.getInstance().getUser().getId());
                if (file.renameTo(t)) {
                    file.delete();
                } else {
                    System.out.println("non");
                }

            } catch (Exception e) {
            }

            pdf.setText(file.getName());
        }
    }
    private void saveFile(File f) throws IOException {

        FileWriter writer = new FileWriter("C:\\wamp\\www\\SocialPro\\web\\images\\pdf\\" + f.getName(), true);

        writer.close();
    }

    public void delete(File file)
            throws IOException {

        if (file.isDirectory()) {

            //directory is empty, then delete it
            if (file.list().length == 0) {

                file.delete();
                System.out.println("Directory is deleted : "
                        + file.getAbsolutePath());

            } else {

                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    delete(fileDelete);
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    System.out.println("Directory is deleted : "
                            + file.getAbsolutePath());
                }
            }

        } else {
            //if file, then delete it
            file.delete();
            System.out.println("File is deleted : " + file.getAbsolutePath());
        }
    }

  @FXML
    void ajoutcours() {
        String titre1 = titre.getText();
        
       String video1 = video.getText();
       
       String image1 = image.getText();
       
       String spec1 = (String) specialite.getValue();
       
       String pdf1 = pdf.getText();
       
       
       String description1 = description.getText();
      
       if ((!titre1.equals(""))&&(!video1.equals(""))&&(!image1.equals(""))&&(!spec1.equals(""))&&(!description1.equals(""))&&(!pdf1.equals("")))
       {
          entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
          // Entreprise e = new Entreprise();
        //e.setId(1);
        Date d = new Date();
        java.sql.Date sqlDate = new java.sql.Date(d.getTime());

        Cours c = new Cours(entreprise, new SpecialiteService().findByNom(spec1), description1, 0, video1, pdf1, sqlDate, null, false, titre1, image1);
        new CrudCours().add(c);

TrayNotification tr = new TrayNotification("Cours ajouté avec succé", "Cours ajouté avec succé", NotificationType.SUCCESS);
    tr.showAndWait();
       }
       else 
       {
          TrayNotification tr = new TrayNotification("Erreur", "Veuillez Remplir les Champs Correctement", NotificationType.ERROR);
    tr.showAndWait();
       }
    }
    
    
}
