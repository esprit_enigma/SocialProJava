/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Commentaires;
import com.adobe.acrobat.Viewer;
import socialproo.models.Cours;
import socialproo.models.User;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTextArea;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import socialproo.services.impl.CrudCommentaires;
import socialproo.services.impl.CrudCours;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author m_s info
 */
public class JoberFormationsController implements Initializable {

    @FXML
    private StackPane stack;

    @FXML
    private ScrollPane scroll1;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private ImageView socialpro;

    private Viewer viewer;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {


            List<Cours> lstcours = new CrudCours().getAll();
            VBox vb1 = new VBox();
            lstcours.forEach(e
                    -> {

                VBox vb2 = new VBox();
                vb2.setPrefWidth(180);
                vb2.setPrefHeight(180);
                VBox vb3 = new VBox();

                HBox hcours = new HBox();
               
                hcours.setSpacing(100);
                ImageView image = new ImageView("http://localhost/SocialPro/web/images/cours/" + e.getImage());

                image.setFitHeight(180);
                image.setFitWidth(300);
                vb2.getChildren().add(image);
                hcours.getChildren().add(vb2);

                Label titre = new Label();
                titre.setText("Titre du cours : " + e.getTitre());
                titre.setTextFill(Color.DARKBLUE);
                hcours.getChildren().add(titre);

                Label description = new Label("Description : " + e.getDescription());
                description.setTextFill(Paint.valueOf("#698a76" ));
                hcours.getChildren().add(description);

                Label dateAjout = new Label("Date d'ajout : " + e.getDateAjout().toString());
                dateAjout.setTextFill(Paint.valueOf("#698a76" ));
                hcours.getChildren().add(dateAjout);
                vb3.getChildren().add(titre);
                vb3.getChildren().add(description);
                vb3.getChildren().add(dateAjout);
                hcours.getChildren().add(vb3);
                VBox vb4 = new VBox();
                Button btnshow = new Button();
                ImageView plus = new ImageView("Ressources/youtube.png");
                
                plus.setFitWidth(80);
                plus.setFitHeight(80);
                btnshow.setGraphic(plus);

                btnshow.setOnAction((event) -> {
                    final Stage dialog = new Stage();
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    Window primaryStage = null;
                    dialog.initOwner(primaryStage);
                    VBox dialogVbox = new VBox(800);
                    WebView wv = new WebView();
                    wv.getEngine().load(e.getTuto());
                    // pdf viewer 

                    
                    dialogVbox.getChildren().add(wv);

                    Scene dialogScene = new Scene(dialogVbox, 1000, 1000);
                    dialog.setScene(dialogScene);
                    dialog.show();

                });
                 Button btncomments = new Button();
                ImageView commentaires = new ImageView("Ressources/chat.png");
                
                commentaires.setFitWidth(80);
                commentaires.setFitHeight(80);
                btncomments.setGraphic(commentaires);
                
                 btncomments.setOnAction(ev
                            -> {
                      List<Commentaires> lstcom = new CrudCommentaires().getAll();
                      System.out.println("coment"+lstcom);
                      System.out.println(e.getId());
                     
                    List<Commentaires> lstComCours =  lstcom.stream().filter(c -> c.getCours().equals(e)).collect(Collectors.toList());
//                    List<Commentaires> lstComCours=lstcom;
                    
                    final Stage dialog = new Stage();
                    dialog.initModality(Modality.APPLICATION_MODAL);
                    Window primaryStage = null;
                    dialog.initOwner(primaryStage);
                     GridPane gridpane = new GridPane();
                     
                    ColumnConstraints column1 = new ColumnConstraints();
                    
                     column1.setPercentWidth(50);
                    ColumnConstraints column2 = new ColumnConstraints();
                    column2.setPercentWidth(50);
                    gridpane.getColumnConstraints().addAll(column1, column2);
                    
                    
                    VBox entete = new VBox();
                    
                    Label lblTitre = new Label("Titre du cours : "+e.getTitre());
                   
                    lblTitre.setTextFill(Color.DARKCYAN);
                    Label lblcomentaires = new Label();
                    lblcomentaires.setText("Liste des Commentaires : ");
                    lblcomentaires.setTextFill(Color.DARKCYAN);
                    lblcomentaires.setFont(new Font(24));
                    HBox h = new HBox();
                    Label nbrCom = new Label("("+lstComCours.stream().count()+")");
                    nbrCom.setFont(new Font(24));
                    lblTitre.setFont(new Font(24));
                    h.getChildren().add(lblcomentaires);
                    h.getChildren().add(nbrCom);
                   
                    entete.getChildren().add(lblTitre);
                    entete.getChildren().add(h);
                    entete.setAlignment(Pos.TOP_LEFT);
                    entete.setFillWidth(true);
                    
                    gridpane.add(entete,0,0);
                    gridpane.setPadding(new Insets(20,20,20,20));
                    Button btnrefresh = new Button();
                ImageView com = new ImageView("Ressources/refresh.png");
                
                com.setFitWidth(80);
                com.setFitHeight(80);
                VBox b = new VBox();
                    
                btnrefresh.setGraphic(com);
                b.getChildren().add(btnrefresh);
                    gridpane.add(b,1 , 0);
                    b.setAlignment(Pos.TOP_RIGHT);
                
                btnrefresh.setOnAction( eve->       
                {
                    for (int i =1; i<=(lstComCours.stream().count()) ;i++)
                   {
                       
                       HBox hb1 = new HBox();
                       hb1.getChildren().clear();
                    VBox hb2 = new VBox();
                    hb2.getChildren().clear();
                    Circle circle = new Circle(50,50,50);
                       
                       ImageView imgv = new ImageView("Ressources/man.png");
                       
                       imgv.setClip(circle);
                       imgv.setFitHeight(120);
                       imgv.setFitWidth(120);
                       
                       hb1.getChildren().add(imgv);
                       
                       
                       Label lbln = new Label("User :"+lstComCours.get(i).getJober().getUsername()+"       ");
                       lbln.setFont(new Font(18));
                       lbln.setTextFill(Color.DARKCYAN);
                       hb2.getChildren().add(lbln);
                       
                       
                       JFXTextArea areacom = new JFXTextArea();
                       areacom.setFont(new Font(18));
                       areacom.setText(lstComCours.get(i).getContenu());
                       areacom.setEditable(false);
                       areacom.setPrefSize(20, 50);
                       hb2.getChildren().add(areacom);
                       hb1.getChildren().add(hb2);
                       gridpane.add(hb1, 0, i+1);
                       
                       System.out.println(i);
                       
                    }
                   
                    
                    
                    Scene dialogScene = new Scene(gridpane, 700, 500);
                    
                    dialog.setScene(dialogScene);
                    
                    dialog.show();
                    
                    
                }
                );
                
                    
                
                
                
                   for (int i =1; i<=(lstComCours.stream().count()) ;i++)
                   {
                       
                       HBox hb1 = new HBox();
                    VBox hb2 = new VBox();
                    Circle circle = new Circle(50,50,50);
                       
                       ImageView imgv = new ImageView("Ressources/man.png");
                       
                       imgv.setClip(circle);
                       imgv.setFitHeight(120);
                       imgv.setFitWidth(120);
                       
                       hb1.getChildren().add(imgv);
                       
                       
                     Label lbln = new Label("User : "+lstComCours.get(i-1).getJober().getUsername());
                       lbln.setFont(new Font(18));
                       lbln.setTextFill(Color.DARKCYAN);
                       hb2.getChildren().add(lbln);
                       
                       
                       JFXTextArea areacom = new JFXTextArea();
                       areacom.setFont(new Font(18));
                       areacom.setText(lstComCours.get(i-1).getContenu());
                       areacom.setEditable(false);
                       areacom.setPrefSize(20, 50);
                       hb2.getChildren().add(areacom);
                       hb1.getChildren().add(hb2);
                       gridpane.add(hb1, 0, i+1);
                       
                       
                       
                    }
                   
                     Button btnco = new Button();
                ImageView co = new ImageView("Ressources/chat.png");
                co.setFitHeight(80);
                co.setFitWidth(80);
                btnco.setPrefWidth(20);
                btnco.setPrefHeight(20);
                btnco.setGraphic(co);
                VBox h11 = new VBox();
                h11.setPrefHeight(80);
                h11.setPrefWidth(80);
                h11.setAlignment(Pos.TOP_RIGHT);
                h11.getChildren().add(btnco);
                gridpane.add(h11, 1,(int)lstComCours.stream().count()+2 );
                
                
               
                
                HBox hbcom = new HBox();
                VBox vbcom2 = new VBox();
                Circle circle = new Circle(50,50,50);
                       
                       ImageView imgv = new ImageView("Ressources/man.png");
                       
                       imgv.setClip(circle);
                       imgv.setFitHeight(120);
                       imgv.setFitWidth(120);
                       
                       hbcom.getChildren().add(imgv);
                LoginController lc = new LoginController();
                
                Label lbln = new Label("User : "+lc.currentUser.getUsername());
                       lbln.setFont(new Font(18));
                       lbln.setTextFill(Color.DARKCYAN);
                       vbcom2.getChildren().add(lbln);
                
                        JFXTextArea areacom1 = new JFXTextArea();
                       areacom1.setFont(new Font(18));
                       areacom1.setPromptText("ajoutez votre commentaire");
                       areacom1.setEditable(true);
                       areacom1.setPrefSize(20, 50);
                       
                       vbcom2.getChildren().add(areacom1);
                       hbcom.getChildren().add(vbcom2);
                       gridpane.add(hbcom, 0, (int)lstComCours.stream().count()+2);
                       
                       btnco.setOnAction(even->
                       { try{
                           Commentaires c = new Commentaires();
                           c.setContenu(areacom1.getText());
                           
                           User jober = lc.currentUser;
                           c.setCours(e);
                           c.setJober(jober);
                           c.setContenu(areacom1.getText());
                           Date d = new Date();
                           java.sql.Date sqlDate = new java.sql.Date(d.getTime());
                           c.setDateAjout(sqlDate);
                           c.setDatemodification(null);
                           CrudCommentaires cc = new CrudCommentaires();
                           
                          
                               cc.add(c);
                               Alert a = new Alert(Alert.AlertType.INFORMATION, "votre commentaire est ajouté avec succes");
                               a.show();
                               a.showAndWait().ifPresent(rs -> {
                                 if (rs == ButtonType.OK) {
                                System.out.println("Pressed OK.");
                                }
                               gridpane.add(hbcom, 0, (int)lstComCours.stream().count()+2);
                           });
                                       }
                       catch(Exception exception)
                           {
                               Alert a = new Alert(Alert.AlertType.ERROR, "votre commentaire est ajouté avec succes");
                               a.show();
                               
                           }
                       });
                       ScrollPane scoll = new ScrollPane();
                       scoll.setContent(gridpane);
                       
                    Scene dialogScene = new Scene(scoll, 700, 500);
                    
                    dialog.setScene(dialogScene);
                    
                    dialog.show();
                    }
                    );

//                vb4.getChildren().add(btnshow);
//                vb4.getChildren().add(btnpdf);
//                vb4.setAlignment(Pos.TOP_LEFT);
                Button btnpartage = new Button();
                ImageView partage = new ImageView("Ressources/facebook.png");
                
                partage.setFitWidth(80);
                partage.setFitHeight(80);
                btnpartage.setGraphic(partage);
                btnpartage.setOnAction(ev->
                {
                    this.partageFB(e.getTuto(),e.getTitre(),e.getDescription());
                }
                );
                
                HBox hb6 = new HBox();
                hb6.getChildren().add(btnpartage);
                hb6.getChildren().add(btnshow);
                hb6.getChildren().add(btncomments);
                hcours.getChildren().add(hb6);

                GridPane coursgp = new GridPane();
               
                vb1.getChildren().add(hcours);
            }
            );
            
            scroll1.setContent(vb1);
//        } catch (IOException ex) {
//           // Logger.getLogger(JoberFormationsController.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }
  
    public void partageFB(String video,String titre,String description){
    
     String accessToken = "EAACEdEose0cBAEV5TYrS6AmxK2ull9d5mi9GRgaTydt6w0ADMEjShnW2TSrUi9vIGszyZBJ30cGnhasIIEZAORXelndL3ZA87YMAL1yQAPCgPsgo7cvNq4xZAukVGeyNOE07PZBkxtc0R1F4jQfXQ57wI58vz9AueqizcEbmUa0VTFqsOhPGKnrr4P2n6VSz1ckvPef3QywZDZD";
         Scanner s = new Scanner(System.in);
        FacebookClient fbClient= new DefaultFacebookClient(accessToken);
         FacebookType response = fbClient.publish("me/feed", FacebookType.class,
                           Parameter.with("message", "Cours publié par social pro : Titre: "+titre+" description : "+description+" video :"+video ));
         System.out.println("ok");
         System.out.println("fb.com/"+response.getId());
    
    
    
    
    }
//    {try{
//                System.out.println("koko");
//           String accessToken = "EAACEdEose0cBAEV5TYrS6AmxK2ull9d5mi9GRgaTydt6w0ADMEjShnW2TSrUi9vIGszyZBJ30cGnhasIIEZAORXelndL3ZA87YMAL1yQAPCgPsgo7cvNq4xZAukVGeyNOE07PZBkxtc0R1F4jQfXQ57wI58vz9AueqizcEbmUa0VTFqsOhPGKnrr4P2n6VSz1ckvPef3QywZDZD";
//               Scanner s = new Scanner(System.in);
//        FacebookClient fbClient= new DefaultFacebookClient(accessToken);
//         FacebookType response = fbClient.publish("me/feed", FacebookType.class,
//                
//                 
//                 Parameter.with("message", "Cours publié par social pro : Titre: "+titre+" description : "+description+" video :"+video ));
//         
//     TrayNotification tr = new TrayNotification("succes de partage", "Partage du cours sur facebook a été effectué avec succes", NotificationType.SUCCESS);
//    tr.showAndWait();
//    }
//    catch(Exception e)
//    {
//       }
//    }

}
