/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Cours;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialproo.services.impl.CrudCours;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author m_s info
 */
public class FormationEntrepController implements Initializable {

    @FXML
    private AnchorPane ap;
    @FXML
    private TableView<?> tv;

    @FXML
    private TableColumn<?, ?> tctuto;

    @FXML
    private TableColumn<?, ?> tcimage;

    @FXML
    private TableColumn<?, ?> tcdateajout;

    @FXML
    private TableColumn<?, ?> tcdatemodification;

    @FXML
    private TableColumn<?, ?> tcspecialite;

    @FXML
    private TableColumn<?, ?> tctitre;

    @FXML
    private TableColumn<?, ?> tcdescription;

    @FXML
    private TableColumn<?, ?> tcid;

    @FXML
    private TableColumn<?, ?> tccourspdf;
    @FXML
    private TableColumn<?, ?> tcetat;

    @FXML
    private JFXButton btnAjouter;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ImageView ajout = new ImageView("Ressources/plus (1).png");

        ajout.setFitWidth(80);
        ajout.setFitHeight(80);
        btnAjouter.setGraphic(ajout);

        List<Cours> lstcours = new ArrayList();
        CrudCours cc = new CrudCours();
        lstcours = cc.getAll();
        tcid.setCellValueFactory(new PropertyValueFactory<>("id"));
        tccourspdf.setCellValueFactory(new PropertyValueFactory<>("courpdf"));
        tcdateajout.setCellValueFactory(new PropertyValueFactory<>("dateAjout"));
        tcdatemodification.setCellValueFactory(new PropertyValueFactory<>("dateModification"));
        tcdescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        tcetat.setCellValueFactory(new PropertyValueFactory<>("etat"));
        tcid.setCellValueFactory(new PropertyValueFactory<>("entreprise"));
        tcimage.setCellValueFactory(new PropertyValueFactory<>("image"));
        tctuto.setCellValueFactory(new PropertyValueFactory<>("tuto"));
        tcspecialite.setCellValueFactory(new PropertyValueFactory<>("specialite"));

        ObservableList l = FXCollections.observableArrayList();
        lstcours.forEach(e -> l.add(e));
        // insertion dans la table

        tv.setItems(l);
        JFXButton btnrefresh = new JFXButton();
        ImageView refresh = new ImageView("Ressources/refresh.png");

        refresh.setFitWidth(80);
        refresh.setFitHeight(80);
        btnrefresh.setGraphic(refresh);
        ap = (AnchorPane) btnAjouter.getParent();
        ap.getChildren().add(btnrefresh);
        btnrefresh.setLayoutX(850);
        btnrefresh.setLayoutY(12.0);
        
        
        JFXButton btnMod = new JFXButton();
        ImageView mod = new ImageView("Ressources/pencil.png");

        mod.setFitWidth(80);
        mod.setFitHeight(80);
        btnMod.setGraphic(mod);
        ap = (AnchorPane) btnrefresh.getParent();
        ap.getChildren().add(btnMod);
        btnMod.setLayoutX(690);
        btnMod.setLayoutY(12.0);
        btnMod.setOnAction(e -> {
            SelectionModel sm = tv.getSelectionModel();
            Cours i = (Cours) (sm.getSelectedItem());
            FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/ModifierCours.fxml"));

        Scene scene;
            try {
                scene = new Scene(fxmlLoader.load());
            
        Stage stage = new Stage();
        stage.setTitle("Modifier cours");
        stage.setScene(scene);
        
        stage.show();
                } catch (IOException ex) {
                Logger.getLogger(FormationEntrepController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        JFXButton btnSupp = new JFXButton();
        ImageView supp = new ImageView("Ressources/delete.png");

        supp.setFitWidth(80);
        supp.setFitHeight(80);
        btnSupp.setGraphic(supp);
        ap = (AnchorPane) btnrefresh.getParent();
        ap.getChildren().add(btnSupp);
        btnSupp.setLayoutX(770);
        btnSupp.setLayoutY(12.0);

        btnSupp.setOnAction(e -> {
            SelectionModel sm = tv.getSelectionModel();
            Cours i = (Cours) (sm.getSelectedItem());
            new CrudCours().delete(i.getId());
             TrayNotification tr = new TrayNotification("Cours supprimé avec succes", "Cours supprimé avec succes", NotificationType.SUCCESS);
    tr.showAndWait();
            this.initialize(url, rb);
        });
        btnrefresh.setOnAction(e
                -> {
            this.initialize(url, rb);
    TrayNotification tr = new TrayNotification("Rafraishissement terminé ", "Votre liste de cous est raffraishie", NotificationType.SUCCESS);
    tr.showAndWait();
        }
        );

    }

    @FXML
    void ajoutercours() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/AjouterCours.fxml"));

        Scene scene = new Scene(fxmlLoader.load());
        Stage stage = new Stage();
        stage.setTitle("Ajout d'un nouveau cours");
        stage.setScene(scene);
        
        stage.show();
    }
   
   
    
    

}
