/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import static java.time.temporal.ChronoUnit.DAYS;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import socialpro.mail.Mail;
import socialproo.models.Entreprise;
import socialproo.models.ListEntreprises;
import socialproo.models.User;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.UserService;
import socialproo.services.interfaces.IUserService;
import socialprotest.SocialProTest;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ValiderEntrepriseController extends Application implements Initializable {

    @FXML
    private TextField tfSearch;
    
    @FXML
    private Button btnLogOut;

    @FXML
    private Button btnRefresh;
    @FXML
    private Button Valider;
    @FXML
    private Button btnRefuser;
    @FXML
    private TableView<ListEntreprises> tblViewEntreprise;
    @FXML
    private MenuItem miView;
    @FXML
    private TableColumn<?, ?> clmEntrepriseId;
    @FXML
    private TableColumn<ListEntreprises, String> clmNmUserName;
    @FXML
    private TableColumn<ListEntreprises, String> clmEmail;
    @FXML
    private TableColumn<ListEntreprises, String> clmConfidentialite;
    @FXML
    private TableColumn<ListEntreprises, String> clmNom;
    @FXML
    private TableColumn<ListEntreprises, String> clmTelephone;
    @FXML
    private Button Voir;
    
    @FXML
    private AnchorPane anchor;
    Task sendingMail;
    String dateajout;
     String datebloque;

    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    IUserService user = new UserService();
        User u = new User();
    ObservableList<ListEntreprises> list = FXCollections.observableArrayList();
    int id;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
        FilteredList<ListEntreprises> filteredData = new FilteredList<>(list, p -> true);
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (per.getUsername().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }else if (Boolean.toString(per.isUpload()).contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<ListEntreprises> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tblViewEntreprise.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tblViewEntreprise.setItems(sortedData);

    }

    private void initCol() {
        clmNmUserName.setCellValueFactory(new PropertyValueFactory<>("username"));
        clmNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        clmEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        clmTelephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        clmConfidentialite.setCellValueFactory(new PropertyValueFactory<>("upload"));

    }

    private void loadData() {

        list = entser.getListtovalide();
        tblViewEntreprise.setItems(list);

    }

    void openPdf(String path) {
       //  ActiviteComplexe selectedCatagory = tblViewActivite.getSelectionModel().getSelectedItem();

    }

    @FXML
    private void tfSearchOnKeyResele(KeyEvent event) {
    }

    @FXML
    private void btnRefreshOnAction(ActionEvent event) {
        loadData();
    }

    @FXML
    private void btnValiderOnAction(ActionEvent event) {
        viewDetails();
    }

    @FXML
    private void btnRefuserOnAction(ActionEvent event) {
         ListEntreprises selectedEntreprise = tblViewEntreprise.getSelectionModel().getSelectedItem();
         System.out.println("valide est " + selectedEntreprise.isValide());
         selectedEntreprise.setValide(true);
         System.out.println("selected"+selectedEntreprise);
        entser.updateValide(selectedEntreprise.getId());
        TrayNotification tn = new TrayNotification();
                            tn.setTitle("SUCCES");
                            tn.setMessage("Entreprise Esta Maintenant Prouvée");
                            tn.setNotificationType(NotificationType.SUCCESS);
                            tn.setAnimationType(AnimationType.SLIDE);
                            tn.showAndWait();

    }

    @FXML
    private void miViewOnAction(ActionEvent event) {
    }

    @FXML
    void btnVoirOnAction(ActionEvent event) {
        ListEntreprises selectedEntreprise = tblViewEntreprise.getSelectionModel().getSelectedItem();
            System.out.println("ID is" + selectedEntreprise.getNom());
        File file = new File("E:\\"+selectedEntreprise.getUsername()+"\\"+selectedEntreprise.getUsername()+".pdf");

        HostServices hostServices = getHostServices();
        hostServices.showDocument(file.getAbsolutePath());

    }

    @FXML
    private void tblViewEntrepriseOnClick(MouseEvent event) {
        if (event.getClickCount() == 1) {
            if(tblViewEntreprise.getSelectionModel().getSelectedItem().isUpload()==false){
            Voir.setDisable(true);
            btnRefuser.setDisable(true);}else
            {
                Voir.setDisable(false);
                 btnRefuser.setDisable(false);
            }
        } else {
            System.out.println("CLICKED");
        }
    }

  
    private void viewDetails() {
        Date dajout = null;
        Date dabloque = null;
        LocalDate dateNow=null;
        LocalDate dateAjoutLocal=null;
        LocalDate dateBloqueLocal=null;
        
        long diff = 0;
        
        if (!tblViewEntreprise.getSelectionModel().isEmpty()) {
            
            ListEntreprises selectedEntreprise = tblViewEntreprise.getSelectionModel().getSelectedItem();
            if(selectedEntreprise.isUpload() && selectedEntreprise.isValide())
            {
                selectedEntreprise.setRole("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}");
                           id=selectedEntreprise.getIduser();
                           u.setId(id);
                             u.setRoles(selectedEntreprise.getRole());
                             System.out.println("user "+u);
                             user.updateRole(u);
            }
            dateajout=selectedEntreprise.getDateajout();
            datebloque=selectedEntreprise.getDatebloque();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd") ;
            try {
                 dajout = f.parse(dateajout);
                 dabloque = f.parse(datebloque);
               
                 dateAjoutLocal=fromDate(dajout);
                 dateBloqueLocal=fromDate(dabloque);
             System.out.println(DAYS.between(dateAjoutLocal,dateBloqueLocal));
                dateNow=LocalDate.now();
                System.out.println("dayNow" + DAYS.between(dateNow,dateBloqueLocal));
                  System.out.println("dayN" + DAYS.between(dateNow,dateAjoutLocal));
                  long v=DAYS.between(dateNow,dateAjoutLocal);
                  long d=DAYS.between(dateNow,dateBloqueLocal);
                  System.out.println("ddd"+v);
                if (v <=0&&d>=0) {
                    
                    System.out.println("enter");
                    
                    if(selectedEntreprise.isUpload()== false){
                        sendEmail();
                         System.out.println("email");
                         TrayNotification tn = new TrayNotification("EMAIL D'ADVERTISSEMENT ENVOYÉ", "Email Envoyé Avec Succés ", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.SLIDE);
                tn.showAndDismiss(Duration.seconds(2));
                    }else{
                        if (selectedEntreprise.isValide()== true) {
                            System.out.println("valide+upload");
                           selectedEntreprise.setRole("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}");
                           id=selectedEntreprise.getIduser();
                           u.setId(id);
                             u.setRoles(selectedEntreprise.getRole());
                             System.out.println("user "+u);
                             user.updateRole(u);
                           
            
            TrayNotification tn = new TrayNotification();
                            tn.setTitle("SUCCES");
                            tn.setMessage("Entreprise Est Validé");
                            tn.setNotificationType(NotificationType.SUCCESS);
                            tn.setAnimationType(AnimationType.SLIDE);
                            tn.showAndWait(); 
                        }else{
                                 System.out.println("validefalse+upload");
                                  sendEmail();
                            System.out.println("email");
                                  TrayNotification tn = new TrayNotification("EMAIL D'ADVERTISSEMENT ENVOYÉ", "Email Envoyé Avec Succés ", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.SLIDE);
                tn.showAndDismiss(Duration.seconds(2));
                        }
                    }    
                }else{
                     System.out.println("bloque");
                    if(selectedEntreprise.isUpload()== false || selectedEntreprise.isValide()== false ){
                        System.out.println("blo");
                        selectedEntreprise.setRole("a:1:{i:0;s:19:\"ROLE_ENTREPRISEBLOQ\";}");
                        id=selectedEntreprise.getIduser();
                           u.setId(id);
                         u.setRoles(selectedEntreprise.getRole());
                        System.out.println("role"+u);
                  user.updateRole(u); 
            TrayNotification tn = new TrayNotification();
                            tn.setTitle("SUCCES");
                            tn.setMessage("Entreprise Est Bloquée");
                            tn.setNotificationType(NotificationType.SUCCESS);
                            tn.setAnimationType(AnimationType.SLIDE);
                            tn.showAndWait(); 
                         System.out.println("user "+u);
                
                    }
                }
              
                
         
                 
            } catch (Exception e) {
            }
            //System.out.println("ID is" +diff);
        } else {
          
            TrayNotification tn = new TrayNotification();
                            tn.setTitle("ATTENTION");
                            tn.setMessage("Veuillez Selectionnez une Entreprise");
                            tn.setNotificationType(NotificationType.WARNING);
                            tn.setAnimationType(AnimationType.SLIDE);
                            tn.showAndWait(); 
            
        }

    }
    public static LocalDate fromDate(Date date) {
    Instant instant = Instant.ofEpochMilli(date.getTime());
    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
        .toLocalDate();
  }
    public void sendEmail()throws Exception {
        ListEntreprises selectedEntreprise = tblViewEntreprise.getSelectionModel().getSelectedItem();
            System.out.println("ID is" + selectedEntreprise.getEmail());
       
        String sub = "Avertissement";
        String content = "Votre Compte va être bloquer si vous n'ajouterez pas un fichier de confidentialité qui prouve votre identité";
               
        
        Task sendingMessage = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
             
             Mail mail = new Mail();
			 //Enter your email and recepient email below as indicated
			 mail.sendMail("rahmasalem03@gmail.com","salahsihem1994.",selectedEntreprise.getEmail(),sub,content);
            
             
             
            return null;
            }
        } ;
        
        
        new Thread(sendingMessage).start();
      
            TrayNotification tn = new TrayNotification("EMAIL ENVOYÉ", "Email Envoyé Avec Succés ", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.SLIDE);
                tn.showAndWait(); 
           

    }
        @FXML
    void btnLogOut(ActionEvent event) throws IOException, Exception {
        loadBloque();
       
        
        

    }
    
     public void loadBloque() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("LogIn -SocialPro");
            nStage.show();
            Stage stage = (Stage) btnLogOut.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   

    @Override
    public void start(Stage primaryStage) throws Exception {

    }
}
