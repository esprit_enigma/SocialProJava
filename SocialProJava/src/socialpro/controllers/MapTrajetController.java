/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.DirectionsPane;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.service.directions.DirectionStatus;
import com.lynden.gmapsfx.service.directions.DirectionsRenderer;
import com.lynden.gmapsfx.service.directions.DirectionsRequest;
import com.lynden.gmapsfx.service.directions.DirectionsResult;
import com.lynden.gmapsfx.service.directions.DirectionsService;
import com.lynden.gmapsfx.service.directions.DirectionsServiceCallback;
import com.lynden.gmapsfx.service.directions.TravelModes;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;



/**
 *
 * @author IMEN
 */
public class MapTrajetController implements Initializable ,MapComponentInitializedListener, DirectionsServiceCallback {
    protected DirectionsService directionsService;
    protected DirectionsPane directionsPane;

    protected StringProperty from = new SimpleStringProperty();
    protected StringProperty to = new SimpleStringProperty();
 @FXML
    protected GoogleMapView mapView;

    @FXML
    protected TextField fromTextField;

    @FXML
    protected TextField toTextField;
    @FXML
    private Button valider;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fromTextField.setText("tunis");
        fromTextField.setVisible(false);
        mapView.addMapInializedListener(this);
        to.bindBidirectional(toTextField.textProperty());
        from.bindBidirectional(fromTextField.textProperty());
        // TODO
    }  
 
    @FXML
    public void toTextFieldAction(ActionEvent event) {
        DirectionsRequest request = new DirectionsRequest(from.get(), to.get(), TravelModes.DRIVING);
        directionsService.getRoute(request, this, new DirectionsRenderer(true, mapView.getMap(), directionsPane));
        System.out.println(fromTextField.getText());
        System.out.println(toTextField.getText());
        // pour recuperer de la base toTextField.getText()
     if (directionsService!= null){
         
          MapOptions options = new MapOptions();

        options.center(new LatLong(47.606189, -122.335842))
                .zoomControl(true)
                .zoom(12)
                .overviewMapControl(false)
                .mapType(MapTypeIdEnum.ROADMAP);
        GoogleMap map = mapView.createMap(options);
        directionsService = new DirectionsService();
        directionsPane = mapView.getDirec();
         DirectionsRequest request1 = new DirectionsRequest(from.get(), to.get(), TravelModes.DRIVING);
        directionsService.getRoute(request1, this, new DirectionsRenderer(true, mapView.getMap(), directionsPane));
     
     }

    }
//     @Override
//    public void directionsReceived(DirectionsResult results, DirectionStatus status) {
//    }
     @Override
    public void mapInitialized() {
        MapOptions options = new MapOptions();

        options.center(new LatLong(47.606189, -122.335842))
                .zoomControl(true)
                .zoom(12)
                .overviewMapControl(false)
                .mapType(MapTypeIdEnum.ROADMAP);
        GoogleMap map = mapView.createMap(options);
        directionsService = new DirectionsService();
        directionsPane = mapView.getDirec();
    }
//     @FXML
//    private void go(ActionEvent event) {
//        
//         ServiceRandonnee srvr = new ServiceRandonnee();
//        ServiceTrajet srvt = new ServiceTrajet();
//if(toTextField.getText().equals("")){
//              TrayNotification tray = new TrayNotification("Erreur", "Précisez votre déstination",ERROR);
//        tray.showAndWait(); }
//        
//        else if(fromTextField.getText().equals("")) {
//             TrayNotification tray = new TrayNotification("Erreur", "Précisez le lieu de votre départ",ERROR);
//        tray.showAndWait();
//        }else {
//        trajet.setDestination(toTextField.getText());
//        trajet.setSource(fromTextField.getText());
//
//
//
//        trajet.setIdrandonnee(idrandonnee);
//
//        srvt.addTrajet(trajet);
//         TrayNotification tray = new TrayNotification("succès", "Trajet ajouté avec succés", SUCCESS);
//        tray.showAndWait();
//    }}

    @Override
    public void directionsReceived(DirectionsResult dr, DirectionStatus ds) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
