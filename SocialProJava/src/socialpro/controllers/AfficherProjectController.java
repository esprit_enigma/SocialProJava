/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;



import socialproo.models.Entreprise;
import socialproo.models.ListProjet;
import socialproo.models.ProjetEntreprise;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.ProjetEntrepriseService;
import socialproo.technique.DataSource;
import socialprotest.ApplicationController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AfficherProjectController implements Initializable {

  
    @FXML
    private TextField tfSearch;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnAddNew;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnDelete;
    @FXML
    private JFXButton btnViewChart;
    @FXML
    private TableView<ListProjet> tblViewProjet;
    @FXML
    private MenuItem miSearch;
    @FXML
    private MenuItem miAddNew;
    @FXML
    private MenuItem miUpdate;
    @FXML
    private MenuItem miDelete;
    @FXML
    private MenuItem miView;

    @FXML
    private JFXButton btnExport;

    @FXML
    private TableColumn<?, ?> clmUnitId;
    @FXML
    private TableColumn<ListProjet, String> clmProjetName;
    @FXML
    private TableColumn<ListProjet, String> clmProjetClient;
    @FXML
    private TableColumn<ListProjet, String> clmProjetDescription;

    @FXML
    private AnchorPane an;
    PieChart myPieChart = new PieChart();

    ApplicationController apCotroller = new ApplicationController();
    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    ProjetEntreprise projet = new ProjetEntreprise();
    ProjetEntrepriseService projetservice = new ProjetEntrepriseService();
    ProjetEntrepriseService sr = new ProjetEntrepriseService();
    int projetId;
    ObservableList<ListProjet> list = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
        FilteredList<ListProjet> filteredData = new FilteredList<>(list, p -> true);
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (per.getClient().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<ListProjet> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tblViewProjet.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tblViewProjet.setItems(sortedData);
    }

    private void initCol() {
        clmProjetName.setCellValueFactory(new PropertyValueFactory<>("nom"));
        clmProjetClient.setCellValueFactory(new PropertyValueFactory<>("client"));
        clmProjetDescription.setCellValueFactory(new PropertyValueFactory<>("description"));

    }

    public void loadData() {

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        list = sr.getList(entreprise.getId());
        tblViewProjet.setItems(list);

    }

    @FXML
    private void tfSearchOnKeyResele(KeyEvent event) {
    }

    @FXML
    private void btnRefreshOnAction(ActionEvent event) {
        loadData();
        FilteredList<ListProjet> filteredData = new FilteredList<>(list, p -> true);
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                } else if (per.getClient().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }

                return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<ListProjet> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tblViewProjet.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tblViewProjet.setItems(sortedData);
    }

    @FXML
    private void btnAddItemOnAction(ActionEvent event) {
        AjouterProjetController ajact = new AjouterProjetController();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/AjouterProjet.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
            AjouterProjetController ajac = fxmlLoader.getController();
           ajac.btnModifier.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void btnUpdateOnAction(ActionEvent event) {
        if (tblViewProjet.getSelectionModel().getSelectedItem() != null) {
            viewDetails();
        } else {
            System.out.println("EMPTY SELECTION");
        }
    }

    @FXML
    private void btnDeleteOnAction(ActionEvent event) {
        ListProjet selectedCatagory = tblViewProjet.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alerte");
        alert.setHeaderText("Confirm");
        alert.setContentText("Vous voulez supprimer ce projet ? \n pour Confirmer cliquez ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());

            projetId = projetservice.findByIdEntreprise(entreprise.getId()).getId();

            projetservice.delete(projetservice.findByIdEntreprise(entreprise.getId()).getId());

            //tblViewProjet.getItems().clear();
            loadData();

        }
    }

    @FXML
    private void miSearchOnAction(ActionEvent event) {
    }

    @FXML
    private void miAddNewOnAction(ActionEvent event) {
    }

    @FXML
    private void miUpdateOnAction(ActionEvent event) {
    }

    @FXML
    private void miDeleteOnAction(ActionEvent event) {
    }

    @FXML
    private void miViewOnAction(ActionEvent event) {
    }

    @FXML
    private void tblViewProjetOnClick(MouseEvent event) {
        if (event.getClickCount() == 2) {
            viewDetails();
        } else {
            System.out.println("CLICKED");
        }
    }

    public void loadAddProjet() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/AjouterProjet.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("SocialPro");
            nStage.show();
            Stage stage = (Stage) btnAddNew.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void viewDetails() {
        if (!tblViewProjet.getSelectionModel().isEmpty()) {
            ListProjet selectedProjet = tblViewProjet.getSelectionModel().getSelectedItem();
            System.out.println("ID is" + selectedProjet.getId());
            //System.out.println(selectedCatagory.getCreatorId());
            String items = selectedProjet.getNom();
            if (!items.equals(null)) {
                AjouterProjetController addActiviteController = new AjouterProjetController();
                // userNameMedia media = new userNameMedia();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/GUI/AjouterProjet.fxml"));
                try {
                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));
                    AjouterProjetController activiteController = fxmlLoader.getController();
                    //media.setId(usrId);
                    //il faut qu'on ajoute un ud dans activiteComplexe
                    activiteController.btnModifier.setVisible(true);
                    activiteController.btnAjouter.setVisible(false);
                    activiteController.projetId = selectedProjet.getId();
                    activiteController.showDetails(selectedProjet.getId());
                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("empty Selection");
        }

    }

    @FXML
    void ViewChart(ActionEvent event) {
       
        try {
           
            CategoryAxis xAxis    = new CategoryAxis();
        xAxis.setLabel("Année");

        NumberAxis yAxis = new NumberAxis(0,10,1);
        yAxis.setLabel("Nombre Des Projets");

        BarChart barChart = new BarChart(xAxis, yAxis);



            barChart.setTitle("Project");
            barChart.setPrefSize(500, 500);
            //myPieChart.setStartAngle(30);
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
            barChart.getData().add(sr.getPie(entreprise.getId()));

            an.getChildren().addAll(barChart);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @FXML
    void ExportChartAction(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        // Set extension filter
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Excel Files(*.xls)", "*.xls");
        chooser.getExtensionFilters().add(filter);
        // Show save dialog
        File file = chooser.showSaveDialog(btnExport.getScene().getWindow());
        if (file != null) {
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
            sr.saveXLSFile(file, entreprise.getId());

        }
    }




}
