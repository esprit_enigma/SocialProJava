/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Jober;
import socialproo.models.Relation;
import socialproo.models.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import socialproo.services.impl.RelationService;

/**
 *
 * @author oudayblouza
 */
public class ProfileDetailController implements Initializable {

    private static User user;
    private static Jober j;
    Stage stage;
    Scene scene;
    Parent root;

    @FXML
    private TextField ville;

    @FXML
    private Group grp;

    @FXML
    private TextField nature;

    @FXML
    private TextField specialite;

    @FXML
    private TextArea description;

    @FXML
    private TextField adress;

    @FXML
    private TextField sexe;

    @FXML
    private TextField numTel;

    @FXML
    private TextField nom;

    @FXML
    private TextField dateNaiss;

    @FXML
    private Button detail;

    @FXML
    private Button abonner;

    @FXML
    private TextField delegation;

    @FXML
    private StackPane sp;

    @FXML
    private TextField prenom;

    @FXML
    private TextField email;

    @FXML
    private TextField username;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ville.setEditable(false);
        ville.setText(j.getDelegation().getVille().getNom());
        delegation.setEditable(false);
        delegation.setText(j.getDelegation().getNom());
        nature.setEditable(true);
        nature.setText(j.getSpecialite().getNature().getNom());
        specialite.setEditable(false);
        specialite.setText(j.getSpecialite().getNom());
        sexe.setEditable(false);
        sexe.setText(j.getSexe());
        adress.setEditable(false);
        adress.setText(j.getAdresse());
        description.setEditable(false);
        description.setText(j.getDescription());
        nom.setEditable(false);
        nom.setText(j.getNom());
        prenom.setEditable(false);
        prenom.setText(j.getPrenom());
        dateNaiss.setEditable(false);
        dateNaiss.setText(j.getDateDeNaissance().toString());
        username.setEditable(false);
        username.setText(user.getUsername());
        email.setEditable(false);
        email.setText(user.getEmail());
        numTel.setEditable(false);
        numTel.setText("" + j.getNumTel());
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        ProfileDetailController.user = user;
    }

    public static Jober getProfil() {
        return j;
    }

    public static void setProfil(Jober profil) {
        ProfileDetailController.j = profil;
    }
//     public void SignOut() throws IOException {
//        LoginController.setCurrentUser(null);
//        stage = (Stage) signoutlabel.getScene().getWindow();
//        root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
//        scene = new Scene(root);
//        stage.setScene(scene);
//        stage.show();
//    }

    public void AbonneDesabonne() {
        RelationService rs = new RelationService();
        Relation r = rs.findByIdFollowerIdFollowed(user.getId(), j.getUser().getId());
        if (null != r) {
            rs.delete(r.getId());
            abonner.setText("abonné");
        } else if (null == r) {
            Relation newrelation = new Relation(user, j.getUser());
            rs.add(newrelation);
            abonner.setText("Desabonné");
        }
    }
    public void detail() throws IOException{
         DetailCvController.setUser(user);
                    DetailCvController.setProfil(j);
                    stage = (Stage) detail.getScene().getWindow();
                    root = FXMLLoader.load(getClass().getResource("/GUI/DetailCv.fxml"));
                    scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
    }
}
