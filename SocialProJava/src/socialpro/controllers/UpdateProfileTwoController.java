/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import socialproo.models.CodePostal;
import socialproo.models.Delegation;
import socialproo.models.Entreprise;
import socialproo.models.Ville;
import socialproo.services.impl.CodePostalService;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.Functions;
import socialproo.services.impl.VilleService;
import socialproo.services.interfaces.INatureVille;
import socialproo.services.interfaces.IService;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class UpdateProfileTwoController implements Initializable {
    @FXML
    private JFXTextField tfNom;
    @FXML
    private JFXTextField tfUNationalite;
    @FXML
    private JFXTextField tfTelephone;
    @FXML
    private JFXTextField tfEmail;
    @FXML
    private JFXTextField tfAdresse;
    @FXML
    private JFXTextField tfSiteWeb;
    @FXML
    private JFXTextArea tfDescription;
    @FXML
    private JFXTextField tfFacebook;
    @FXML
    private JFXTextField tfLinkedin;
    @FXML
    private JFXTextField tfTwitter;
    @FXML
    private JFXTextField tfGoogle;
    @FXML
    private JFXComboBox<String> cbVille;
    @FXML
    private JFXComboBox<String> cbDelegation;
    @FXML
    private JFXComboBox<String> cbCodePostal;
    @FXML
    private JFXButton btnAttachImag;
    @FXML
    private TextField tfFile;
    @FXML
    private JFXButton btnAttachFile;
    @FXML
    private JFXButton btnModifier;
    @FXML
    private ImageView imvUsrImg;

        private File file;
    private BufferedImage bufferedImage;
    private Image image;
private Functions functions;
    private String imagePath;
    Entreprise entreprise = new Entreprise();
     public String vileName;
    public int vileId;
    public String delegationName;
    String delegationN;
    String codepostalName;
     socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        //System.out.println("Affiche" + LoginController.getInstance().getUser().getId());
     //C:\Users\ASUS\Documents\NetBeansProjects\SocialProTest\src\socialpro\images\pdfEntreprises
        EntrepriseService entser = new EntrepriseService();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         functions = new Functions();
//         C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\
        File f = new File("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\"+ LoginController.getInstance().getUser().getUsername());
          System.out.println("username"+LoginController.getInstance().getUser().getUsername());
       // socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("Affiche" + LoginController.getInstance().getUser().getId());
        Entreprise entreprise = new Entreprise();
        EntrepriseService entser = new EntrepriseService();
//
        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        Image img = new Image("http://localhost/SocialPro/web/LogoEntreprises/" + LoginController.getInstance().getUser().getUsername() + "/" + LoginController.getInstance().getUser().getUsername() + ".jpg");
        imvUsrImg.setImage(img);
        if(f.exists()&&f.isDirectory()){
            System.out.println("exist");
        tfFile.setText(LoginController.getInstance().getUser().getUsername()+".pdf");}
        else
        System.out.println("ne exist pas");
        
        System.out.println("mar" + entreprise.getEmail());
        System.out.println("mar" + entreprise.getSiteWeb());
        System.out.println("hello");
        tfNom.setText(entreprise.getNom());
        tfUNationalite.setText(entreprise.getNationalite());
        tfEmail.setText(entreprise.getEmail());
        tfSiteWeb.setText(entreprise.getSiteWeb());
        tfDescription.setText(entreprise.getDescription());
        tfAdresse.setText(entreprise.getAdresse());
        tfTelephone.setText(String.valueOf(entreprise.getNumTel()));
         
           tfFacebook.setText(entreprise.getFacebook());
            tfTwitter.setText(entreprise.getTwitter());
         tfLinkedin.setText(entreprise.getLinkedin());
            tfGoogle.setText(entreprise.getGooglePlus());
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
          System.out.println("code"+entreprise.codePostal.getDelegation().getVille().getNom());
         socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        
        if(entreprise.getCodePostal() == null){
            System.out.println("nullentreprise");
            cbVille.setPromptText("Ville");
            cbCodePostal.setPromptText("Code Postale");
            cbDelegation.setPromptText("Delegation");
      
        }
        else{
              System.out.println("rempentreprise"+entreprise.getCodePostal().getDelegation().getVille().getNom());
              
            cbVille.setPromptText(entreprise.getCodePostal().getDelegation().getVille().getNom());
            cbCodePostal.setPromptText(entreprise.getCodePostal().getNom());
            cbDelegation.setPromptText(entreprise.getCodePostal().getDelegation().getNom());
           
        }
    

    }

    @FXML
    private void cbVilleOnAction(MouseEvent event) {
        List<Ville> villes = new ArrayList<>();
        cbVille.getItems().clear();
        cbVille.setPromptText(null);
        Ville ville = new Ville();
        INatureVille villeservice = new VilleService();
        villes=villeservice.getAll();
        System.out.println(villes);
        for (int i = 0; i < villes.size(); i++) {
            vileName= villes.get(i).getNom();
            cbVille.getItems().remove(vileName);
            cbVille.getItems().add(vileName);
            
        }
    }

    @FXML
    private void cbDelegationOnAction(MouseEvent event) {
        cbDelegation.getItems().clear();
        vileName = cbVille.getSelectionModel().getSelectedItem();
        Ville ville = new Ville();
        Delegation delegation =new Delegation();
        List<Delegation> delegations = new ArrayList<>();
        VilleService villeservice = new VilleService(); 
        ville=villeservice.findByVillename(vileName);
        DelegationService delegationService = new DelegationService();
        delegations=delegationService.findBynv(ville);
        
        for (int i = 0; i < delegations.size(); i++) {
            delegationName= delegations.get(i).getNom();
            cbDelegation.getItems().remove(delegationName);
            cbDelegation.getItems().add(delegationName);}
    }

    @FXML
    private void CodePostalOnAction(MouseEvent event) {
                DelegationService delegationService = new DelegationService();
       
        delegationN=cbDelegation.getSelectionModel().getSelectedItem();
        //delegationNom="hiboun";
    //    System.out.println("deln "+delegationN);
        Delegation del = new Delegation();
        del= delegationService.findByDelegationNom(delegationN);
        //System.out.println("del "+del.getNom());
        CodePostal codepostal = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codepostal= codepostalservice.findByCodepostalId(del.getId());  
        codepostalName= codepostal.getNom();
            cbCodePostal.getItems().remove(codepostalName);
            cbCodePostal.getItems().add(codepostalName);
        
        

    
    }

    @FXML
    private void btnAttachImageOnAction(ActionEvent event) throws IOException {
        
    socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\" + LoginController.getInstance().getUser().getUsername());
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {

                System.out.println("Failed to create directory!");
            }
        } else {
            try {

                delete(file);

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }

            file.mkdir();

        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG (Joint Photographic Group)", "*.jpg"),
                new FileChooser.ExtensionFilter("JPEG (Joint Photographic Experts Group)", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG (Portable Network Graphics)", "*.png")
        );

        fileChooser.setTitle("Choise a Image File");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            System.out.println(file);
            bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);

            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\" + LoginController.getInstance().getUser().getUsername() + "\\" + LoginController.getInstance().getUser().getUsername() + ".jpg");
                File h = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\"+ LoginController.getInstance().getUser().getUsername() + "\\" + file.getName());
//boolean c;
//File g =Files.move(h.toPath(), t.toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING).toFile();

                ImageIO.write(bufferedImage, "jpg", h);
                if (h.renameTo(t)) {
                    System.out.println("ok");
                } else {
                    System.out.println("non");
                }

            } catch (Exception e) {
            }

            imvUsrImg.setImage(image);
            imagePath = file.getAbsolutePath();
        }
    }

    public void delete(File file)
            throws IOException {

        if (file.isDirectory()) {

            //directory is empty, then delete it
            if (file.list().length == 0) {

                file.delete();
                System.out.println("Directory is deleted : "
                        + file.getAbsolutePath());

            } else {

                //list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);

                    //recursive delete
                    delete(fileDelete);
                }

                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    System.out.println("Directory is deleted : "
                            + file.getAbsolutePath());
                }
            }

        } else {
            //if file, then delete it
            file.delete();
            System.out.println("File is deleted : " + file.getAbsolutePath());
        }
    }


    @FXML
    private void btnAttachFileOnAction(ActionEvent event) throws IOException {
   socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\" + LoginController.getInstance().getUser().getUsername());
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
         else {
            try {

                delete(file);

            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }

            file.mkdir();

        }
        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdf\\"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF", "*.pdf")
        );

        fileChooser.setTitle("Choise a pdf File");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\" + LoginController.getInstance().getUser().getUsername() + "\\" + LoginController.getInstance().getUser().getUsername() + ".pdf");
           // File h =new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdfEntreprises\\"+LoginController.getInstance().getUser().getUsername()+"\\"+file.getName());

                saveFile(file);
                System.out.println(file);
               // EntrepriseService entrepriseService = new EntrepriseService();
               // entrepriseService.updateUpload(LoginController.getInstance().getUser().getId());
                if (file.renameTo(t)) {
                    file.delete();
                } else {
                    System.out.println("non");
                }

            } catch (Exception e) {
            }

            tfFile.setText(file.getName());
        }

    }

    private void saveFile(File f) throws IOException {

        FileWriter writer = new FileWriter("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\" + LoginController.getInstance().getUser().getUsername() + "\\" + f.getName(), true);

        writer.close();
    }

    @FXML
    private void btnModifier(ActionEvent event) {
        if (isValidCondition()) {
            if (functions.isValidInteger(tfTelephone,8)) {
            if(tfFile.getText().isEmpty()){
                TrayNotification tn = new TrayNotification();
            tn.setTitle("ATTENTION");
            tn.setMessage("N'oublier Pas D'attacher Un Fichier De Confidentialité");
            tn.setNotificationType(NotificationType.INFORMATION);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();
            }
            Entreprise entrep = new Entreprise();
            EntrepriseService entser = new EntrepriseService();
            entrep = entser.findByIdUser(LoginController.getInstance().getUser().getId());
            System.out.println("update" + entrep.getNom());
            System.out.println(entrep.toString());
            // entrep.getId();
            entrep.setNom(tfNom.getText());
            entrep.setNationalite(tfUNationalite.getText());
            entrep.setEmail(tfEmail.getText());
            System.out.println("rrr" + tfSiteWeb.getText());
            entrep.setNumTel(Integer.parseInt(tfTelephone.getText()));
            entrep.setDescription(tfDescription.getText());
            entrep.setSiteWeb(tfSiteWeb.getText());
            entrep.setAdresse(tfAdresse.getText());
            entrep.setFacebook(tfFacebook.getText());
            entrep.setTwitter(tfTwitter.getText());
            entrep.setLinkedin(tfLinkedin.getText());
            entrep.setGooglePlus(tfGoogle.getText());

            
System.out.println("uploadeddd"+tfFile.getText());
          if(!tfFile.getText().equals("")){
              entrep.setUpload(true);
              System.out.println("uploadeddd"+entrep.getId());
                entser.updateUpload(entrep.getId());
          }   
         
                
                if(cbCodePostal.getSelectionModel().getSelectedItem()!=null){
        codepostalName=cbCodePostal.getSelectionModel().getSelectedItem();
        CodePostal codePostl = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codePostl=codepostalservice.findByCodepostalName(codepostalName);
        entrep.setCodePostal(codePostl);
        // LoginController.getInstance().getUser().se
      
            System.out.println(entreprise.toString());
        entser.updateCodePostal(entrep);}
          entser.update(entrep);
         TrayNotification tn = new TrayNotification("MODIDIFCATION", "Modification Effectué Avec Succés", NotificationType.SUCCESS);
                tn.setAnimationType(AnimationType.POPUP);
                tn.showAndDismiss(Duration.seconds(2));
           

        }else{TrayNotification tn = new TrayNotification();
            tn.setTitle("NUMÉRO TÉLÉPHONE INVALIDE");
            tn.setMessage("Veuillez Saisir Un Numéro De 8 Chiffres");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();}
    }else {
        TrayNotification tn = new TrayNotification();
            tn.setTitle("ETAT DE SAUVGARDE");
            tn.setMessage("Remplissez Tous Les Champs Obligatoires Avant De Sauvgarder");
            tn.setNotificationType(NotificationType.ERROR);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();

        }
    }
    
    private boolean isValidCondition() {
        boolean registration = false;
        if (nullChecq()) {
            System.out.println("Condition valid");
            registration = true;
        } else {
            System.out.println("Condition Invalid");
            registration = false;
        }
        return registration;
    }

    private boolean nullChecq() {
        boolean nullChecq = false;
        if (tfNom.getText().trim().isEmpty()
                || tfUNationalite.getText().trim().isEmpty()
                || tfTelephone.getText().isEmpty()
                || tfAdresse.getText().isEmpty()
                || tfEmail.getText().isEmpty())
                
                
                 {

            System.out.println("Empty user Name");
            nullChecq = false;
        } else {
            System.out.println("User Name not Empty");
            nullChecq = true;
        }
        return nullChecq;
    }

    
    
}
