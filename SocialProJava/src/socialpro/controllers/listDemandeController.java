/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import socialproo.models.DemandesRecomendation;
import socialproo.models.Notification;
import socialproo.models.Recomendation;
import socialproo.services.impl.DemandesRecomendationService;
import socialproo.services.impl.NotificationService;
import socialproo.services.impl.RecomendationService;

/**
 *
 * @author ASUS
 */
public class listDemandeController implements Initializable {
     @FXML
    VBox  DemRecVbox;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ShowUpdateDems();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void ShowUpdateDems() {
        DemRecVbox.getChildren().clear();
        DemandesRecomendationService drs = new DemandesRecomendationService();
        List<DemandesRecomendation> ldr = drs.getAllByExp(LoginController.getCurrentUser().getId());

        for (DemandesRecomendation d : ldr) {
            HBox hb = new HBox();
            Label l = new Label("Recommendation request from " + d.getExpediteur_id().getUsername());
            Button btn1 = new Button("Delete");
            btn1.setOnAction((ActionEvent event) -> {
                drs.delete(d.getId());
                ShowUpdateDems();
            });
            Button btn2 = new Button("Accepte");
            btn2.setOnAction((ActionEvent event) -> {
                RecomendationService rs = new RecomendationService();
                Recomendation r = new Recomendation(d.getExpediteur_id(), d.getRecepteur_id());
                rs.add(r);
                drs.delete(d.getId());
                NotificationService nss = new NotificationService();
                nss.add(new Notification(d.getExpediteur_id(), d.getRecepteur_id().getUsername()+" accepted your recommandation request! ", "not opened"));
                ShowUpdateDems();
            });
            hb.getChildren().addAll(l, btn1, btn2);
            DemRecVbox.getChildren().add(hb);
        }}
    
}
