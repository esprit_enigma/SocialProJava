package socialpro.controllers;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;;
import java.io.IOException;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import socialproo.models.Delegation;
import socialproo.models.Ville;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.VilleService;
import socialproo.technique.DataSource;
import socialproo.models.AnnonceJober;
import socialproo.models.Specialite;
import socialproo.services.impl.AnnonceJoberService;
import socialproo.services.impl.SpecialiteService;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

public class AjoutAnnonceJoberController implements Initializable {

    @FXML
    private JFXTextField txtTitre;
    @FXML
    private JFXTextField txtSalaire;
    @FXML
    private JFXTextArea txtDescription;
    @FXML
    public Button btnClose;
    @FXML
    private JFXButton btnAjouter;
    @FXML
    private JFXComboBox<String> cbtype;
    @FXML
    private JFXComboBox<String> cbtypeEmploi;
    @FXML
    private JFXComboBox<String> cbniveau;
    @FXML
    private JFXComboBox<String> cbExperience;
    @FXML
    private JFXComboBox<String> cbSpecialite;
    @FXML
    private JFXComboBox<String> cbDelegation;
    @FXML
    private JFXComboBox<String> cbVille;
    private PreparedStatement ps;
    /* @FXML
    private ListView<AnnonceJober> listannonce;
    List<AnnonceJober> ls = new ArrayList<>();*/
    private Connection connection;

    ObservableList<String> optionsTypeEmploi = FXCollections.observableArrayList(
            "mi-temps",
            "plein-temps",
            "a distance"
    );
         ObservableList<String> optionsExperience = FXCollections.observableArrayList(
                "1ans",
                "2ans",
                "3ans",
                "4ans",
                "plus 5ans"
        );
  ObservableList<String> optionsType = FXCollections.observableArrayList(
                "Offre",
                "Demande"
        );
     ObservableList<String> optionsNiveau = FXCollections.observableArrayList(
                "BAC+1",
                "BAC+2",
                "BAC+3",
                "BAC+4",
                "BAC+5"
        );
       
    public AjoutAnnonceJoberController() {
        connection = DataSource.getInstance().getConnection();

    }

    public void btnCloseOnAction(ActionEvent actionEvent) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void btnAjouterAction(ActionEvent event) throws IOException {
        String description = txtDescription.getText();
        String titre = txtTitre.getText();
        String salaire = txtSalaire.getText();
        int s = Integer.parseInt(salaire);

        String type = cbtype.getValue();
        String typeEmploi = cbtypeEmploi.getValue();
        String niveau = cbniveau.getValue();
        String experience = cbExperience.getValue();
        String ss = (String) cbSpecialite.getValue();
        SpecialiteService spse = new SpecialiteService();
        Specialite specialite = spse.findByNom(ss);

        //delegation
        String dd = (String) cbDelegation.getValue();
        DelegationService del = new DelegationService();
        Delegation delegation = del.findByNom(dd);
        AnnonceJoberService a = new AnnonceJoberService();
        AnnonceJober ann = new AnnonceJober(delegation, specialite, 1, type, titre, typeEmploi, description, s, niveau, experience, 0);
        a.add(ann);
      TrayNotification tn = new TrayNotification("AJOUT ANNONCE ENTREPRISE", "Ajout Effectué Avec Succés", NotificationType.SUCCESS);
        tn.setAnimationType(AnimationType.POPUP);
        tn.showAndDismiss(Duration.seconds(2));

    }

    @FXML
    private void cbtypeOnAction(MouseEvent event) {
      
        cbtype.setItems(optionsType);
    }

    @FXML
    private void cbniveauOnAction(MouseEvent event) {

     
        cbniveau.setItems(optionsNiveau);
    }

    @FXML
    private void cbExperienceOnAction(MouseEvent event) {

   
        cbExperience.setItems(optionsExperience);
    }

    @FXML
    private void cbtypeEmploiOnAction(MouseEvent event) {

     
        cbtypeEmploi.setItems(optionsTypeEmploi);
    }
      @FXML
    private void cbSpecialiteOnAction(MouseEvent event) {

          SpecialiteService s = new SpecialiteService();
        List<Specialite> ss = s.getAll();
        ObservableList<String> specialites = FXCollections.observableArrayList();
        ss.forEach(i -> specialites.add(i.getNom()));
        cbSpecialite.setItems(specialites);
    }
        @FXML
    private void cbVilleOnAction(MouseEvent event) {

          //ville choice box
        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();
        ObservableList<String> villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i.getNom()));
        cbVille.setItems(villechoices);
        
        //delegation choice box
        cbVille.getSelectionModel().selectedItemProperty().addListener((v, oldvalue, newvalue) -> {
            String s = (String) cbVille.getValue();
            Ville vvvv = vs.findByNom(s);
            DelegationService ds = new DelegationService();
            List<Delegation> ld = ds.getAllByVilleId(vvvv.getId());
            if (ld != null) {
                ObservableList<String> delegationchoices = FXCollections.observableArrayList();
                ld.forEach(i -> delegationchoices.add(i.getNom()));
                cbDelegation.setItems(delegationchoices);
            }
        });}
   
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {

       

    

    }

}
