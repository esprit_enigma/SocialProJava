/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.StageStyle;
import socialproo.models.Entreprise;
import socialproo.services.impl.EntrepriseService;
import socialprotest.ApplicationController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterReseauSociauxController implements Initializable {

    @FXML
    private JFXTextField tfFacebook;
    @FXML
    private JFXTextField tfLinkedin;
    @FXML
    private JFXTextField tfTwitter;
    @FXML
    private JFXTextField tfGoogle;
   
    @FXML
    private JFXButton btnAjouter;

    ApplicationController apCotroller = new ApplicationController();
    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
    String facebook;
    String twitter;
    String google;
    String linkedin;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
          entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        System.out.println("entreprisefinal"+entreprise);
        if (entreprise.getFacebook() == null) {
            System.out.println("nullentreprise"+entreprise.getFacebook());
            tfFacebook.setPromptText("Facebook");
           // btnModifier.setVisible(false);

        } else {
            System.out.println("rempentreprise" + entreprise.getFacebook());
                        tfFacebook.setPromptText("Facebook");

            tfFacebook.setText(entreprise.getFacebook());
            //btnAjouter.setVisible(false);
        }
        if (entreprise.getLinkedin() == null) {
            System.out.println("nullentreprise");
            tfLinkedin.setPromptText("Linkedin");
            //btnModifier.setVisible(false);

        } else {
            System.out.println("rempentreprise" + entreprise.getLinkedin());
            tfLinkedin.setPromptText("Linkedin");
            tfLinkedin.setText(entreprise.getLinkedin());
            //btnAjouter.setVisible(false);
        }
        if (entreprise.getTwitter() == null) {
            System.out.println("nullentreprise");
            tfTwitter.setPromptText("Twitter");
            //btnModifier.setVisible(false);

        } else {
            System.out.println("rempentreprise" + entreprise.getTwitter());
            tfTwitter.setPromptText("Twitter");
            tfTwitter.setText(entreprise.getTwitter());
           // btnAjouter.setVisible(false);
        }
        if (entreprise.getGooglePlus() == null) {
            System.out.println("nullentreprise");
            tfGoogle.setPromptText("Google Plus");
           // btnModifier.setVisible(false);

        } else {
            System.out.println("rempentreprise" + entreprise.getGooglePlus());
                     tfGoogle.setPromptText("Google Plus");
            tfGoogle.setText(entreprise.getGooglePlus());
           // btnAjouter.setVisible(false);
        }
         
    }
    
   
    
    @FXML
    private void btnSaveReseaux(ActionEvent event) {
        if (isNotNull() ) {           
           entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
           facebook=tfFacebook.getText();
           entreprise.setFacebook(facebook);
           twitter=tfTwitter.getText();
           entreprise.setTwitter(twitter);
           linkedin=tfLinkedin.getText();
           entreprise.setLinkedin(linkedin);
           google=tfGoogle.getText();
           entreprise.setGooglePlus(google);
           entser.updateReseaux(entreprise);
           System.out.println(entreprise.toString());
           
           Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Succès");
            alert.setHeaderText("Succès : Ajout ");
            alert.setContentText("Ajout effectué avec Succès");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
           
                  
        }
    }
    
     public boolean isNotNull() {
        boolean isNotNull;
        if ( tfFacebook.getText().isEmpty() && tfTwitter.getText().isEmpty() && tfLinkedin.getText().isEmpty() && tfGoogle.getText().isEmpty()) {
         
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur : Champs Vide ");
            alert.setContentText("Veuillez remplir au moins un champs");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            

            isNotNull = false;
        } else {
            
            isNotNull = true;
        }
        return isNotNull;
    }
         
    
    

}
