/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialproo.models.Activite;
import socialproo.models.ActiviteComplexe;
import socialproo.models.AvoirActivite;
import socialproo.models.Entreprise;
import socialproo.services.impl.ActiviteService;
import socialproo.services.impl.AvoirActiviteService;
import socialproo.services.impl.EntrepriseService;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class listFonctionnalitesController implements Initializable {
    @FXML
    private JFXTextField tfSearch;
    @FXML
    private Button btnRefresh;
    @FXML
    private JFXButton btnDelete;
    
     @FXML
    private JFXButton btnAddNew;

    @FXML
    private JFXButton btnUpdate;

    @FXML
    private JFXListView<ActiviteComplexe> listView;
    private ObservableList<ActiviteComplexe> activitecomplexeData = FXCollections.observableArrayList();

    AvoirActiviteService avoiractiviteservice = new AvoirActiviteService();
    int actviteId;
    AvoirActivite av = new AvoirActivite();
    Entreprise entreprise = new Entreprise();
    EntrepriseService entser = new EntrepriseService();
     ObservableList<ActiviteComplexe> avoiractiv = FXCollections.observableArrayList();
      AvoirActiviteService sr = new AvoirActiviteService();
    //entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         initcol();
         loadData();
        
        FilteredList<ActiviteComplexe> filteredData = new FilteredList<>(avoiractiv, p -> true);
     
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                    // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }else if (per.getNomcategorie().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
 
                    return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        //SortedList<ActiviteComplexe> sortedData = new SortedList<>(filteredData);
        Comparator<ActiviteComplexe> comparator = new Comparator<ActiviteComplexe>() {

             @Override
             public int compare(ActiviteComplexe o1, ActiviteComplexe o2) {
                  //To change body of generated methods, choose Tools | Templates.
                 return o1.getNom().compareTo(o2.getNom());
             }
         };
        // 4. Bind the SortedList comparator to the TableView comparator.
        SortedList<ActiviteComplexe> sortedData =filteredData.sorted(comparator);

        // 5. Add sorted (and filtered) data to the table.
        listView.setItems(sortedData);
        
        
    }  
    public void initcol(){Entreprise entreprise = new Entreprise();
           EntrepriseService entser = new EntrepriseService();
       entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
           //entreprise = entser.findByIdUser(16);
        actviteId = avoiractiviteservice.findByIdEntreprise(entreprise.getId()).getActivite().getId();

        activitecomplexeData=avoiractiviteservice.getList(entreprise.getId());
        System.out.println("data "+activitecomplexeData);
        listView.setItems(activitecomplexeData);
        listView.setExpanded(true);
        listView.depthProperty().set(5);
        
        listView.setCellFactory(fonct->new ActiviteCelluleController());}
    
      @FXML
    void btnUpdateOnAction(ActionEvent event) {
           if (listView.getSelectionModel().getSelectedItem() != null) {
            viewDetails();
        } else {
            System.out.println("EMPTY SELECTION");
        }

    }
    
   
    @FXML
    void btnDeleteOnAction(ActionEvent event) {
        ActiviteComplexe selectedCatagory = listView.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Alerte");
        alert.setHeaderText("Confirm");
        alert.setContentText("Vous voulez supprimer cette activite ? \n pour Confirmer cliquez ok");
        alert.initStyle(StageStyle.UNDECORATED);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());

        actviteId = avoiractiviteservice.findByIdEntreprise(entreprise.getId()).getActivite().getId();

        //av = avoiractiviteservice.findByIdActivite(actviteId);
        Activite activite = new Activite();
        ActiviteService as = new ActiviteService();
        activite = as.findById(actviteId);
            System.out.println("tt"+actviteId);
      
        avoiractiviteservice.delete(avoiractiviteservice.findByIdEntreprise(entreprise.getId()).getId());
           as.delete(actviteId);   
          // tblViewActivite.getItems().clear();
           //listView.getItems().clear();
           loadData();

    }}
    private void loadData() {
       
         entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        avoiractiv=sr.getList(entreprise.getId());
          System.out.println("donnes"+avoiractiv);
          
         listView.setItems(avoiractiv);
       
        
      }
        @FXML
    private void btnRefreshOnAction(ActionEvent event) {
    avoiractiv.clear();
       initcol();
      loadData() ;
       FilteredList<ActiviteComplexe> filteredData = new FilteredList<>(avoiractiv, p -> true);
     
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                    // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }else if (per.getNomcategorie().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
 
                    return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        //SortedList<ActiviteComplexe> sortedData = new SortedList<>(filteredData);
        Comparator<ActiviteComplexe> comparator = new Comparator<ActiviteComplexe>() {

             @Override
             public int compare(ActiviteComplexe o1, ActiviteComplexe o2) {
                  //To change body of generated methods, choose Tools | Templates.
                 return o1.getNom().compareTo(o2.getNom());
             }
         };
        // 4. Bind the SortedList comparator to the TableView comparator.
        SortedList<ActiviteComplexe> sortedData =filteredData.sorted(comparator);

        // 5. Add sorted (and filtered) data to the table.
        listView.setItems(sortedData);
        
    }
       @FXML
    void btnAddItemOnAction(ActionEvent event) {
         AjouterActiviteController ajact = new AjouterActiviteController();
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/GUI/AjouterActivite.fxml"));
        try {
            fxmlLoader.load();
            Parent parent = fxmlLoader.getRoot();
            Scene scene = new Scene(parent);
            scene.setFill(new Color(0, 0, 0, 0));
          AjouterActiviteController ajac  = fxmlLoader.getController();
         ajac.btnModifier.setVisible(false);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.initModality(Modality.APPLICATION_MODAL);
            nStage.initStyle(StageStyle.TRANSPARENT);
            nStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
           
    public void loadAddActivite() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/AjouterActivite.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("SocialPro");
            nStage.show();
            Stage stage = (Stage) btnAddNew.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void viewDetails() {
        if (!listView.getSelectionModel().isEmpty()) {
            ActiviteComplexe selectedActivite = listView.getSelectionModel().getSelectedItem();
            System.out.println("ID is"+selectedActivite.getId());
            //System.out.println(selectedCatagory.getCreatorId());
            String items = selectedActivite.getNom();
            if (!items.equals(null)) {
                AjouterActiviteController addActiviteController = new AjouterActiviteController();
               // userNameMedia media = new userNameMedia();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/GUI/AjouterActivite.fxml"));
                try {
                    fxmlLoader.load();
                    Parent parent = fxmlLoader.getRoot();
                    Scene scene = new Scene(parent);
                    scene.setFill(new Color(0, 0, 0, 0));
                    AjouterActiviteController activiteController = fxmlLoader.getController();
                    //media.setId(usrId);
                    //il faut qu'on ajoute un ud dans activiteComplexe
                    activiteController.btnModifier.setVisible(true);
                    activiteController.btnAjouter.setVisible(false);
                    activiteController.actviteId = selectedActivite.getId();
                    System.out.println("chapati id"+selectedActivite.getId());
                    activiteController.showDetails(selectedActivite.getId());
                    Stage nStage = new Stage();
                    nStage.setScene(scene);
                    nStage.initModality(Modality.APPLICATION_MODAL);
                    nStage.initStyle(StageStyle.TRANSPARENT);
                    nStage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("empty Selection");
        }

    }

    
}
