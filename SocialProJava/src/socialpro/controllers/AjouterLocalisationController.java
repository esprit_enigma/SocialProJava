/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import socialproo.models.CodePostal;
import socialproo.models.Delegation;
import socialproo.models.Entreprise;
import socialproo.models.Ville;
import socialproo.services.impl.CodePostalService;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.VilleService;
import socialproo.services.interfaces.INatureVille;
import socialproo.services.interfaces.IdelegationSpecialite;
import socialprotest.ApplicationController;


/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterLocalisationController implements Initializable {
    @FXML
    private Label lblHeader;
    @FXML
    private Button btnAddSupplyer;
    @FXML
    private Button btnAddSupplyer1;
    @FXML
    private JFXComboBox<String> cbVille;
    @FXML
    private JFXComboBox<String> cbDelegation;
    @FXML
    private JFXComboBox<String> cbCodePostal;
    @FXML
    private JFXButton btnAjouter;
    
    @FXML
    private JFXButton btnModifier;
    public String vileName;
    public int vileId;
    public String delegationName;
    String delegationN;
    String codepostalName;
     ApplicationController apCotroller = new ApplicationController();
        //System.out.println("Affiche" + LoginController.getInstance().getUser().getId());
        Entreprise entreprise = new Entreprise();
        EntrepriseService entser = new EntrepriseService();

       

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
          System.out.println("code"+entreprise.codePostal.getDelegation().getVille().getNom());
         ApplicationController apCotroller = new ApplicationController();
        
        if(entreprise.getCodePostal() == null){
            System.out.println("nullentreprise");
            cbVille.setPromptText("Ville");
            cbCodePostal.setPromptText("Code Postale");
            cbDelegation.setPromptText("Delegation");
            btnModifier.setVisible(false);
            
        }
        else{
              System.out.println("rempentreprise"+entreprise.getCodePostal().getDelegation().getVille().getNom());
              
            cbVille.setPromptText(entreprise.getCodePostal().getDelegation().getVille().getNom());
            cbCodePostal.setPromptText(entreprise.getCodePostal().getNom());
            cbDelegation.setPromptText(entreprise.getCodePostal().getDelegation().getNom());
            btnAjouter.setVisible(false);
        }
     
//        btnAjouter.disableProperty().bind(cbVille.selectionModelProperty().isNull());
//        btnModifier.disableProperty().bind(cbVille.selectionModelProperty().isNotNull());
    }    

    
    @FXML
    private void cbDelegationOnAction(MouseEvent event) {
        cbDelegation.getItems().clear();
        vileName = cbVille.getSelectionModel().getSelectedItem();
        Ville ville = new Ville();
        Delegation delegation =new Delegation();
        List<Delegation> delegations = new ArrayList<>();
        VilleService villeservice = new VilleService(); 
        ville=villeservice.findByVillename(vileName);
        DelegationService delegationService = new DelegationService();
        delegations=delegationService.findBynv(ville);
        
        for (int i = 0; i < delegations.size(); i++) {
            delegationName= delegations.get(i).getNom();
            cbDelegation.getItems().remove(delegationName);
            cbDelegation.getItems().add(delegationName);}
       
        

    }
    @FXML
    private void cbVilleOnAction(MouseEvent event) {
        List<Ville> villes = new ArrayList<>();
        cbVille.getItems().clear();
        cbVille.setPromptText(null);
        Ville ville = new Ville();
        INatureVille villeservice = new VilleService();
        villes=villeservice.getAll();
        System.out.println(villes);
        for (int i = 0; i < villes.size(); i++) {
            vileName= villes.get(i).getNom();
            cbVille.getItems().remove(vileName);
            cbVille.getItems().add(vileName);
            
        }
         
    }
        @FXML
    void CodePostalOnAction(MouseEvent event) {
        DelegationService delegationService = new DelegationService();
       
        delegationN=cbDelegation.getSelectionModel().getSelectedItem();
        //delegationNom="hiboun";
        System.out.println("deln "+delegationN);
        Delegation del = new Delegation();
        del= delegationService.findByDelegationNom(delegationN);
        System.out.println("del "+del.getNom());
        CodePostal codepostal = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codepostal= codepostalservice.findByCodepostalId(del.getId());  
        codepostalName= codepostal.getNom();
            cbCodePostal.getItems().remove(codepostalName);
            cbCodePostal.getItems().add(codepostalName);
        
        

    }

    @FXML
    private void btnSaveLocalisation(ActionEvent event) {
        System.out.println("hello");
        if (isNotNull() ) {
            System.out.println("hello again");
        ApplicationController apCotroller = new ApplicationController();
        System.out.println("salut" + LoginController.getInstance().getUser().getId());
        Entreprise entreprise = new Entreprise();
        EntrepriseService entser = new EntrepriseService();

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        codepostalName=cbCodePostal.getSelectionModel().getSelectedItem();
        CodePostal codePostl = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codePostl=codepostalservice.findByCodepostalName(codepostalName);
        entreprise.setCodePostal(codePostl);
        // LoginController.getInstance().getUser().se
            System.out.println(entreprise.toString());
        entser.updateCodePostal(entreprise);
        btnAjouter.setVisible(false);
        btnModifier.setVisible(true);
         Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Succès");
            alert.setHeaderText("Succès : Ajout ");
            alert.setContentText("Ajout effectué avec Succès");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
        
        }
    }

    @FXML
    private void OnClickbtnModifier(ActionEvent event) {
         System.out.println("hello");
        if (isNotNull() ) {
            System.out.println("hello again");
        ApplicationController apCotroller = new ApplicationController();
        System.out.println("salut" + LoginController.getInstance().getUser().getId());
        Entreprise entreprise = new Entreprise();
        EntrepriseService entser = new EntrepriseService();

        entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        codepostalName=cbCodePostal.getSelectionModel().getSelectedItem();
        CodePostal codePostl = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codePostl=codepostalservice.findByCodepostalName(codepostalName);
        entreprise.setCodePostal(codePostl);
        // LoginController.getInstance().getUser().se
            System.out.println(entreprise.toString());
        entser.updateCodePostal(entreprise);
       
         Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Succès");
            alert.setHeaderText("Succès : Modification ");
            alert.setContentText("Modification effectué avec Succès");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
        
        }
    }
    public boolean isNotNull() {
        boolean isNotNull;
        if ( cbVille.getSelectionModel().isEmpty()) {
         
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur : Champs Vide ");
            alert.setContentText("Veuillez remplir tous les champs");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            

            isNotNull = false;
        } else {
            
            isNotNull = true;
        }
        return isNotNull;
    }
    
}
