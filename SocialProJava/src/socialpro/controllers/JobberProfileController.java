/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Jober;
import socialproo.models.Relation;
import socialproo.models.User;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import socialproo.services.impl.JoberService;
import socialproo.services.impl.RelationService;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class JobberProfileController implements Initializable {

    @FXML
    Label usernamelabel, nomlabel, prenomlabel, datenaissancelabel, numtelelabel, emaillabel, sexelabel, addresselabel, villelabel, delegationlabel, apropredemoinlabel,signoutlabel;
    @FXML
    Button menubtn;
    @FXML
    AnchorPane navList;
    @FXML
    VBox UserDetailsVbox, hohoho;
    @FXML
    JFXButton abon;

    Stage stage;
    Scene scene;
    Parent root;

    private static User user;
    private static Jober profil;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        User u = user;
        Jober p = profil;

        RelationService rs = new RelationService();
        Relation r = rs.findByIdFollowerIdFollowed(u.getId(), p.getUser().getId());
        if (null != r) {
            abon.setText("Desabonné");
        } else if (r == null) {
            abon.setText("abonné");
        }

        System.out.println(p.getNom());

        usernamelabel.setText(u.getUsername());
        nomlabel.setText(p.getNom());
        prenomlabel.setText(p.getPrenom());

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String daten = df.format(p.getDateDeNaissance());
        datenaissancelabel.setText(daten);

        numtelelabel.setText(Integer.toString(p.getNumTel()));
        emaillabel.setText(u.getEmail());
        sexelabel.setText(p.getSexe());
        addresselabel.setText(p.getAdresse());
        apropredemoinlabel.setText(p.getDescription());

    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        JobberProfileController.user = user;
    }

    public static Jober getProfil() {
        return profil;
    }

    public static void setProfil(Jober profil) {
        JobberProfileController.profil = profil;
    }

    public void SignOut() throws IOException {
        LoginController.setCurrentUser(null);
        stage = (Stage) signoutlabel.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/GUI/Login.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void AbonneDesabonne() {
        RelationService rs = new RelationService();
        Relation r = rs.findByIdFollowerIdFollowed(user.getId(), profil.getUser().getId());
        if (null != r) {
            rs.delete(r.getId());
            abon.setText("abonné");
        } else if (null == r) {
            Relation newrelation = new Relation(user, profil.getUser());
            rs.add(newrelation);
            abon.setText("Desabonné");
        }
    }
}
