/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import socialproo.models.CodePostal;
import socialproo.models.Delegation;
import socialproo.models.Entreprise;
import socialproo.models.Ville;
import socialproo.services.impl.CodePostalService;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.Functions;
import socialproo.services.impl.UserService;
import socialproo.services.impl.VilleService;
import socialproo.services.interfaces.INatureVille;
import socialproo.services.interfaces.IService;
import socialproo.services.interfaces.IUserService;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class RegistrationProfileController implements Initializable {
    @FXML
    private JFXTextField tfNom;
    @FXML
    private JFXTextField tfUNationalite;
    @FXML
    private JFXTextField tfTelephone;
    @FXML
    private JFXTextField tfEmail;
    @FXML
    private JFXTextField tfAdresse;
    @FXML
    private JFXTextField tfSiteWeb;
    @FXML
    private JFXTextArea tfDescription;
    @FXML
    private JFXTextField tfFacebook;
    @FXML
    private JFXTextField tfLinkedin;
    @FXML
    private JFXTextField tfTwitter;
    @FXML
    private JFXTextField tfGoogle;
    @FXML
    private Button btnRegistration;
    @FXML
    private JFXComboBox<String> cbVille;
    @FXML
    private JFXComboBox<String> cbDelegation;
    @FXML
    private JFXComboBox<String> cbCodePostal;
    @FXML
    private ImageView imvUsrImg;
    @FXML
    private JFXButton btnAttachImag;
    @FXML
    private TextField tfFile;
    @FXML
    private JFXButton btnAttachFile;
     private File file;
    private BufferedImage bufferedImage;
    private Image image;

    private String imagePath;
    Entreprise entreprise = new Entreprise();
  private Functions functions;
    String role;
    int id;
    public String vileName;
    public int vileId;
    public String delegationName;
    String delegationN;
    String codepostalName;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       functions = new Functions();
    }    

    @FXML
    private void btnRegistration(ActionEvent event) throws IOException {
           
         ApplicationController apController = new ApplicationController();
         FXMLLoader loader = new FXMLLoader();
       
            loader.setLocation(getClass().getResource("/GUI/Application.fxml"));
            loader.load();
            Parent parent = loader.getRoot();
            Scene adminPanelScene = new Scene(parent);
            Stage adminPanelStage = new Stage();
            adminPanelStage.setMaximized(true);
        
    if (isValidCondition()) {
        if (functions.isValidInteger(tfTelephone,8)) {
            if(tfFile.getText().isEmpty()){
                TrayNotification tn = new TrayNotification();
            tn.setTitle("ATTENTION");
            tn.setMessage("Il Vaut Reste 3 Jour Pour \n Attacher Un Fichier De Confidentialité");
            tn.setNotificationType(NotificationType.INFORMATION);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();
            }
            socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
            System.out.println("mar" + LoginController.getInstance().getUser().getId());
            entreprise.userId = LoginController.getInstance().getUser();
            entreprise.id = LoginController.getInstance().getUser().getId();
            entreprise.nom = tfNom.getText();
            entreprise.nationalite = tfUNationalite.getText();
            entreprise.email = tfEmail.getText();
            entreprise.numTel = Integer.parseInt(tfTelephone.getText());
            entreprise.description = tfDescription.getText();
            entreprise.emailentrp = tfSiteWeb.getText();
            entreprise.adresse = tfAdresse.getText();
            entreprise.facebook=tfFacebook.getText();
            entreprise.linkedin=tfLinkedin.getText();
            entreprise.googlePlus=tfGoogle.getText();
            entreprise.twitter=tfTwitter.getText();

            IService entrepriseService = new EntrepriseService();
            IUserService userService = new UserService();
            entrepriseService.add(entreprise);
            LoginController.getInstance().getUser().setRoles("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}");
            userService.updateRole(LoginController.getInstance().getUser());
             Entreprise entreprise = new Entreprise();
        EntrepriseService entser = new EntrepriseService();
            entreprise = entser.findByIdUser(LoginController.getInstance().getUser().getId());
        codepostalName=cbCodePostal.getSelectionModel().getSelectedItem();
        CodePostal codePostl = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codePostl=codepostalservice.findByCodepostalName(codepostalName);
        entreprise.setCodePostal(codePostl);
        // LoginController.getInstance().getUser().se
            System.out.println(entreprise.toString());
          
        entser.updateCodePostal(entreprise);
if(!tfFile.getText().isEmpty()){
   entreprise.upload=true;
                entser.updateUpload(LoginController.getInstance().getUser().getId());
}  
                
            TrayNotification tn = new TrayNotification();
                            tn.setTitle("CONNECTER MAINTENANT");
                            tn.setMessage("Votre Copmte a été creer avec succée");
                            tn.setNotificationType(NotificationType.SUCCESS);
                            tn.setAnimationType(AnimationType.SLIDE);
                            tn.showAndWait();   
//                            TrayNotification tn = new TrayNotification("CONNECTER MAINTENANT", "Votre Copmte a été creer avec succée \n pour se connecter cliquer sur ok", NotificationType.SUCCESS);
//                            tn.setAnimationType(AnimationType.SLIDE);
//                            tn.showAndWait();
                
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setTitle("Connecter Maintenant");
//            alert.setHeaderText("Connecter Maintenant");
//            alert.setContentText("Votre Copmte a été creer avec succée \n pour se connecter cliquer sur ok");
//            alert.initStyle(StageStyle.UNDECORATED);
//            Optional<ButtonType> result = alert.showAndWait();
//            if (result.isPresent() && result.get() == ButtonType.OK)
//            
//            
//            
//            {
              ApplicationController apControl = loader.getController();
                        apControl.onClickProfile(event);
                        apControl.viewDetails();
                        adminPanelStage.setScene(adminPanelScene);
                   
                        adminPanelStage.setTitle("e");
                        adminPanelStage.show();

                        Stage stage = (Stage) btnRegistration.getScene().getWindow();
                        stage.close();
            
//        } 
    }else{TrayNotification tn = new TrayNotification();
            tn.setTitle("NUMÉRO TÉLÉPHONE INVALIDE");
            tn.setMessage("Veuillez Saisir Un Numéro De 8 Chiffres");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();}
    }else {
        TrayNotification tn = new TrayNotification();
            tn.setTitle("ENREGISTREMENT ÉCHOUÉ");
            tn.setMessage("Remplissez Tous Les Champs Obligatoires");
            tn.setNotificationType(NotificationType.ERROR);
            tn.setAnimationType(AnimationType.POPUP);
            tn.showAndWait();

        }

    }

    @FXML
    private void btnAttachImageOnAction(ActionEvent event) throws IOException {
        socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
       
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\" + LoginController.getInstance().getUser().getUsername());
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG (Joint Photographic Group)", "*.jpg"),
                new FileChooser.ExtensionFilter("JPEG (Joint Photographic Experts Group)", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG (Portable Network Graphics)", "*.png")
        );

        fileChooser.setTitle("Choise a Image File");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            System.out.println(file);
            bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);
            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\"+ LoginController.getInstance().getUser().getUsername() + "\\" + LoginController.getInstance().getUser().getUsername() + ".jpg");
                File h = new File("C:\\wamp\\www\\SocialPro\\web\\LogoEntreprises\\" + LoginController.getInstance().getUser().getUsername() + "\\" + file.getName());
//boolean c;
//File g =Files.move(h.toPath(), t.toPath(), java.nio.file.StandardCopyOption.REPLACE_EXISTING).toFile();

                ImageIO.write(bufferedImage, "jpg", h);
                if (h.renameTo(t)) {
                    System.out.println("ok");
                } else {
                    System.out.println("non");
                }

            } catch (Exception e) {
            }

            imvUsrImg.setImage(image);
            imagePath = file.getAbsolutePath();
        }
    }

    @FXML
    private void btnAttachFileOnAction(ActionEvent event) throws IOException {
        socialprotest.ApplicationController apCotroller = new socialprotest.ApplicationController();
        System.out.println("mar" + LoginController.getInstance().getUser().getId());
        File file = new File("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\" + LoginController.getInstance().getUser().getUsername());
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        FileChooser fileChooser = new FileChooser();
        //fileChooser.setInitialDirectory(new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdf\\"));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF", "*.pdf")
        );

        fileChooser.setTitle("Choise a pdf File");

        file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                File t = new File("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\"+ LoginController.getInstance().getUser().getUsername() + "\\" + LoginController.getInstance().getUser().getUsername() + ".pdf");
           // File h =new File("C:\\Users\\ASUS\\Documents\\NetBeansProjects\\SocialProTest\\src\\socialpro\\images\\pdfEntreprises\\"+LoginController.getInstance().getUser().getUsername()+"\\"+file.getName());

                saveFile(file);
                
               
                System.out.println(file);
                if (file.renameTo(t)) {
                    file.delete();
                } else {
                    System.out.println("non");
                }
 
            } catch (Exception e) {
            }
            
            tfFile.setText(file.getName());
        }

    }

    private void saveFile(File f) throws IOException {

        FileWriter writer = new FileWriter("C:\\wamp\\www\\SocialPro\\web\\ValideEntreprises\\" + LoginController.getInstance().getUser().getUsername() + "\\" + f.getName(), true);

        writer.close();
    }
     private boolean isValidCondition() {
        boolean registration = false;
        if (nullChecq()) {
            System.out.println("Condition valid");
            registration = true;
        } else {
            System.out.println("Condition Invalid");
            registration = false;
        }
        return registration;
    }
// functions.isValidInteger(tfTelephone, 10)) 
    private boolean nullChecq() {
        boolean nullChecq = false;
        if (tfNom.getText().trim().isEmpty()
                || tfUNationalite.getText().trim().isEmpty()
                || tfTelephone.getText().isEmpty()
                || tfAdresse.getText().isEmpty()
                || tfEmail.getText().isEmpty()
                || cbVille.getSelectionModel().getSelectedItem().isEmpty()
                || cbCodePostal.getSelectionModel().getSelectedItem().isEmpty()
                || cbDelegation.getSelectionModel().getSelectedItem().isEmpty()
                || imagePath.isEmpty())
                
                 {

            System.out.println("Empty user Name");
            nullChecq = false;
        } else {
            System.out.println("User Name not Empty");
            nullChecq = true;
        }
        return nullChecq;
    }

    public void loadHome() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Application.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("SocialPro");
            nStage.show();
            Stage stage = (Stage) btnRegistration.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @FXML
    private void cbDelegationOnAction(MouseEvent event) {
        cbDelegation.getItems().clear();
        vileName = cbVille.getSelectionModel().getSelectedItem();
        Ville ville = new Ville();
        Delegation delegation =new Delegation();
        List<Delegation> delegations = new ArrayList<>();
        VilleService villeservice = new VilleService(); 
        ville=villeservice.findByVillename(vileName);
        DelegationService delegationService = new DelegationService();
        delegations=delegationService.findBynv(ville);
        
        for (int i = 0; i < delegations.size(); i++) {
            delegationName= delegations.get(i).getNom();
            cbDelegation.getItems().remove(delegationName);
            cbDelegation.getItems().add(delegationName);}
       
        

    }
    @FXML
    private void cbVilleOnAction(MouseEvent event) {
        List<Ville> villes = new ArrayList<>();
        cbVille.getItems().clear();
        cbVille.setPromptText(null);
        Ville ville = new Ville();
        INatureVille villeservice = new VilleService();
        villes=villeservice.getAll();
        System.out.println(villes);
        for (int i = 0; i < villes.size(); i++) {
            vileName= villes.get(i).getNom();
            cbVille.getItems().remove(vileName);
            cbVille.getItems().add(vileName);
            
        }
         
    }
        @FXML
    void CodePostalOnAction(MouseEvent event) {
        DelegationService delegationService = new DelegationService();
       
        delegationN=cbDelegation.getSelectionModel().getSelectedItem();
        //delegationNom="hiboun";
        System.out.println("deln "+delegationN);
        Delegation del = new Delegation();
        del= delegationService.findByDelegationNom(delegationN);
        System.out.println("del "+del.getNom());
        CodePostal codepostal = new CodePostal();
        CodePostalService codepostalservice = new CodePostalService();
        codepostal= codepostalservice.findByCodepostalId(del.getId());  
        codepostalName= codepostal.getNom();
            cbCodePostal.getItems().remove(codepostalName);
            cbCodePostal.getItems().add(codepostalName);
        
        

    }

    
}
