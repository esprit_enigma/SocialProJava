/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

/**
 *
 * @author oudayblouza
 */
import socialproo.models.Experience;
import socialproo.models.Jober;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Date;

import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

import javafx.scene.layout.StackPane;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialproo.services.impl.ExperiencesService;
import socialproo.services.impl.JoberService;

import ui.DateUtils;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterExperienceController implements Initializable {

    @FXML
    private JFXTextField tfPoste;
    @FXML
    private JFXTextField tfOrganisation;
    @FXML
    private JFXTextArea taDescription;
    @FXML
    private StackPane root;
        @FXML
    private StackPane stackexp;
    @FXML
    public JFXButton closeAjouter, close;
    @FXML
    private Button closeClose;
    @FXML
    private DatePicker itemdate, itemdate1;

    Jober jober = new Jober();
    JoberService joberser = new JoberService();
    Experience experience = new Experience();
    ExperiencesService experienceservice = new ExperiencesService();

    ;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Image imgclose = new Image("/Ressources/close_new.png");
        
                     Image img = new Image("Ressources/experience.jpg");
 
        BackgroundImage myBI1 = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
        stackexp.setBackground(new Background(myBI1));
        
        ImageView imgvb = new ImageView(imgclose);
        imgvb.setFitHeight(30);
        imgvb.setFitWidth(30);
        close.setGraphic(imgvb);
        close.setButtonType(JFXButton.ButtonType.RAISED);
        DropShadow shadow = new DropShadow();
        Image img2 = new Image("Ressources/back.jpg");
        BackgroundImage myBI = new BackgroundImage(img2, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
        
        
        close.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                close.setEffect(shadow);

            }
        });

        close.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                close.setEffect(null);
            }
        });

        root.setStyle("-fx-background-color: null;");
      
        root.setPadding(new Insets(10));
        Rectangle innerRect = new Rectangle();
        Rectangle outerRect = new Rectangle();

        root.layoutBoundsProperty().addListener(
                (observable, oldBounds, newBounds) -> {
                    innerRect.relocate(
                            newBounds.getMinX() + 500,
                            newBounds.getMinY() + 500
                    );
                    innerRect.setWidth(newBounds.getWidth() - 500 * 2);
                    innerRect.setHeight(newBounds.getHeight() - 500 * 2);

                    outerRect.setWidth(newBounds.getWidth());
                    outerRect.setHeight(newBounds.getHeight());

                    Shape clip = Shape.subtract(outerRect, innerRect);
                    root.setClip(clip);
                }
        );

    }

    @FXML
    private void AjouterAction(ActionEvent event) throws FileNotFoundException {
        if ((!tfPoste.getText().equals("")) && (!taDescription.getText().equals("")) && (!tfOrganisation.getText().equals("")) && (itemdate.getValue() != null) && (itemdate1.getValue() != null)) {
            jober = joberser.findByUser(LoginController.currentUser);
            experience.setPoste(tfPoste.getText());
            experience.setDescription(taDescription.getText());
            experience.setOrganisation(tfOrganisation.getText());
            experience.setJober(jober);
            Date d1 = DateUtils.asDate(itemdate.getValue());
            experience.setDateDebut(d1);
            Date d2 = DateUtils.asDate(itemdate1.getValue());
            experience.setDateFin(d2);
            Date d3 = new Date();
            experience.setDateAjout(d3);

            experienceservice.add(experience);

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Succès");
            alert.setHeaderText("Succès : Ajout ");
            alert.setContentText("Ajout effectué avec Succès");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();

            Stage stage = (Stage) close.getScene().getWindow();
            stage.close();
            JoberController jc = new JoberController();
            jc.refresh();
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Informations Incompletes");
            alert.setContentText("Veuillez remplir tous les champs");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
        }

    }

    @FXML
    private void closeCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }

    public void datecompar() {
        DateUtils du = new DateUtils();
        if ((itemdate.getValue() != null) && (itemdate1.getValue() != null)) {
            du.comparedate(itemdate, itemdate1);
        }
    }
}
