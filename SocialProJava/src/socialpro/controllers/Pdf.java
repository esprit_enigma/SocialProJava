
package socialpro.controllers;

import socialproo.models.Avoir_Competance;
import socialproo.models.Experience;
import socialproo.models.Formation;
import socialproo.models.Jober;
import socialproo.models.Projet;
import socialproo.models.User;
import com.itextpdf.text.Chunk;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.control.Alert;
import javafx.stage.Stage;
import socialproo.services.impl.Avoir_CompetanceService;
import socialproo.services.impl.ExperiencesService;
import socialproo.services.impl.FormationService;
import socialproo.services.impl.ProjetService;
import socialproo.services.impl.StageService;

public class Pdf {

    private static User user;
    private static Jober j;

    public void generer() {

        try {

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("C:/Users/ASUS/Documents/PDF/" + j.getNom() + "_" + j.getPrenom() + "_CV.pdf"));
            document.open();

            Font font = new Font(FontFamily.HELVETICA, 30, Font.BOLD);
            Paragraph p1 = new Paragraph(new Paragraph("Curriculum Vitae" + "\n\n", font));
            p1.setAlignment(Element.ALIGN_CENTER);

            Font font1 = new Font(FontFamily.HELVETICA, 20, Font.BOLD);
            Paragraph p2 = new Paragraph(new Paragraph(j.getNom() + " " + j.getPrenom() + "\n", font1));

            SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
            String d1 = formater.format(j.getDateDeNaissance());

            Font font2 = new Font(FontFamily.HELVETICA, 12, Font.ITALIC);

            Paragraph p3 = new Paragraph(new Paragraph("Né le " + d1, font2));

            Font font3 = new Font(FontFamily.HELVETICA, 15, Font.BOLD);
            Font font4 = new Font(FontFamily.HELVETICA, 12, Font.BOLD);
            Paragraph p4 = new Paragraph("Spécialisé en : " + j.getSpecialite().getNom(), font2);
            Paragraph p5 = new Paragraph("Habite à : " + j.getAdresse() + " , " + j.getDelegation().getNom(), font2);
            Paragraph p6 = new Paragraph("Num Tel  : " + j.getNumTel(), font2);
            Paragraph p7 = new Paragraph("Description  : " + j.getDescription(), font2);
            Paragraph p8 = new Paragraph("Email  : " + j.getUser().getEmail() + "\n\n", font2);

            Paragraph p9 = new Paragraph("Formation \n", font1);
            PdfPTable table = new PdfPTable(1);

            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            table.addCell(p9);

            document.add(p1);
            document.add(p2);
            document.add(p3);
            document.add(p4);
            document.add(p5);
            document.add(p6);
            document.add(p7);
            document.add(p8);

            document.add(table);

            document.add(new Paragraph("\n", font2));
            FormationService fs = new FormationService();
            List<Formation> f = fs.findByJober(j);

            f.forEach(e -> {
                PdfPTable t1 = new PdfPTable(2);
                t1.getDefaultCell().setBorder(0);
                Paragraph p10 = new Paragraph(e.getDateDebut() + " à " + e.getDateFin(), font4);
                Phrase ph1 = new Phrase("Institut : ", font4);
                Phrase ph2 = new Phrase(e.getInstitut(), font2);
                Paragraph p11 = new Paragraph();
                p11.add(ph1);
                p11.add(ph2);
                t1.addCell(p10);
                t1.addCell(p11);

                Paragraph p12 = new Paragraph("", font4);
                Phrase ph3 = new Phrase("Diplome : ", font4);
                Phrase ph4 = new Phrase(e.getDiplome(), font2);
                Paragraph p13 = new Paragraph();
                p13.add(ph3);
                p13.add(ph4);
                t1.addCell(p12);
                t1.addCell(p13);

                Paragraph p14 = new Paragraph("", font4);
                Phrase ph5 = new Phrase("Description : ", font4);
                Phrase ph6 = new Phrase(e.getDescription(), font2);
                Paragraph p15 = new Paragraph();
                p15.add(ph5);
                p15.add(ph6);
                t1.addCell(p14);
                t1.addCell(p15);

                try {
                    document.add(t1);
                } catch (DocumentException ex) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
                Paragraph p16 = new Paragraph("\n");
                try {
                    document.add(p16);
                } catch (DocumentException ex) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            Paragraph p20 = new Paragraph("Experience \n", font1);
            PdfPTable table1 = new PdfPTable(1);

            table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table1.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            table1.addCell(p20);
            document.add(table1);
            document.add(new Paragraph("\n", font2));

            ExperiencesService es = new ExperiencesService();
            List<Experience> ex = es.findByJober(j);

            ex.forEach(e -> {
                PdfPTable t1 = new PdfPTable(2);
                t1.getDefaultCell().setBorder(0);
                Paragraph p10 = new Paragraph(e.getDateDebut() + " à " + e.getDateFin(), font4);
                Phrase ph1 = new Phrase("Poste : ", font4);
                Phrase ph2 = new Phrase(e.getPoste(), font2);
                Paragraph p11 = new Paragraph();
                p11.add(ph1);
                p11.add(ph2);
                t1.addCell(p10);
                t1.addCell(p11);

                Paragraph p12 = new Paragraph("", font4);
                Phrase ph3 = new Phrase("Organisation : ", font4);
                Phrase ph4 = new Phrase(e.getOrganisation(), font2);
                Paragraph p13 = new Paragraph();
                p13.add(ph3);
                p13.add(ph4);
                t1.addCell(p12);
                t1.addCell(p13);

                Paragraph p14 = new Paragraph("", font4);
                Phrase ph5 = new Phrase("Description : ", font4);
                Phrase ph6 = new Phrase(e.getDescription(), font2);
                Paragraph p15 = new Paragraph();
                p15.add(ph5);
                p15.add(ph6);
                t1.addCell(p14);
                t1.addCell(p15);

                try {
                    document.add(t1);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
                Paragraph p16 = new Paragraph("\n");
                try {
                    document.add(p16);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            Paragraph p21 = new Paragraph("Stages \n", font1);
            PdfPTable table2 = new PdfPTable(1);

            table2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table2.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            table2.addCell(p21);
            document.add(table2);
            document.add(new Paragraph("\n", font2));

            StageService ss = new StageService();
            List<socialproo.models.Stage> s = ss.findByJober(j);

            s.forEach(e -> {
                PdfPTable t1 = new PdfPTable(2);
                t1.getDefaultCell().setBorder(0);
                Paragraph p10 = new Paragraph(e.getDateDebut() + " à " + e.getDateFin(), font4);
                Phrase ph1 = new Phrase("Organisation : ", font4);
                Phrase ph2 = new Phrase(e.getOrganisation(), font2);
                Paragraph p11 = new Paragraph();
                p11.add(ph1);
                p11.add(ph2);
                t1.addCell(p10);
                t1.addCell(p11);

                Paragraph p14 = new Paragraph("", font4);
                Phrase ph5 = new Phrase("Description : ", font4);
                Phrase ph6 = new Phrase(e.getDescription(), font2);
                Paragraph p15 = new Paragraph();
                p15.add(ph5);
                p15.add(ph6);
                t1.addCell(p14);
                t1.addCell(p15);

                try {
                    document.add(t1);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
                Paragraph p16 = new Paragraph("\n");
                try {
                    document.add(p16);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            Paragraph p22 = new Paragraph("Projets \n", font1);
            PdfPTable table3 = new PdfPTable(1);

            table3.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table3.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            table3.addCell(p22);
            document.add(table3);
            document.add(new Paragraph("\n", font2));

            ProjetService ps = new ProjetService();
            List<Projet> p = ps.findByJober(j);

            p.forEach(e -> {
                PdfPTable t1 = new PdfPTable(2);
                t1.getDefaultCell().setBorder(0);
                Paragraph p10 = new Paragraph(e.getDateDebut() + " à " + e.getDateFin(), font4);
                Phrase ph1 = new Phrase("Nom  : ", font4);
                Phrase ph2 = new Phrase(e.getNom(), font2);
                Paragraph p11 = new Paragraph();
                p11.add(ph1);
                p11.add(ph2);
                t1.addCell(p10);
                t1.addCell(p11);

                Paragraph p12 = new Paragraph("", font4);
                Phrase ph3 = new Phrase("Organisation", font4);
                Phrase ph4 = new Phrase(e.getOrganisation(), font2);
                Paragraph p13 = new Paragraph();
                p13.add(ph3);
                p13.add(ph4);
                t1.addCell(p12);
                t1.addCell(p13);

                Paragraph p14 = new Paragraph("", font4);
                Phrase ph5 = new Phrase("Description : ", font4);
                Phrase ph6 = new Phrase(e.getDescription(), font2);
                Paragraph p15 = new Paragraph();
                p15.add(ph5);
                p15.add(ph6);
                t1.addCell(p14);
                t1.addCell(p15);

                try {
                    document.add(t1);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
                Paragraph p16 = new Paragraph("\n");
                try {
                    document.add(p16);
                } catch (DocumentException ex1) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            Paragraph p28 = new Paragraph("Competances \n", font1);
            PdfPTable table8 = new PdfPTable(1);

            table8.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table8.getDefaultCell().setVerticalAlignment(Element.ALIGN_CENTER);
            table8.addCell(p28);
            document.add(table8);
            document.add(new Paragraph("\n", font2));

            Avoir_CompetanceService acs = new Avoir_CompetanceService();
            List<Avoir_Competance> ac = acs.findbyJober(j);

            ac.forEach(e -> {
                PdfPTable t1 = new PdfPTable(2);
                t1.getDefaultCell().setBorder(0);
                Paragraph p10 = new Paragraph(e.getCompetance().getNom(), font4);
                Phrase ph1 = new Phrase("Niveau  : ", font4);
                Phrase ph2 = new Phrase("" + e.getNiveau(), font2);
                Paragraph p11 = new Paragraph();
                p11.add(ph1);
                p11.add(ph2);
                t1.addCell(p10);
                t1.addCell(p11);

                try {
                    document.add(t1);
                } catch (DocumentException ex7) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex7);
                }
                Paragraph p16 = new Paragraph("\n");
                try {
                    document.add(p16);
                } catch (DocumentException ex7) {
                    Logger.getLogger(Pdf.class.getName()).log(Level.SEVERE, null, ex7);
                }
            });

            document.addTitle("Cv");
            document.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("pdf avec success");
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("information");
        alert.setHeaderText(null);
        alert.setContentText("la publication a été imprimer en PDF avec success");
        alert.show();
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        Pdf.user = user;
    }

    public static Jober getProfil() {
        return j;
    }

    public static void setProfil(Jober profil) {
        Pdf.j = profil;
    }
}
