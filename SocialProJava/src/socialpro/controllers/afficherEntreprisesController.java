/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import socialproo.models.Entreprise;
import socialproo.models.ListEntreprises;
import socialproo.models.ListProjet;
import socialproo.services.impl.EntrepriseService;
import socialprotest.ApplicationController;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class afficherEntreprisesController implements Initializable {
    @FXML
    private TextField tfSearch;
    @FXML
    private Button btnRefresh;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnBloque;
    @FXML
    private TableView<ListEntreprises> tblViewEntreprise;
    @FXML
    private TableColumn<?, ?> clmEntrepriseId;
    @FXML
    private TableColumn<ListEntreprises, String> clmNmUserName;
    @FXML
    private TableColumn<ListEntreprises, String> clmEmail;
    @FXML
    private TableColumn<ListEntreprises, String> clmNom;
    @FXML
    private TableColumn<ListEntreprises, String> clmTelephone;
    
    
    EntrepriseService entser = new EntrepriseService();
        ObservableList<ListEntreprises> list = FXCollections.observableArrayList();
     

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
         initCol();
      loadData() ;
       FilteredList<ListEntreprises> filteredData = new FilteredList<>(list, p -> true);
        tfSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(per -> {
                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                    // Compare first name and last name of every person with filter text.
                String lowerCaseFilter = newValue.toLowerCase();

                if (per.getNom().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches first name.
                }else if (per.getUsername().toLowerCase().contains(lowerCaseFilter)) {
                    return true; // Filter matches last name.
                }
 
                    return false; // Does not match.
            });
        });
        // 3. Wrap the FilteredList in a SortedList. 
        SortedList<ListEntreprises> sortedData = new SortedList<>(filteredData);

        // 4. Bind the SortedList comparator to the TableView comparator.
        sortedData.comparatorProperty().bind(tblViewEntreprise.comparatorProperty());

        // 5. Add sorted (and filtered) data to the table.
        tblViewEntreprise.setItems(sortedData);
    } 

        private void initCol() {
        clmNmUserName.setCellValueFactory(new PropertyValueFactory<>("username"));
        clmNom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        clmEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        clmTelephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
        
        
    }
      private void loadData() {
            
        list=entser.getList();
        tblViewEntreprise.setItems(list);
       
      } 

    @FXML
    private void tfSearchOnKeyResele(KeyEvent event) {
    }

    @FXML
    private void btnRefreshOnAction(ActionEvent event) {
            loadData() ;
    }

    @FXML
    private void btnDeleteOnAction(ActionEvent event) {
    }

    @FXML
    private void btnBloqueOnAction(ActionEvent event) {
    }

    @FXML
    private void tblViewEntrepriseOnClick(MouseEvent event) {
          if (event.getClickCount() == 2) {
            viewDetails();
        } else {
            System.out.println("CLICKED");
        }
    }
    private void viewDetails() {
        if (!tblViewEntreprise.getSelectionModel().isEmpty()) {
            ListEntreprises selectedProjet = tblViewEntreprise.getSelectionModel().getSelectedItem();
            System.out.println("ID is"+selectedProjet.getId());
            } else {
            System.out.println("empty Selection");
        }

    }
    
}
