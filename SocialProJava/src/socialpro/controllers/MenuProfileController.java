/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class MenuProfileController implements Initializable {
    @FXML
    public BorderPane bpProfile;
    @FXML
    private AnchorPane acHeadProfile;
    @FXML
    public ToggleButton btnProfile;
    @FXML
    private ToggleButton btnModifier;
    @FXML
    private StackPane spMainContent;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ToggleGroup toggleGroup = new ToggleGroup();
        btnProfile.setSelected(true);
        btnProfile.setToggleGroup(toggleGroup);
        btnModifier.setToggleGroup(toggleGroup);
    }    

    @FXML
    public void btnProfileOnAction(ActionEvent event) throws IOException {
        afficherProfileController prof= new afficherProfileController();
        FXMLLoader fXMLLoader = new FXMLLoader();
        
                   fXMLLoader.setLocation(getClass().getResource("/GUI/Profile.fxml"));

            fXMLLoader.load();
        
        
        
        
        
       
         afficherProfileController p = fXMLLoader.getController();
        StackPane acPane = fXMLLoader.getRoot();
        spMainContent.getChildren().clear();
        spMainContent.getChildren().add(acPane);
    }

    @FXML
    private void btnModifierOnAction(ActionEvent event) throws IOException {
        FXMLLoader fXMLLoader = new FXMLLoader();
         fXMLLoader.load(getClass().getResource("/GUI/Update.fxml").openStream());
        StackPane acPane = fXMLLoader.getRoot();
        spMainContent.getChildren().clear();
        spMainContent.getChildren().add(acPane);
    }
    
}
