/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;
import socialproo.models.Activite;
import socialproo.models.AnnonceEntreprise;
import socialproo.models.Delegation;
import socialproo.models.Ville;
import socialproo.services.impl.ActiviteService;
import socialproo.services.impl.AnnonceEntrepriseService;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.VilleService;
import socialproo.technique.DataSource;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 * FXML Controller class
 *
 * @author IMEN
 */
public class AjoutAnnonceEntrepriseController implements Initializable {

    @FXML
    private JFXComboBox<String> cbtype;

    @FXML
    private JFXTextField txtTitre;

    @FXML
    private JFXTextArea txtDetails;

    @FXML
    private JFXTextField txtBudget;

    @FXML
    private JFXComboBox<String> cbActivite;

    @FXML
    private JFXComboBox<String> cbDelegation;

    @FXML
    private JFXComboBox<String> cbVille;

    @FXML
    private JFXButton btnAjouter;
    @FXML
    public Button btnClose;

    private Connection connection;

    public AjoutAnnonceEntrepriseController() {
        connection = DataSource.getInstance().getConnection();
    }

    ObservableList<String> optionsType = FXCollections.observableArrayList(
            "Offre",
            "Demande"
    );

    @FXML
    public void cbActiviteOnAction(MouseEvent actionEvent) {
        ActiviteService s = new ActiviteService();
        List<Activite> ss = s.getAll();
        ObservableList<String> activitechoices = FXCollections.observableArrayList();
        ss.forEach(i -> activitechoices.add(i.getNom()));
       
        cbActivite.setItems(activitechoices);
    }

    @FXML

    public void cbVilleOnAction(MouseEvent actionEvent) {
        //ville choice box
        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();
        ObservableList<String> villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i.getNom()));
        cbVille.setItems(villechoices);

        //delegation choice box
        cbVille.getSelectionModel().selectedItemProperty().addListener((v, oldvalue, newvalue) -> {
            String s = (String) cbVille.getValue();
            Ville vvvv = vs.findByNom(s);
            DelegationService ds = new DelegationService();
            List<Delegation> ld = ds.getAllByVilleId(vvvv.getId());
            if (ld != null) {
                ObservableList<String> delegationchoices = FXCollections.observableArrayList();
                ld.forEach(i -> delegationchoices.add(i.getNom()));
                cbDelegation.setItems(delegationchoices);
            }
        });
    }

    @FXML
    public void btnCloseOnAction(ActionEvent actionEvent) {
        Stage stage = (Stage) btnClose.getScene().getWindow();
        stage.close();
    }

    @FXML
    public void cbtypeOnAction(MouseEvent actionEvent) {
        cbtype.setItems(optionsType);
    }

    @FXML
    public void btnAjouterAction(ActionEvent event) throws IOException {

        String details = txtDetails.getText();
        String titre = txtTitre.getText();
        String budget = txtBudget.getText();
        String type = cbtype.getValue();

        //activite
        String act = cbActivite.getValue();
        ActiviteService ac = new ActiviteService();
        Activite activite = ac.findByNom(act);

        //delegation
        String dd = (String) cbDelegation.getValue();
        DelegationService del = new DelegationService();
        Delegation delegation = del.findByNom(dd);
        
        System.out.println(dd);
        AnnonceEntrepriseService a = new AnnonceEntrepriseService();
        AnnonceEntreprise ann = new AnnonceEntreprise(activite, 2, delegation, type, titre, details, budget, 0);
        a.add(ann);
        TrayNotification tn = new TrayNotification("AJOUT ANNONCE ENTREPRISE", "Ajout Effectué Avec Succés", NotificationType.SUCCESS);
        tn.setAnimationType(AnimationType.POPUP);
        tn.showAndDismiss(Duration.seconds(2));

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
}
