/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Avoir_Competance;

import socialproo.models.Delegation;
import socialproo.models.Experience;
import socialproo.models.Formation;
import socialproo.models.Jober;
import socialproo.models.Nature;
import socialproo.models.Projet;
import socialproo.models.Specialite;
import socialproo.models.Stage;
import socialproo.models.User;
import socialproo.models.Ville;
import com.jfoenix.controls.JFXButton;

import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;

import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;
import socialproo.services.impl.Avoir_CompetanceService;
import socialproo.services.impl.DelegationService;
import socialproo.services.impl.ExperiencesService;
import socialproo.services.impl.FormationService;
import socialproo.services.impl.JoberService;
import socialproo.services.impl.NatureService;
import socialproo.services.impl.ProjetService;
import socialproo.services.impl.SpecialiteService;
import socialproo.services.impl.StageService;
import socialproo.services.impl.UserService;
import socialproo.services.impl.VilleService;
import ui.DateUtils;

/**
 *
 * @author oudayblouza
 */
public class ModifierProfileController implements Initializable {

    @FXML
    private JFXButton genererbtn;
    @FXML
    private AnchorPane ap2;
    @FXML
    private VBox vboxchart;
    @FXML
    private ComboBox<?> ville;

    @FXML
    private Group grp;

    @FXML
    private ComboBox<?> nature;

    @FXML
    private ComboBox<?> specialite;

    @FXML
    private TextArea description;

    @FXML
    private TextField username;
    @FXML
    private ComboBox<?> sexe;

    @FXML
    private TextField adress;

    @FXML
    private Button submitbtn;

    @FXML
    private TextField numTel;

    @FXML
    private TextField nom;

    @FXML
    private ComboBox<?> delegation;

    @FXML
    private StackPane sp;

    @FXML
    private TextField prenom;
    @FXML
    private TextField email;
    @FXML
    private JFXButton lockBtn;
    @FXML
    private ProgressIndicator prog;

    @FXML
    private ProgressBar pbcv;
    @FXML
    private Pane pane1;
    @FXML
    private DatePicker DateDeNai;
    
    
          String messagef = "";
        String messagec = "";
        String messagee = "";
        String messagep = "";
        String messages = "";
     int cv = 1;
    LoginController lc = new LoginController();
    User user = lc.currentUser;
    JoberService js = new JoberService();
    Jober j = js.findByUser(user);
    UserService us = new UserService();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.moncv();

        vboxchart.setAlignment(Pos.CENTER);
        numTel.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    numTel.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }

        });

        Image imgUnlock = new Image("/Ressources/Unlock.png");
        Image imgLock = new Image("/Ressources/Lock.png");

        ImageView imgvUnlock = new ImageView(imgUnlock);
        imgvUnlock.setFitHeight(90);
        imgvUnlock.setFitWidth(90);

        ImageView imgvLock = new ImageView(imgLock);
        imgvLock.setFitHeight(90);
        imgvLock.setFitWidth(90);

        lockBtn.setGraphic(imgvLock);
        lockBtn.setAccessibleText("1");

        lockBtn.setButtonType(JFXButton.ButtonType.RAISED);
        DropShadow shadow = new DropShadow();
        this.lock();

        lockBtn.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {

            if (lockBtn.getAccessibleText().equals("1")) {
                lockBtn.setGraphic(imgvUnlock);
                lockBtn.setAccessibleText("0");
                this.unlock();

            } else {
                lockBtn.setGraphic(imgvLock);
                lockBtn.setAccessibleText("1");
                this.lock();
            }

        });

        description.setText(j.getDescription());
        username.setText(user.getUsername());
        email.setText(user.getEmail());
        adress.setText(j.getAdresse());
        numTel.setText("" + j.getNumTel() + "");
        nom.setText(j.getNom());
        prenom.setText(j.getPrenom());
        LocalDate d1 = DateUtils.asLocalDate(j.getDateDeNaissance());
        DateDeNai.setValue(d1);
    }

    public void unlock() {
        this.combo();
        username.setEditable(true);
        adress.setEditable(true);
        numTel.setEditable(true);
        nom.setEditable(true);
        prenom.setEditable(true);
        DateDeNai.setEditable(true);
        description.setEditable(true);
        submitbtn.setVisible(true);
    }

    public void lock() {
        ville.getItems().clear();
        ObservableList villechoices = FXCollections.observableArrayList();
        villechoices.add(j.getDelegation().getVille().getNom().toString());
        ville.setItems(villechoices);
        ville.getSelectionModel().select(0);

        ObservableList delegationchoices = FXCollections.observableArrayList();
        delegationchoices.add(j.getDelegation().getNom().toString());
        delegation.setItems(delegationchoices);
        delegation.getSelectionModel().select(0);

        ObservableList naturechoices = FXCollections.observableArrayList();
        naturechoices.add(j.getSpecialite().getNature().getNom().toString());
        nature.setItems(naturechoices);
        nature.getSelectionModel().select(0);

        specialite.setEditable(false);
        ObservableList specialitechoices = FXCollections.observableArrayList();
        specialitechoices.add(j.getSpecialite().getNom().toString());
        specialite.setItems(specialitechoices);
        specialite.getSelectionModel().select(0);

        sexe.setEditable(false);
        ObservableList sexechoices = FXCollections.observableArrayList();
        sexechoices.add(j.getSexe().toString());
        sexe.setItems(sexechoices);
        sexe.getSelectionModel().select(0);

        description.setEditable(false);
        username.setEditable(false);
        email.setEditable(false);
        adress.setEditable(false);
        numTel.setEditable(false);
        nom.setEditable(false);
        prenom.setEditable(false);
        DateDeNai.setEditable(false);
        submitbtn.setVisible(false);

    }

    public void combo() {

        //Sexe ComboBox
        ObservableList sexechoices = FXCollections.observableArrayList("Homme", "Femme");
        sexe.setItems(sexechoices);
        int sexeid = sexe.getItems().indexOf(j.getSexe().toString());

        sexe.getSelectionModel().select(sexeid);
        //ville ComboBox
        VilleService vs = new VilleService();
        List<Ville> lv = vs.getAll();
        ObservableList villechoices = FXCollections.observableArrayList();
        lv.forEach(i -> villechoices.add(i.getNom()));
        ville.setItems(villechoices);
        int villeid = ville.getItems().indexOf(j.getDelegation().getVille().toString());

        ville.getSelectionModel().select(villeid);

        //delegation ComboBox
        Ville v1 = vs.findByNom((String) ville.getValue());
        DelegationService ds1 = new DelegationService();
        List<Delegation> ld1 = ds1.findBynv(v1);
        ObservableList delegationchoices1 = FXCollections.observableArrayList();
        ld1.forEach(i -> delegationchoices1.add(i.getNom()));
        delegation.setItems(delegationchoices1);
        ville.setOnAction(e -> {
            Ville v = vs.findByNom((String) ville.getValue());
            DelegationService ds = new DelegationService();
            List<Delegation> ld = ds.findBynv(v);
            if (ld != null) {
                ObservableList delegationchoices = FXCollections.observableArrayList();
                ld.forEach(i -> delegationchoices.add(i.getNom()));
                delegation.setItems(delegationchoices);
            }

        });
        int delegationid = delegation.getItems().indexOf(j.getDelegation().toString());

        delegation.getSelectionModel().select(delegationid);
        //Nature ComboBox
        NatureService ns = new NatureService();
        List<Nature> ln = ns.getAll();
        ObservableList Naturechoices = FXCollections.observableArrayList();
        ln.forEach(n -> Naturechoices.add(n.getNom()));
        nature.setItems(Naturechoices);
        int natureid = nature.getItems().indexOf(j.getSpecialite().getNature().toString());

        nature.getSelectionModel().select(natureid);
        //Specialite ComboBox
        Nature n1 = ns.findByNom((String) nature.getValue());
        SpecialiteService ss1 = new SpecialiteService();
        List<Specialite> ls1 = ss1.findBynv(n1);
        ObservableList specialitechoices1 = FXCollections.observableArrayList();
        ls1.forEach(i -> specialitechoices1.add(i.getNom()));
        specialite.setItems(specialitechoices1);

        nature.setOnAction(e -> {
            Nature n = ns.findByNom((String) nature.getValue());
            SpecialiteService ss = new SpecialiteService();
            List<Specialite> ls = ss.findBynv(n);

            if (ls != null) {
                ObservableList specialitechoices = FXCollections.observableArrayList();
                ls.forEach(i -> specialitechoices.add(i.getNom()));
                specialite.setItems(specialitechoices);
            }

        });
        int specialiteid = specialite.getItems().indexOf(j.getSpecialite().toString());

        specialite.getSelectionModel().select(specialiteid);
    }

    public void update() {
        if ((this.verifUsername()) && (DateDeNai.getValue() != null) && (!prenom.getText().equals("")) && (!nom.getText().equals("")) && (!description.getText().equals("")) && (!numTel.getText().equals(""))) {
            j.setDescription(description.getText());

            j.setAdresse(adress.getText());
            j.setNumTel(Integer.parseInt(numTel.getText().toString()));
            j.setNom(nom.getText());
            j.setPrenom(prenom.getText());
            Date d1 = DateUtils.asDate(DateDeNai.getValue());
            j.setDateDeNaissance(d1);
            j.setSexe(sexe.getValue().toString());
            DelegationService ds = new DelegationService();
            Delegation d = ds.findByNom(delegation.getValue().toString());
            j.setDelegation(d);

            SpecialiteService ss = new SpecialiteService();
            Specialite s = ss.findByNom(specialite.getValue().toString());
            j.setSpecialite(s);

            user.setUsername(username.getText());
            user.setUsername_canonical(username.getText());
            us.update(user);
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Succès");
            alert.setHeaderText("Succès : Modification ");
            alert.setContentText("Modification effectué avec Succès");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            js.update(j);
            Image imgLock = new Image("/Ressources/Lock.png");
            ImageView imgvLock = new ImageView();
            imgvLock.setFitHeight(90);
            imgvLock.setFitWidth(90);
            imgvLock.setImage(imgLock);

            this.lock();
            lockBtn.setGraphic(imgvLock);
            lockBtn.setAccessibleText("1");
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Informations Incompletes");
            alert.setContentText("Veuillez remplir tous les champs");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
        }

    }

    public void moncv() {
 List<Avoir_Competance> competances = null;
        Avoir_CompetanceService acs = new Avoir_CompetanceService();
        if(acs!=null)
       competances = acs.findbyJober(j);
        
        FormationService fs = new FormationService();
        List<Formation> formations = fs.findByJober(j);

        StageService ss = new StageService();
        List<Stage> stages = ss.findByJober(j);

        ExperiencesService es = new ExperiencesService();
        List<Experience> experiences = es.findByJober(j);

        ProjetService ps = new ProjetService();
        List<Projet> projets = ps.findByJober(j);

       
        int notecv = 0;
  
        int noteformation = 0;
        if (formations.size() == 0) {
            messagef = "Vous n'avez aucune formation, Cela nuira a votre CV ";

        } else if (formations.size() <= 2) {
            notecv = notecv + 10;
            noteformation = 10;
        } else if ((formations.size() <= 4) && (formations.size() > 2)) {
            notecv = notecv + 20;
            noteformation = 20;
        } else if (formations.size() > 4) {
            notecv = notecv + 30;
            noteformation = 30;
        }
        int noteexperience = 0;
        if (experiences.size() == 0) {
            messagee = "Vous n'avez aucune experiance,Votre Cv est faible  ";

        } else {
            int nbannee = 0;
            for (int i = 0; i < experiences.size(); i++) {
                LocalDate date1 = DateUtils.asLocalDate(experiences.get(i).getDateFin());
                LocalDate date = DateUtils.asLocalDate(experiences.get(i).getDateDebut());
                int year1 = date1.getYear();
                int year = date.getYear();
                int annee = year1 - year;
                nbannee = nbannee + annee;

            }

            if (nbannee <= 1) {
                notecv = notecv + 2;
                noteexperience = 2;
            } else if (nbannee == 2) {
                notecv = notecv + 5;
                noteexperience = 5;
            } else if ((nbannee > 2) && (nbannee <= 5)) {
                notecv = notecv + 10;
                noteexperience = 10;
            } else if ((nbannee > 5) && (nbannee <= 10)) {
                notecv = notecv + 15;
                noteexperience = 15;
            } else if ((nbannee > 10) && (nbannee < 20)) {
                notecv = notecv + 23;
                noteexperience = 23;
            } else if (nbannee >= 20) {
                notecv = notecv + 30;
                noteexperience = 30;
            }
        }
        int notestage = 0;
        if (stages.size() == 0) {
            messages = "Vous n'avez aucun stage  ";

        } else if (stages.size() <= 2) {
            notecv = notecv + 4;
            notestage = 4;
        } else if ((stages.size() <= 4) && (stages.size() > 2)) {
            notecv = notecv + 7;
            notestage = 7;
        } else if (stages.size() > 4) {
            notecv = notecv + 10;
            notestage = 10;
        }

        int noteprojet = 0;
        if (projets.size() == 0) {
            messagep = "Vous n'avez travailler sur aucun Projet ?  ";

        } else if (projets.size() <= 2) {
            notecv = notecv + 4;
            noteprojet = 4;
        } else if ((projets.size() <= 4) && (projets.size() > 2)) {
            notecv = notecv + 7;
            noteprojet = 7;
        } else if (projets.size() > 4) {
            notecv = notecv + 10;
            noteprojet = 10;
        }
       
        int notecompetance = 0;
        if (competances.size() == 0) {
            messagec = "Vous n'avez pas de compétances, votre cv en Souffrira ";

        } else if (competances.size() <= 2) {
            notecv = notecv + 2;
            notecompetance = 2;
        } else if ((competances.size() <= 6) && (competances.size() > 2)) {
            notecv = notecv + 10;
            notecompetance = 10;
        } else if (competances.size() > 6) {
            notecv = notecv + 20;
            notecompetance = 20;
        }

        if ((formations.size() == 0 && (experiences.size() == 0) & stages.size() == 0)) {
            cv = 0;
        }
        int reste = 100 - notecv;

        PieChart chart = new PieChart();

        PieChart.Data slice1 = new PieChart.Data("Competances", notecompetance);
        PieChart.Data slice2 = new PieChart.Data("Formations", noteformation);
        PieChart.Data slice3 = new PieChart.Data("Experience", noteexperience);
        PieChart.Data slice4 = new PieChart.Data("Stage", notestage);
        PieChart.Data slice5 = new PieChart.Data("Projet", noteprojet);
        PieChart.Data slice6 = new PieChart.Data("--", reste);

        chart.getData().add(slice1);
        chart.getData().add(slice2);
        chart.getData().add(slice3);
        chart.getData().add(slice4);
        chart.getData().add(slice5);
        chart.getData().add(slice6);

        chart.setTitle("Detail CV");

        Number val = notecv;
        Image imageOk = new Image("/Ressources/pdf.png");
        ImageView imgv = new ImageView(imageOk);
        imgv.setFitHeight(30);
        imgv.setFitWidth(30);
        Button genererbt = new Button("Generer CV", imgv);
      genererbt.addEventHandler(MouseEvent.MOUSE_CLICKED, (e)->{
        
          this.alert();
          
      });
        pbcv.setProgress(val.doubleValue() / 100);
        prog.setProgress(val.doubleValue() / 100);

        vboxchart.getChildren().add(chart);
        vboxchart.getChildren().add(genererbt);
        genererbt.setAlignment(Pos.CENTER);
        genererbt.addEventHandler(MouseEvent.MOUSE_PRESSED, (seeee) -> {

        });

    }

    public void dateverif() {

        LocalDate date1 = LocalDate.now();

        LocalDate date = DateDeNai.getValue();
        int year1 = date1.getYear();
        int year = date.getYear();

        if (year >= year1 - 15) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Date Invalide");
            alert.setContentText("Votre age est inférieur ou égale à 15 ans vous n'etes pas autorise à continuer");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            DateDeNai.setValue(null);
        }

    }

    public boolean verifUsername() {
        System.out.println(" verif ok");
        UserService us = new UserService();
        User u = us.findByUsername(username.getText());
        if (u != null) {
            username.clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Username Invalide");
            alert.setContentText("Ce username existe déja");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            return (false);
        }
        return true;
    }

    public boolean verifemail() {
        UserService us = new UserService();
        User u = us.findByEmail(username.getText());
        if (u != null) {
            email.clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Username Invalide");
            alert.setContentText("Cet email existe déja");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            return false;
        }
        SignupController su = new SignupController();

        if (!su.ValidateEmail(email.getText())) {
            email.clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Email Invalide");
            alert.setContentText("Cet email est invalide");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
            return false;
        }
        return true;
    }

    public void generer() {
        socialpro.controllers.Pdf p = new socialpro.controllers.Pdf();
        p.generer();
        
    }
 
    public void alert(){
        if(cv==1){
        if((!messagef.equals(""))||(!messagec.equals(""))||(!messagee.equals(""))||(!messagep.equals(""))||(!messages.equals(""))){
         Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Attention");
            alert.setHeaderText(" set");
            alert.setContentText(messagef+"\n"+messagec+"\n"+messagee+"\n"+messagep+"\n"+messages);
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    Pdf.setProfil(j);
                    Pdf.setUser(user);
                   this.generer();
                }
            });
        }else{
                               Pdf.setProfil(j);
                    Pdf.setUser(user);
                   this.generer(); 
        }
        
    }else{
                      Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Cv Incomplet");
            alert.setContentText("Vous ne pouvez pas generer un cv car votre Cv est trop faible");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });  
        }
    }
}
