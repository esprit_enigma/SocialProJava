/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author IMEN
 */
public class AnnonceController implements Initializable {
  @FXML
    private JFXButton btnAE;

    @FXML
    private JFXButton btnAnJ;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    
    
    
     @FXML
        void OnClickbtnAnJ(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
          
                   fxmlLoader.setLocation(getClass().getResource("/GUI/MesAnnoncesJober.fxml"));

            fxmlLoader.load();
       
              
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);
    }

    
     @FXML
    void OnClickbtnAE(ActionEvent event) {
         FXMLLoader fxmlLoader = new FXMLLoader();
        try {
                fxmlLoader.setLocation(getClass().getResource("/GUI/MesAnnonceEntreprise.fxml"));

            fxmlLoader.load();
          
        } catch (IOException e) {
            
        }
        AnchorPane root = fxmlLoader.getRoot();
        ApplicationController.rootP.getChildren().clear();
        ApplicationController.rootP.getChildren().add(root);

    }
    
}
