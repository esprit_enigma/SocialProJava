/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Avoir_Competance;
import socialproo.models.Experience;
import socialproo.models.Formation;
import socialproo.models.Jober;
import socialproo.models.Projet;
import socialproo.models.User;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import socialproo.services.impl.Avoir_CompetanceService;
import socialproo.services.impl.ExperiencesService;
import socialproo.services.impl.FormationService;
import socialproo.services.impl.ProjetService;
import socialproo.services.impl.StageService;
import ui.FillProgressIndicator;

/**
 *
 * @author oudayblouza
 */
public class DetailCvController implements Initializable {
      @FXML
    private StackPane stack;

    @FXML
    private ScrollPane scroll1;  
    private static User user;
    private static Jober j;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    this.moncv();
    }
    public void moncv(){
        stack.getChildren().clear();
        stack.getChildren().add(scroll1);
        Avoir_CompetanceService comp = new Avoir_CompetanceService();
        List<Avoir_Competance> listcomp = comp.findbyJober(j);
        int i;
        HBox root = new HBox();
        for (i = 0; i < listcomp.size(); i++) {

            FillProgressIndicator pb = new FillProgressIndicator();
            pb.setProgress(listcomp.get(i).getNiveau());
            Text competa = new Text();
            competa.setFill(Color.BLACK);
            competa.setText(listcomp.get(i).getCompetance().getNom());

            VBox vbox1 = new VBox();
            vbox1.setAlignment(Pos.CENTER);
            vbox1.getChildren().addAll(pb, competa);

            root.getChildren().addAll(vbox1);
        }
        VBox vbox = new VBox();
        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_LEFT);
        Label compet = new Label();
        Image img0 = new Image("/Ressources/competance.png");
        ImageView imgv0 = new ImageView();

        imgv0.setFitWidth(50);
        imgv0.setPreserveRatio(true);
        imgv0.setSmooth(true);
        imgv0.setCache(true);
        imgv0.setImage(img0);
        compet.setTextFill(Color.STEELBLUE);
        compet.setText("Mes Competances :");


 
        InnerShadow is = new InnerShadow();
        is.setOffsetX(4.0f);
        is.setOffsetY(4.0f);
        compet.setEffect(is);
 
        compet.setUnderline(true);
        hbox.setSpacing(10);
        hbox.getChildren().addAll(imgv0, compet);
        vbox.getChildren().addAll(hbox, root);

        //formation
        VBox vbox1 = new VBox();
        vbox1.setSpacing(50);
        HBox hbox1 = new HBox();
        hbox1.setAlignment(Pos.CENTER_LEFT);
        Label form = new Label();
        Image img = new Image("/Ressources/formation.png");
        ImageView imgv = new ImageView();

        imgv.setFitWidth(50);
        imgv.setPreserveRatio(true);
        imgv.setSmooth(true);
        imgv.setCache(true);
        imgv.setImage(img);
        form.setTextFill(Color.STEELBLUE);
        form.setText("Mes Formation :");

        InnerShadow is1 = new InnerShadow();
        is1.setOffsetX(4.0f);
        is1.setOffsetY(4.0f);
        form.setEffect(is1);

        form.setUnderline(true);


       
        hbox1.getChildren().addAll(imgv, form);
        hbox1.setSpacing(10);
        FormationService fs = new FormationService();
        List<Formation> lstforma = fs.findByJober(j);
        this.afficherFormation(vbox1, hbox1, lstforma);
        //experience
        VBox vbox2 = new VBox();
        vbox2.setSpacing(50);
        HBox hbox2 = new HBox();
        hbox2.setAlignment(Pos.CENTER_LEFT);
        Label exp = new Label();
        Image img1 = new Image("/Ressources/experience.png");
        ImageView imgv1 = new ImageView();

        imgv1.setFitWidth(50);
        imgv1.setPreserveRatio(true);
        imgv1.setSmooth(true);
        imgv1.setCache(true);
        imgv1.setImage(img1);
        exp.setTextFill(Color.STEELBLUE);
        exp.setText("Mon Experience :");

        InnerShadow is2 = new InnerShadow();
        is2.setOffsetX(4.0f);
        is2.setOffsetY(4.0f);
        exp.setEffect(is1);

        exp.setUnderline(true);
  
  

        hbox2.getChildren().addAll(imgv1, exp);
        hbox2.setSpacing(10);
        ExperiencesService exps = new ExperiencesService();
        List<Experience> lstexp = exps.findByJober(j);
        this.afficherExperience(vbox2, hbox2, lstexp);

        //projet
        VBox vbox3 = new VBox();
        vbox3.setSpacing(50);
        HBox hbox3 = new HBox();
        hbox3.setAlignment(Pos.CENTER_LEFT);
        Label pro = new Label();
        Image img2 = new Image("/Ressources/projet.png");
        ImageView imgv2 = new ImageView();

        imgv2.setFitWidth(50);
        imgv2.setPreserveRatio(true);
        imgv2.setSmooth(true);
        imgv2.setCache(true);
        imgv2.setImage(img2);
        pro.setTextFill(Color.STEELBLUE);
        pro.setText("Mes projets :");

        InnerShadow is3 = new InnerShadow();
        is3.setOffsetX(4.0f);
        is3.setOffsetY(4.0f);
        pro.setEffect(is3);

        pro.setUnderline(true);

 

        hbox3.getChildren().addAll(imgv2, pro);
        hbox3.setSpacing(10);
        ProjetService pros = new ProjetService();
        List<Projet> listpro = pros.findByJober(j);
        this.afficherPro(vbox3, hbox3, listpro);

        //stage
        VBox vbox4 = new VBox();
        vbox4.setSpacing(50);
        HBox hbox4 = new HBox();
        hbox4.setAlignment(Pos.CENTER_LEFT);
        Label sta = new Label();
        Image img3 = new Image("/Ressources/stage.png");
        ImageView imgv3 = new ImageView();

        imgv3.setFitWidth(50);
        imgv3.setPreserveRatio(true);
        imgv3.setSmooth(true);
        imgv3.setCache(true);
        imgv3.setImage(img3);
        sta.setTextFill(Color.STEELBLUE);
        sta.setText("Mes stages :");

        InnerShadow is4 = new InnerShadow();
        is4.setOffsetX(4.0f);
        is4.setOffsetY(4.0f);
        sta.setEffect(is4);

        sta.setUnderline(true);

   

        hbox4.getChildren().addAll(imgv3, sta);
        hbox4.setSpacing(10);
        StageService stas = new StageService();
        List<socialproo.models.Stage> liststa = stas.findByJober(j);
        this.afficherStage(vbox4, hbox4, liststa);

        Separator sp = new Separator();
        Separator sp1 = new Separator();
        Separator sp2 = new Separator();
        Separator sp3 = new Separator();
        VBox grandvbox = new VBox();
        grandvbox.setSpacing(10);
        grandvbox.getChildren().addAll(vbox, sp, vbox1, sp1, vbox2, sp2, vbox3, sp3, vbox4);

        System.out.println("    ok here ");

        scroll1.setContent(grandvbox);
    }
     public void afficherPro(VBox vbox3, HBox hbox3, List<Projet> listpro) {
        vbox3.getChildren().addAll(hbox3);
        for (int i = 0; i < listpro.size(); i++) {
            HBox boxprojet = new HBox();
            boxprojet.setAlignment(Pos.CENTER_LEFT);
            boxprojet.setSpacing(50);

            Label date2 = new Label();
            date2.setText(listpro.get(i).getDateAjout().toString() + " -- " + listpro.get(i).getDateFin().toString());
            date2.setTextFill(Color.DARKBLUE);
            boxprojet.setPadding(new Insets(15, 12, 15, 30));
            boxprojet.getChildren().add(date2);

            GridPane projetgp = new GridPane();

            Label nompro = new Label("Nom du projet :");
            nompro.setTextFill(Color.DARKCYAN);
            Label organisationpro = new Label("Organisation :");
            organisationpro.setTextFill(Color.DARKCYAN);

            Label descriptionpro = new Label("Description :");
            descriptionpro.setTextFill(Color.DARKCYAN);

            projetgp.add(nompro, 0, 0);
            projetgp.setHalignment(nompro, HPos.LEFT);
            projetgp.add(organisationpro, 0, 1);
            projetgp.setHalignment(organisationpro, HPos.LEFT);
            projetgp.add(descriptionpro, 0, 2);
            projetgp.setHalignment(descriptionpro, HPos.LEFT);

            Label nomprocont = new Label(listpro.get(i).getNom());
            Label organisationprocont = new Label(listpro.get(i).getOrganisation());
            Text descriptionprocont = new Text(listpro.get(i).getDescription());
            TextFlow descriptionprof = new TextFlow(descriptionprocont);
            descriptionprof.setMaxWidth(600);

            projetgp.add(nomprocont, 3, 0);
            projetgp.add(organisationprocont, 3, 1);
            projetgp.add(descriptionprocont, 3, 2);
            projetgp.setHalignment(descriptionprof, HPos.LEFT);

            boxprojet.getChildren().add(projetgp);
            vbox3.getChildren().addAll(boxprojet);
        }
    }

    public void afficherStage(VBox vbox4, HBox hbox4, List<socialproo.models.Stage> liststa) {

        vbox4.getChildren().addAll(hbox4);
        for (int i = 0; i < liststa.size(); i++) {
            HBox boxstage = new HBox();
            boxstage.setAlignment(Pos.CENTER_LEFT);
            boxstage.setSpacing(50);

            Label date3 = new Label();
            date3.setText(liststa.get(i).getDateAjout().toString() + " -- " + liststa.get(i).getDateFin().toString());
            date3.setTextFill(Color.DARKBLUE);
            boxstage.setPadding(new Insets(15, 12, 15, 30));
            boxstage.getChildren().add(date3);

            GridPane stagegp = new GridPane();

            Label organisationsta = new Label("Organisation :");
            organisationsta.setTextFill(Color.DARKCYAN);
            Label descriptionsta = new Label("Description :");
            descriptionsta.setTextFill(Color.DARKCYAN);

            stagegp.add(organisationsta, 0, 0);
            stagegp.setHalignment(organisationsta, HPos.LEFT);
            stagegp.add(descriptionsta, 0, 1);
            stagegp.setHalignment(descriptionsta, HPos.LEFT);

            Label organisationstacont = new Label(liststa.get(i).getOrganisation());

            Text descriptionstacont = new Text(liststa.get(i).getDescription());
            TextFlow descriptionstaf = new TextFlow(descriptionstacont);
            descriptionstaf.setMaxWidth(600);

            stagegp.add(organisationstacont, 3, 0);
            stagegp.add(descriptionstaf, 3, 1);
            stagegp.setHalignment(descriptionstaf, HPos.RIGHT);

            boxstage.getChildren().add(stagegp);
            vbox4.getChildren().addAll(boxstage);
        }

//
    }

    public void afficherExperience(VBox vbox2, HBox hbox2, List<Experience> lstexp) {
        vbox2.getChildren().addAll(hbox2);
        for (int i = 0; i < lstexp.size(); i++) {
            HBox boxexperience = new HBox();
            boxexperience.setAlignment(Pos.CENTER_LEFT);
            boxexperience.setSpacing(50);

            Label date1 = new Label();
            date1.setText(lstexp.get(i).getDateDebut().toString() + " -- " + lstexp.get(i).getDateFin().toString());
            date1.setTextFill(Color.DARKBLUE);
            boxexperience.getChildren().add(date1);
            boxexperience.setPadding(new Insets(15, 12, 15, 30));
            GridPane experiencegp = new GridPane();

            Label organisation = new Label("Organisation :");
            organisation.setTextFill(Color.DARKCYAN);
            Label poste = new Label("Poste :");
            poste.setTextFill(Color.DARKCYAN);
            Label descriptionexp = new Label("Description :");
            descriptionexp.setTextFill(Color.DARKCYAN);

            experiencegp.add(organisation, 0, 0);
            experiencegp.setHalignment(organisation, HPos.LEFT);
            experiencegp.add(poste, 0, 1);
            experiencegp.setHalignment(poste, HPos.LEFT);
            experiencegp.add(descriptionexp, 0, 2);
            experiencegp.setHalignment(descriptionexp, HPos.LEFT);

            Label organisationcont = new Label(lstexp.get(i).getOrganisation());
            Label postecont = new Label(lstexp.get(i).getPoste());
            Text descriptionexpcont = new Text(lstexp.get(i).getDescription());
            TextFlow descriptionexpf = new TextFlow(descriptionexpcont);
            descriptionexpf.setMaxWidth(600);
            experiencegp.setHalignment(descriptionexpf, HPos.LEFT);

            experiencegp.add(organisationcont, 3, 0);
            experiencegp.add(postecont, 3, 1);
            experiencegp.add(descriptionexpf, 3, 2);

            boxexperience.getChildren().add(experiencegp);

            vbox2.getChildren().addAll(boxexperience);
        }

    }

    public void afficherFormation(VBox vbox1, HBox hbox1, List<Formation> lstforma) {
        vbox1.getChildren().addAll(hbox1);
        for (int i = 0; i < lstforma.size(); i++) {
            HBox boxformation = new HBox();
            boxformation.setAlignment(Pos.CENTER_LEFT);
            boxformation.setSpacing(50);
            System.out.println(lstforma.get(i).getId());
            Label date = new Label();
            date.setText(lstforma.get(i).getDateAjout().toString() + " -- " + lstforma.get(i).getDateFin().toString());
            date.setTextFill(Color.DARKBLUE);
            boxformation.getChildren().add(date);
            boxformation.setPadding(new Insets(15, 12, 15, 30));
            GridPane formationgp = new GridPane();

            Label institut = new Label("Institut :");
            institut.setTextFill(Color.DARKCYAN);
            Label diplome = new Label("Diplome :");
            diplome.setTextFill(Color.DARKCYAN);
            Label description = new Label("Description :");
            description.setTextFill(Color.DARKCYAN);

            formationgp.add(institut, 0, 0);
            formationgp.setHalignment(institut, HPos.LEFT);
            formationgp.add(diplome, 0, 1);
            formationgp.setHalignment(diplome, HPos.LEFT);
            formationgp.add(description, 0, 2);
            formationgp.setHalignment(description, HPos.LEFT);

            Label institutcont = new Label(lstforma.get(i).getInstitut());
            Label diplomecont = new Label(lstforma.get(i).getDiplome());
            Text descriptioncont = new Text(lstforma.get(i).getDescription());
            TextFlow descriptiontf = new TextFlow(descriptioncont);
            descriptiontf.setMaxWidth(600);
            formationgp.setHalignment(descriptiontf, HPos.LEFT);

            formationgp.add(institutcont, 3, 0);
            formationgp.add(diplomecont, 3, 1);
            formationgp.add(descriptiontf, 3, 2);

            boxformation.getChildren().add(formationgp);
            vbox1.getChildren().addAll(boxformation);
        }
    }
      public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        DetailCvController.user = user;
    }

    public static Jober getProfil() {
        return j;
    }

    public static void setProfil(Jober profil) {
         DetailCvController.j = profil;
    }

}
