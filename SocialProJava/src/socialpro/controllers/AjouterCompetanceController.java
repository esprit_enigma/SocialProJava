/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

/**
 *
 * @author oudayblouza
 */
import socialproo.models.Avoir_Competance;
import socialproo.models.Competance;
import socialproo.models.Jober;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXSlider;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ProgressIndicator;

import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

import javafx.scene.layout.StackPane;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialproo.services.impl.Avoir_CompetanceService;
import socialproo.services.impl.CompetanceService;
import socialproo.services.impl.JoberService;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class AjouterCompetanceController implements Initializable {
    
    @FXML
    private ProgressIndicator Prind;
    @FXML
    private JFXComboBox<Competance> ComboComp;
    @FXML
    private JFXSlider tfNiveau;
    @FXML
    private StackPane stackcomp;
    @FXML
    private StackPane root;
    @FXML
    public JFXButton closeAjouter, close;
    @FXML
    private Button closeClose;
    
    Jober jober = new Jober();
    JoberService joberser = new JoberService();
    
    CompetanceService cs = new CompetanceService();
    List<Competance> lstcp = cs.getAll();
    Avoir_Competance ac = new Avoir_Competance();
    Avoir_CompetanceService acservice = new Avoir_CompetanceService();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Image imgclose = new Image("/Ressources/close_new.png");
        
        Image img = new Image("Ressources/formation.jpg");
    
       BackgroundImage myBI = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
       stackcomp.setBackground(new Background(myBI));

        ImageView imgvb = new ImageView(imgclose);
        imgvb.setFitHeight(30);
        imgvb.setFitWidth(30);
        close.setGraphic(imgvb);
        close.setButtonType(JFXButton.ButtonType.RAISED);
        DropShadow shadow = new DropShadow();
//Adding the shadow when the mouse cursor is on
        close.addEventHandler(MouseEvent.MOUSE_ENTERED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                close.setEffect(shadow);
                
            }
        });
//Removing the shadow when the mouse cursor is off
        close.addEventHandler(MouseEvent.MOUSE_EXITED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                close.setEffect(null);
            }
        });
        
        root.setStyle("-fx-background-color: null;");
        root.setPadding(new Insets(10));
        Rectangle innerRect = new Rectangle();
        Rectangle outerRect = new Rectangle();
        
        root.layoutBoundsProperty().addListener(
                (observable, oldBounds, newBounds) -> {
                    innerRect.relocate(
                            newBounds.getMinX() + 500,
                            newBounds.getMinY() + 500
                    );
                    innerRect.setWidth(newBounds.getWidth() - 500 * 2);
                    innerRect.setHeight(newBounds.getHeight() - 500 * 2);
                    
                    outerRect.setWidth(newBounds.getWidth());
                    outerRect.setHeight(newBounds.getHeight());
                    
                    Shape clip = Shape.subtract(outerRect, innerRect);
                    root.setClip(clip);
                }
        );
        ObservableList<Competance> Competancechoices = FXCollections.observableArrayList();
        lstcp.forEach(e -> Competancechoices.add(e));
        ComboComp.setItems(Competancechoices);
        Number val = 50;
        Prind.setProgress(val.doubleValue() / 100);
        
        tfNiveau.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                    Number old_val, Number new_val) {
                
                Prind.setProgress(new_val.doubleValue() / 100);
            }
        });
    }
    
    @FXML
    private void AjouterAction(ActionEvent event) throws FileNotFoundException {
        if ((tfNiveau.getValue() > 0) && (ComboComp.getSelectionModel() != null)) {
            
            jober = joberser.findByUser(LoginController.currentUser);
            String nomcomp = ComboComp.getValue().toString();
            CompetanceService compser = new CompetanceService();
            Competance competance = compser.findByNom(nomcomp);
            
            System.out.println(competance + " " + jober);
            if (acservice.findbyJoberComp(jober, competance).getId() == 0) {
                ac.setCompetance(competance);
                ac.setNiveau((int) tfNiveau.getValue());
                ac.setJober(jober);
                
                Date d3 = new Date();
                ac.setDateAjout(d3);
                
                acservice.add(ac);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Succès");
                alert.setHeaderText("Succès : Ajout ");
                alert.setContentText("Ajout effectué avec Succès");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                Stage stage = (Stage) close.getScene().getWindow();
                stage.close();
                JoberController jc = new JoberController();
                jc.refresh();
            } else {
                ac = acservice.findbyJoberComp(jober, competance);
                ac.setNiveau((int) tfNiveau.getValue());
                acservice.update(ac);
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Succès");
                alert.setHeaderText("Succès : Modification ");
                alert.setContentText("Modification Niveau effectué avec Succès");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
            }
            
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Erreur");
            alert.setHeaderText("Informations Incompletes");
            alert.setContentText("Veuillez remplir tous les champs");
            alert.showAndWait().ifPresent(rs -> {
                if (rs == ButtonType.OK) {
                    System.out.println("Pressed OK.");
                }
            });
        }
        
    }
    
    @FXML
    private void closeCloseOnAction(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }
    
}
