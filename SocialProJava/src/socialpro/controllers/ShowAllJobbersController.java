/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import socialproo.models.Jober;
import socialproo.models.Specialite;
import socialproo.models.User;
import com.jfoenix.controls.JFXRadioButton;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.imageio.ImageIO;
import socialproo.services.impl.JoberService;
import socialproo.services.impl.SpecialiteService;
import socialproo.services.impl.UserService;

/**
 * FXML Controller class
 *
 * @author Alaa
 */
public class ShowAllJobbersController implements Initializable {

    @FXML
    VBox JobbersVbox, EntrepVbox;
    @FXML
    HBox jisHbox, eisHbox;

    @FXML
    JFXRadioButton jobberradiobtn, entrepradiobtn;

    Stage stage;
    Parent root;
    Scene scene;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UserService us = new UserService();
        JobbersVbox.setVisible(true);
        ToggleGroup group = new ToggleGroup();
        jobberradiobtn.setToggleGroup(group);
        jobberradiobtn.setSelected(true);
        jobberradiobtn.setSelectedColor(Color.GOLD);
        entrepradiobtn.setToggleGroup(group);
        entrepradiobtn.setSelectedColor(Color.GOLD);
        JobberSelected();
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if (group.getSelectedToggle().equals(jobberradiobtn)) {
                    JobberSelected();
                } else if (group.getSelectedToggle().equals(entrepradiobtn)) {
                    EntrepSelected();
                }
            }
        });

    }

    public void JobberSelected() {
        JobbersVbox.getChildren().clear();
        UserService us = new UserService();
        JoberService ps = new JoberService();
        SpecialiteService ss = new SpecialiteService();
        List<User> listejobbers = us.getAllJobber();

        for (User jobber : listejobbers) {
            Jober p = ps.findByUser(jobber);
            Specialite s = ss.findById(p.getSpecialite().getId());
            
            
            
            
            Image image = new Image("Ressources/socialpro.png");
            ImageView img = new ImageView(image);
            try{
        InputStream input = null;
        try {
            input = new URL("http://localhost/images/ProfilePic/" + jobber.getUsername() + "/" + jobber.getUsername() + ".jpg").openStream();
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedImage image1 = null;
        try {
            image1 = ImageIO.read(input);
        } catch (IOException ex) {
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Image image0 = SwingFXUtils.toFXImage(image1, null);

        img.setImage(image0);}
catch(Exception ex1) {
         img.setImage(image);
        }
            
            
            
            img.setFitHeight(150);
            img.setFitWidth(150);
            Label usernameLabel = new Label("Username: " + jobber.getUsername());
            usernameLabel.setPadding(new Insets(0, 0, 5, 0));
            usernameLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 25));

            Label spec = new Label("Specialité: " + s.getNom());
            spec.setPadding(new Insets(5, 0, 0, 0));

            Button btn = new Button("Open Profil");
            btn.setId(String.valueOf(jobber.getId()));

            btn.setOnAction((ActionEvent event) -> {
                try {
                    ProfileDetailController.setUser(jobber);
                    ProfileDetailController.setProfil(p);
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(getClass().getResource("/GUI/ProfileDetail.fxml"));

                    Scene scene = new Scene(fxmlLoader.load());
                    Stage stage = new Stage();

                    stage.setTitle("Ajout Competance");
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(ShowAllJobbersController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            VBox v = new VBox(usernameLabel, spec, btn);
            v.setPadding(new Insets(5, 5, 5, 20));
            HBox h = new HBox(img, v);
            v.setAlignment(Pos.CENTER);
            h.setAlignment(Pos.TOP_CENTER);
            //h.setBackground(new Background(new BackgroundFill(Paint.valueOf("#FFFFFF"), CornerRadii.EMPTY, Insets.EMPTY)));

            h.setStyle("-fx-border-style: solid inside;"
                    + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
                    + "-fx-border-radius: 5;" + "-fx-border-color: blue;");
            JobbersVbox.setAlignment(Pos.TOP_CENTER);
            JobbersVbox.getChildren().add(h);
        }
    }

    public void EntrepSelected() {
        JobbersVbox.getChildren().clear();
        UserService us = new UserService();
        JoberService ps = new JoberService();
        SpecialiteService ss = new SpecialiteService();
        List<User> listeentreps = us.getAllEntreps();

        for (User ent : listeentreps) {
            Jober p = ps.findByUser(ent);
            Specialite s = ss.findById(p.getSpecialite().getId());
            Image image = new Image("Ressources/socialpro.png");
            ImageView img = new ImageView(image);
            img.setFitHeight(150);
            img.setFitWidth(150);
            Label usernameLabel = new Label("Username: " + ent.getUsername());
            usernameLabel.setPadding(new Insets(0, 0, 5, 0));
            usernameLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 25));

            Label spec = new Label("Specialité: " + s.getNom());
            spec.setPadding(new Insets(5, 0, 0, 0));

            Button btn = new Button("Open Profil");
            btn.setId(String.valueOf(ent.getId()));

            btn.setOnAction((ActionEvent event) -> {
                try {
                    JobberProfileController.setUser(ent);
                    JobberProfileController.setProfil(p);
                    stage = (Stage) JobbersVbox.getScene().getWindow();
                    root = FXMLLoader.load(getClass().getResource("/GUI/JobberProfile.fxml"));
                    scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException ex) {
                    Logger.getLogger(ShowAllJobbersController.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            VBox v = new VBox(usernameLabel, spec, btn);
            v.setPadding(new Insets(5, 5, 5, 20));
            HBox h = new HBox(img, v);
            v.setAlignment(Pos.CENTER);
            h.setAlignment(Pos.TOP_CENTER);

            h.setStyle("-fx-border-style: solid inside;"
                    + "-fx-border-width: 2;" + "-fx-border-insets: 5;"
                    + "-fx-border-radius: 5;" + "-fx-border-color: blue;");
            JobbersVbox.setAlignment(Pos.TOP_CENTER);
            JobbersVbox.getChildren().add(h);
        }
    }

}
