/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import custom.CustomTf;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialproo.models.User;
import socialproo.services.impl.EntrepriseService;
import socialproo.services.impl.UserService;
import socialproo.services.interfaces.IUserService;
import socialproo.technique.BCrypt;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;


/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class LoginController implements Initializable {
     @FXML
    private Hyperlink forgetpasswordlabel;

    @FXML
    private AnchorPane apMother;
    @FXML
    private AnchorPane apDesignPane;
    @FXML
    private Hyperlink hlCrateAccount;
    @FXML
    private TextField tfUserName;
    @FXML
    private PasswordField pfUserPassword;
    @FXML
    private Button btnUserNameTfClear;
    @FXML
    private Button btnPassFieldClear;
    @FXML
    private Button btnLogin;
   
    public static LoginController loginController;
    private User user;
    CustomTf cTF = new CustomTf();
    
    Stage stage;
    Scene scene;
    Parent root;

    static User currentUser;

    /**
     * Initializes the controller class.
     */
    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        LoginController.currentUser = currentUser;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cTF.clearTextFieldByButton(tfUserName, btnUserNameTfClear);
        cTF.clearTextFieldByButton(pfUserPassword, btnPassFieldClear);
        BooleanBinding boolenBinding = tfUserName.textProperty().isEmpty()
                .or(pfUserPassword.textProperty().isEmpty());

        btnLogin.disableProperty().bind(boolenBinding);

    }

    public LoginController() {
        loginController = this;
    }

    public static LoginController getInstance() {
        return loginController;
    }

    public User getUser() {
        return user;
    }
    
    private Connection connection;
    EntrepriseService t = new EntrepriseService();

  

    @FXML
    private void pfUserPassOnHitEnter(ActionEvent event) {
    }

    @FXML
    private void pfUserNameOnHitEnter(ActionEvent event) {
    }

    @FXML
    private void btnLogin(ActionEvent event) throws IOException {
   
        user = new User();
         ApplicationController apController = new ApplicationController();
         FXMLLoader loader = new FXMLLoader();
       
            loader.setLocation(getClass().getResource("/GUI/Application.fxml"));
            loader.load();
            Parent parent = loader.getRoot();
            Scene adminPanelScene = new Scene(parent);
            Stage adminPanelStage = new Stage();
            adminPanelStage.setMaximized(true);
        String username = tfUserName.getText();
        UserService us = new UserService();
        User u = us.findByUsername(username);
        StringBuilder finalpassword = new StringBuilder(u.getPassword());
        finalpassword.setCharAt(2, 'a');
        String pass = finalpassword.toString();
        if (u != null) {
            if (BCrypt.checkpw(pfUserPassword.getText(), pass)) {
                currentUser = u;
                user=u;
                if ("a:1:{i:0;s:10:\"ROLE_ADMIN\";}".equals(u.getRoles())) {
                    stage = (Stage) tfUserName.getScene().getWindow();
                    root = FXMLLoader.load(getClass().getResource("/GUI/AdminProfile.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } else if ("a:1:{i:0;s:10:\"ROLE_JOBER\";}".equals(u.getRoles())) {
                    stage = (Stage) tfUserName.getScene().getWindow();
                    root = FXMLLoader.load(getClass().getResource("/GUI/Jober.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.centerOnScreen();
                    stage.show();

                } else if ("a:1:{i:0;s:15:\"ROLE_ENTREPRISE\";}".equals(u.getRoles())) {
                    System.out.println(user);
                ApplicationController apControl = loader.getController();
                        apControl.onClickProfile(event);
                        apControl.viewDetails();
                        adminPanelStage.setScene(adminPanelScene);
                   
                        adminPanelStage.setTitle("SocialPro");
                        adminPanelStage.show();

                        Stage stage = (Stage) btnLogin.getScene().getWindow();
                        stage.close();
                } else if ("a:1:{i:0;s:13:\"ROLE_PREJOBER\";}".equals(u.getRoles())) {
                    stage = (Stage) tfUserName.getScene().getWindow();
                    root = FXMLLoader.load(getClass().getResource("/GUI/InitJobberProfile.fxml"));
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.centerOnScreen();
                    stage.show();
                } else if ("a:1:{i:0;s:18:\"ROLE_PREENTREPRISE\";}".equals(u.getRoles())) {
                    loadRegistration();
                } else if ("a:1:{i:0;s:19:\"ROLE_ENTREPRISEBLOQ\";}".equals(u.getRoles())) {
                    loadUpdate();
                }
            } else {
                TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Mot De Passe Est Invalide ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
            }
            
        } else if (u == null) {
            TrayNotification tn = new TrayNotification();
            tn.setTitle("AVERTISSEMENT");
            tn.setMessage("Utilisateur n existe pas ");
            tn.setNotificationType(NotificationType.WARNING);
            tn.setAnimationType(AnimationType.SLIDE);
            tn.showAndWait();
        }

        
        
        
}
        
        
        
        
       
    
    
    
    
    
    
    
    
    
    
    
    

    private void loadRegistration() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Registration.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("Registration -SocialPro");
            nStage.show();
            Stage stage = (Stage) btnLogin.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void loadBloque() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/ValiderEntreprises.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("Validation -SocialPro");
            nStage.show();
            Stage stage = (Stage) btnLogin.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
      public void loadHome(ActionEvent event) throws IOException {
     Parent root = null;
     FXMLLoader loader = new FXMLLoader();
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Application.fxml"));
            ApplicationController apControl = loader.getController();
                        
                        apControl.onClickProfile(event);
                       
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("SocialPro");
            nStage.show();
            Stage stage = (Stage) btnLogin.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
          
          
          
          
          
          
          
          
          
      
             
     
        
//             ApplicationController app= loader.getController();
//                app.onClickProfile(event);
//            
            
            
            
            
           
          
      

    }

    public void entrepriseRole(User u) throws IOException {
        IUserService user = new UserService();
        User c;
        c = user.findById(u.getId());
        if (c != null) {
            System.out.println(u);
            if (c.getRoles().equals("a:1:{i:0;s:18:\"ROLE_PREENTREPRISE\";}")) {
                loadRegistration();
            }else if(c.getRoles().equals("a:2:{i:0;s:10:\"ROLE_JOBER\";i:1;s:10:\"ROLE_ADMIN\";}")){
                loadBloque();
            }else if(c.getRoles().equals("a:1:{i:0;s:19:\"ROLE_ENTREPRISEBLOQ\";}")){
                loadUpdate();
            }
            else {
                loadRegistration();
            }
        }
    }
    public void loadUpdate() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/Update.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("Bloque -SocialPro");
            nStage.show();
            Stage stage = (Stage) btnLogin.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
        public void loadRegidter() {
            System.out.println("hello");
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/GUI/register.fxml"));
            Scene scene = new Scene(root);
            Stage nStage = new Stage();
            nStage.setScene(scene);
            nStage.setMaximized(true);
            nStage.setTitle("Registration -SocialPro");
            nStage.show();
            Stage stage = (Stage) hlCrateAccount.getScene().getWindow();
            stage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

      @FXML
    void SignUp(ActionEvent event) throws IOException {
        loadRegidter();
    }

    
    
    @FXML
    void forgetPassword(ActionEvent event) throws IOException {
        stage = (Stage) tfUserName.getScene().getWindow();
        root = FXMLLoader.load(getClass().getResource("/GUI/ResetPassword.fxml"));
        scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();}

}
