/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import socialproo.models.Notification;
import socialproo.services.impl.NotificationService;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ListNotifController implements Initializable {

    @FXML
    VBox vb;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NotificationService ns = new NotificationService();
        List<Notification> ls = ns.getAllByUser(LoginController.currentUser.getId());
        System.out.println(LoginController.currentUser.getId());
        for (Notification n : ls) {
            Label l = new Label(n.getText());
            l.setPadding(new Insets(8, 0, 8, 0));
            l.setStyle("-fx-effect: dropshadow(three-pass-box,  #132736, 10, 0, 0, 0);"
                    + "-fx-font-family: Cambria,'Helvetica Neue',Arial,sans-serif;"
                    + "-fx-text-fill: #1304db;"
                    + "-fx-font-size: 17px;");
            vb.getChildren().add(l);
        }
    }

}
