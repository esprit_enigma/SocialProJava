/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package socialpro.controllers;

import com.jfoenix.controls.JFXListCell;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import socialproo.models.ActiviteComplexe;

/**
 * FXML Controller class
 *
 * @author ASUS
 */
public class ActiviteCelluleController extends JFXListCell<ActiviteComplexe>  {
    @FXML
    private HBox box;
    @FXML
    private Text tfNom;
    @FXML
    private Text tfCtaegorie;
    @FXML
    private Text tfDescription;
    private FXMLLoader loader;
    

   

    @Override
    public void updateItem(ActiviteComplexe item, boolean empty) {
        super.updateItem(item, empty); 
        if (empty || item == null) {
            setText(null);
            setGraphic(null);
           
        } else {
            if(loader==null){
                loader = new FXMLLoader(getClass().getResource("/GUI/Activite.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            tfNom.setText(item.getNom());
            tfCtaegorie.setText(item.getNomcategorie());
            tfDescription.setText(item.getDescription());
            System.out.println("nom "+item.getNom());
            setText(null);
            setGraphic(box);
        }
    }
    

    
}
